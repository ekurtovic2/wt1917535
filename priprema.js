const db = require('./db.js')
db.sequelize.sync({force:true}).then(function(){
    inicializacija().then(function(){
        console.log("Gotovo kreiranje tabela i ubacivanje pocetnih podataka!");
        process.exit();
    });
});
function inicializacija(){
    var osobeListaPromisea=[];
    var saleListaPromisea=[];
    var terminiListaPromisea=[];
    var rezervacijeListaPromisea=[];
    //stavicemo uporedo termine i sale ili zaredom mozda vidjecemo

    return new Promise(function(resolve,reject){
      osobeListaPromisea.push(db.Osoblje.create({id:1,ime:'Neko',prezime:'Nekić',uloga:'profesor'}));
      osobeListaPromisea.push(db.Osoblje.create({id:2,ime:'Drugi',prezime:'Neko',uloga:'asistent'}));
      osobeListaPromisea.push(db.Osoblje.create({id:3,ime:'test',prezime:'test',uloga:'asistent'}));
      //promijeniti da se ubacuju jedan za drugim;ne treba ustv

      //dakle ako Promise.all(osobeListaPromisea) bude reject-an, then nema handlera za to,
      //pa se prosto kreira novi promise koji preuzima finalno stanje prvog;
      //mozda bolje drugi parametar kod then
      Promise.all(osobeListaPromisea).then(function(osobe){
          var osoba1=osobe.filter(function(o){return o.ime==='Neko'})[0];
          var osoba2=osobe.filter(function(o){return o.ime==='Drugi'})[0];
          var osoba3=osobe.filter(function(o){return o.ime==='test'})[0];

          //Promise.resolve vs new Promise(resolve)-kasnije vidjet fino, za sad sa new
          saleListaPromisea.push(db.Sala.create({id:1,naziv:'1-11'}).then(function(s){
            return s.setOsoblje(osoba1).then(function(){return new Promise(function(resolve,reject){resolve(s);});});
          }));
          saleListaPromisea.push(db.Sala.create({id:2,naziv:'1-15'}).then(function(s){
            return s.setOsoblje(osoba1).then(function(){return new Promise(function(resolve,reject){resolve(s);});});
          }));
          Promise.all(saleListaPromisea).then(function(sale){
            var sala1=sale.filter(function(s){return s.naziv==='1-11'})[0];
            var sala2=sale.filter(function(s){return s.naziv==='1-15'})[0];

            terminiListaPromisea.push(db.Termin.create({id:1,redovni:false,dan:null,datum:'01.01.2020',semestar:null,pocetak:'12:00',kraj:'13:00'}));
            terminiListaPromisea.push(db.Termin.create({id:2,redovni:true,dan:0,datum:null,semestar:'zimski',pocetak:'13:00',kraj:'14:00'}));
            Promise.all(terminiListaPromisea).then(function(termini){
              var termin1=termini.filter(function(t){return t.pocetak==='12:00'})[0];
              var termin2=termini.filter(function(t){return t.pocetak==='13:00'})[0];

              rezervacijeListaPromisea.push(db.Rezervacija.create({id:1}).then(function(r){
                return Promise.all([r.setOsoblje(osoba1),r.setTermin(termin1),r.setSala(sala1)]).then(function(){
                  return new Promise(function(resolve,reject){resolve(r);});});
              }));
              rezervacijeListaPromisea.push(db.Rezervacija.create({id:2}).then(function(r){
                return Promise.all([r.setOsoblje(osoba3),r.setTermin(termin2),r.setSala(sala1)]).then(function(){
                  return new Promise(function(resolve,reject){resolve(r);});});
              }));
              Promise.all(rezervacijeListaPromisea).then(function(rezervacije){resolve(rezervacije);}).catch(function(err){console.log("Rezervacija greska "+err);});
            }).catch(function(err){console.log("Termin greska "+err);});
          }).catch(function(err){console.log("Sala greska "+err);});
      }).catch(function(err){console.log("Osoblje greska "+err);});
    });
}
