require('dotenv').config();
console.log(process.env.NODE_ENV) // remove this after you've confirmed it working
const express=require('express');
const bodyParser=require('body-parser');
const url=require('url');
const fs=require('fs');
const app=express();
const cacheTime = 86400000 * 30;
const db = require('./db.js');

var ukupanBrojZahtjeva=0;
var nizZahtjeva=[];

db.sequelize.sync({force:true}).then(function(){
    inicializacija().then(function(){
        console.log("Gotovo kreiranje tabela i ubacivanje pocetnih podataka!");
        //process.exit();
    });
});
function inicializacija(){
    var osobeListaPromisea=[];
    var saleListaPromisea=[];
    var terminiListaPromisea=[];
    var rezervacijeListaPromisea=[];
    //stavicemo uporedo termine i sale ili zaredom mozda vidjecemo

    return new Promise(function(resolve,reject){
      osobeListaPromisea.push(db.Osoblje.create({id:1,ime:'Neko',prezime:'Nekić',uloga:'profesor'}));
      osobeListaPromisea.push(db.Osoblje.create({id:2,ime:'Drugi',prezime:'Neko',uloga:'asistent'}));
      osobeListaPromisea.push(db.Osoblje.create({id:3,ime:'test',prezime:'test',uloga:'asistent'}));
      //promijeniti da se ubacuju jedan za drugim;ne treba ustv

      //dakle ako Promise.all(osobeListaPromisea) bude reject-an, then nema handlera za to,
      //pa se prosto kreira novi promise koji preuzima finalno stanje prvog;
      //mozda bolje drugi parametar kod then
      Promise.all(osobeListaPromisea).then(function(osobe){
          var osoba1=osobe.filter(function(o){return o.ime==='Neko'})[0];
          var osoba2=osobe.filter(function(o){return o.ime==='Drugi'})[0];
          var osoba3=osobe.filter(function(o){return o.ime==='test'})[0];

          //Promise.resolve vs new Promise(resolve)-kasnije vidjet fino, za sad sa new
          saleListaPromisea.push(db.Sala.create({id:1,naziv:'1-11'}).then(function(s){
            return s.setOsoblje(osoba1).then(function(){return new Promise(function(resolve,reject){resolve(s);});});
          }));
          saleListaPromisea.push(db.Sala.create({id:2,naziv:'1-15'}).then(function(s){
            return s.setOsoblje(osoba1).then(function(){return new Promise(function(resolve,reject){resolve(s);});});
          }));
          Promise.all(saleListaPromisea).then(function(sale){
            var sala1=sale.filter(function(s){return s.naziv==='1-11'})[0];
            var sala2=sale.filter(function(s){return s.naziv==='1-15'})[0];

            terminiListaPromisea.push(db.Termin.create({id:1,redovni:false,dan:null,datum:'01.01.2020',semestar:null,pocetak:'12:00',kraj:'13:00'}));
            terminiListaPromisea.push(db.Termin.create({id:2,redovni:true,dan:0,datum:null,semestar:'zimski',pocetak:'13:00',kraj:'14:00'}));
            Promise.all(terminiListaPromisea).then(function(termini){
              var termin1=termini.filter(function(t){return t.pocetak==='12:00'})[0];
              var termin2=termini.filter(function(t){return t.pocetak==='13:00'})[0];

              rezervacijeListaPromisea.push(db.Rezervacija.create({id:1}).then(function(r){
                return Promise.all([r.setOsoblje(osoba1),r.setTermin(termin1),r.setSala(sala1)]).then(function(){
                  return new Promise(function(resolve,reject){resolve(r);});});
              }));
              rezervacijeListaPromisea.push(db.Rezervacija.create({id:2}).then(function(r){
                return Promise.all([r.setOsoblje(osoba3),r.setTermin(termin2),r.setSala(sala1)]).then(function(){
                  return new Promise(function(resolve,reject){resolve(r);});});
              }));
              Promise.all(rezervacijeListaPromisea).then(function(rezervacije){resolve(rezervacije);}).catch(function(err){console.log("Rezervacija greska "+err);});
            }).catch(function(err){console.log("Termin greska "+err);});
          }).catch(function(err){console.log("Sala greska "+err);});
      }).catch(function(err){console.log("Osoblje greska "+err);});
    });
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/slike',express.static(__dirname+'/slike',{maxAge: cacheTime}));//ne treba nista sakriti
app.use('/style',express.static(__dirname+'/style'));
app.use('/js',express.static(__dirname+'/js'));

app.get('/',function(req,res){
  res.sendFile(__dirname+'/pocetna.html');
});
app.get('/pocetna.html',function(req,res){
  res.sendFile(__dirname+'/pocetna.html');
});
app.get('/rezervacija.html',function(req,res){
  res.sendFile(__dirname+'/rezervacija.html');
});
app.get('/sale.html',function(req,res){
  res.sendFile(__dirname+'/sale.html');
});
app.get('/testoviS2.html',function(req,res){
  res.sendFile(__dirname+'/testoviS2.html');
});
app.get('/unos.html',function(req,res){
  res.sendFile(__dirname+'/unos.html');
});
app.get('/osoblje.html',function(req,res){
  res.sendFile(__dirname+'/osoblje.html');
});
app.get('/testoviS4.html',function(req,res){
  res.sendFile(__dirname+'/testoviS4.html');
});
app.get('/zauzeca.json',function(req,res){
  res.sendFile(__dirname+'/zauzeca.json');
});
app.get('/1.png',function(req,res){
  res.sendFile(__dirname+'/1.png');
});
//bolje sa post valjda, sigurnost..
//prepraviti za predavaca u odnosu na spiralu 3
app.post('/zauzeca.json',function(req,res){
  //console.log("aaaaaaaaaaaaaaaaa");
  let tijelo=req.body,period=true;
  if(tijelo['period']=='false') period=false;
  fs.readFile(__dirname+'/zauzeca.json',function(err,data){
    //console.log("aaaaaaaaaaaaaaaaa");
    if (err) return console.error(err);

    let odg=data.toString();
    let obj=JSON.parse(odg),nalazi=false,z,per,van;
    per=obj.periodicna;
    van=obj.vanredna;

    let zDanSedmica,zDanMjesec,zMjesec,zSemestar,zDatum,zPocetak,zPocetakH,zPocetakM,zKraj,zKrajH,zKrajM,zSala,zPredavac;
    let oDanSedmica=tijelo['oDanSedmica'],oDanMjesec=tijelo['oDanMjesec'],oMjesec=tijelo['oMjesec'],oSemestar=tijelo['oSemestar'];
    let oDatum=tijelo['oDatum'],oPocetak=tijelo['oPocetak'],oKraj=tijelo['oKraj'],oSala=tijelo['oSala'],oPredavac=tijelo['oPredavac'];
    let bPocetak=oPocetak,bKraj=oKraj;
    //oDanSedmica je od 1, dok oMjesec od 0

    oPocetak=oPocetak.split(":");
    oKraj=oKraj.split(":");
    let oPocetakH=parseInt(oPocetak[0]),oPocetakM=parseInt(oPocetak[1]),oKrajH=parseInt(oKraj[0]),oKrajM=parseInt(oKraj[1]);

    //da je oPocetak<oKraj testirat cemo u browseru
    for(let i=0;i<per.length;i++){
      z=per[i];
      zDanSedmica=z.dan+1;zSemestar=z.semestar;zPocetak=z.pocetak;zKraj=z.kraj;zSala=z.naziv;zPredavac=z.predavac;
      //zDanSedmica je od 1-uvecali

      zPocetak=zPocetak.split(":");
      zKraj=zKraj.split(":");
      zPocetakH=parseInt(zPocetak[0]);
      zPocetakM=parseInt(zPocetak[1]);
      zKrajH=parseInt(zKraj[0]);
      zKrajM=parseInt(zKraj[1]);
      //mozda je bolje zbog efikasnosti ovo izvan, ali ovako manje pisanja
      //dodat pretvaranja u int-vidjet kasnije zbog automatske konverzije..
      if(period){
        if(oSala==zSala && oSemestar==zSemestar && oDanSedmica==zDanSedmica &&
          !((zKrajH<oPocetakH || zKrajH==oPocetakH && zKrajM<=oPocetakM)||(oKrajH<zPocetakH || oKrajH==zPocetakH && oKrajM<=zPocetakM))){
          nalazi=true;
          break;
        }
      }
      else{
        if(oSala==zSala && oDanSedmica==zDanSedmica &&
          !((zKrajH<oPocetakH || zKrajH==oPocetakH && zKrajM<=oPocetakM)||(oKrajH<zPocetakH || oKrajH==zPocetakH && oKrajM<=zPocetakM))&&
          (zSemestar=="zimski" && (oMjesec>=9 && oMjesec <=11 || oMjesec==0) || zSemestar=="ljetni" && oMjesec>=1 && oMjesec <=5)){
          nalazi=true;
          break;
        }
      }
    }
    //
    if(nalazi){
      oDatum=oDatum.split('.');
      res.send("Nije moguće rezervisati salu "+oSala+" za navedeni datum "+oDatum[0]+"/"+oDatum[1]+"/"+oDatum[2]+" i termin od "+bPocetak+" do "+bKraj+"!");
    }
    //
    for(let i=0;i<van.length;i++){
      z=van[i];
      zDatum=z.datum;zSemestar=z.semestar;zPocetak=z.pocetak;zKraj=z.kraj;zSala=z.naziv;zPredavac=z.predavac;
      zDatum=zDatum.split(".");
      zDanMjesec=parseInt(zDatum[0]);
      //zMjesec=parseInt(zDatum[1]);//sad ne umanjujemo,zasto sam to napisao-valjda kad sam mislio da cu samo datum slati kao parametar, treba umanjiti
      zMjesec=parseInt(zDatum[1])-1;

      zPocetak=zPocetak.split(":");
      zKraj=zKraj.split(":");
      zPocetakH=parseInt(zPocetak[0]);
      zPocetakM=parseInt(zPocetak[1]);
      zKrajH=parseInt(zKraj[0]);
      zKrajM=parseInt(zKraj[1]);

      let vzDatum=new Date(2022,zMjesec,zDanMjesec);
      zDanSedmica=vzDatum.getDay()
      if(zDanSedmica==0) zDanSedmica=7;//zDanSedmica je od 1

      if(period){
        if(oSala==zSala && oDanSedmica==zDanSedmica &&
          !((zKrajH<oPocetakH || zKrajH==oPocetakH && zKrajM<=oPocetakM)||(oKrajH<zPocetakH || oKrajH==zPocetakH && oKrajM<=zPocetakM))&&
          (oSemestar=="zimski" && (zMjesec>=9 && zMjesec <=11 || zMjesec==0) || oSemestar=="ljetni" && zMjesec>=1 && zMjesec <=5)){
          console.log('ne moze, postoji zauzece '+zDatum[0]+'.'+zDatum[1]);
          nalazi=true;
          break;
        }
      }
      else{
        if(oSala==zSala && oDanMjesec==zDanMjesec && oMjesec==zMjesec &&
          !((zKrajH<oPocetakH || zKrajH==oPocetakH && zKrajM<=oPocetakM)||(oKrajH<zPocetakH || oKrajH==zPocetakH && oKrajM<=zPocetakM))){
          nalazi=true;
          break;
        }
      }
    }
    if(nalazi){
      oDatum=oDatum.split('.');
      res.send("Nije moguće rezervisati salu "+oSala+" za navedeni datum "+oDatum[0]+"/"+oDatum[1]+"/"+oDatum[2]+" i termin od "+bPocetak+" do "+bKraj+"!");
    }
    else{
      if(period){
        let objekat={dan:oDanSedmica-1, semestar:oSemestar, pocetak:bPocetak, kraj:bKraj,naziv:oSala,predavac:oPredavac};
        obj.periodicna.push(objekat);
        let objTekst=JSON.stringify(obj);
        fs.writeFile('zauzeca.json', objTekst, function (err) {
          if (err) throw err;
          console.log('Replaced! periodicni ubacen');
          res.sendFile(__dirname+'/zauzeca.json');
        });
      }
      else{
        let objekat={datum:oDatum, pocetak:bPocetak, kraj:bKraj,naziv:oSala,predavac:oPredavac};
        obj.vanredna.push(objekat);
        let objTekst=JSON.stringify(obj);
        fs.writeFile('zauzeca.json', objTekst, function (err) {
          if (err) throw err;
          console.log('Replaced! vanredni ubacen');
          res.sendFile(__dirname+'/zauzeca.json');
        });
      }
    }
  });
});
//dakle ne trebaju niti staticke?
app.get('/slike',function(req,res){
  //console.log("sad");
  fs.readdir(__dirname+'/slike', function(err,files){
    let redniBr=parseInt(req.query.br);
    let brSlika=files.length;
    let odg;

    //vidjet jos ako pritisne sljedeci a jos se nisu ucitale zadnje slike-mozemo ukodirat u poruku pa prepravit il poslat zathyjev prije
    //for(let i=0;i<brSlika;i++) console.log(files[i]);
    if(brSlika-(redniBr+1)*3<-2) res.send('<,slike/');
    else if(brSlika-(redniBr+1)*3==-2) res.send('-2,slike/'+files[redniBr*3]);
    else if(brSlika-(redniBr+1)*3==-1) res.send('-1,slike/'+files[redniBr*3]+','+'slike/'+files[redniBr*3+1]);
    else if(brSlika-(redniBr+1)*3==0) res.send('0,slike/'+files[redniBr*3]+','+'slike/'+files[redniBr*3+1]+','+'slike/'+files[redniBr*3+2]);
    else if(brSlika-(redniBr+1)*3>0) res.send('>,slike/'+files[redniBr*3]+','+'slike/'+files[redniBr*3+1]+','+'slike/'+files[redniBr*3+2]);
  });
})

//spirala 4
app.get('/osoblje',function(req,res){
    db.Osoblje.findAll().then(function(osobe){
      res.send(JSON.stringify(osobe));
    });
});
app.get('/sale',function(req,res){
    db.Sala.findAll().then(function(sale){
      res.send(JSON.stringify(sale));
    });
});

//sad kad znam da se ocuva poredak kod promise.all mozemo sa manje koda slj fju, pa promijeniti poslije
function vratiZauzeca(){
  let odg={};
  odg.periodicna=[];
  odg.vanredna=[];
  let zauzecaListaPromisea=[];
  return db.Rezervacija.findAll().then(function(zauzeca){
    zauzeca.forEach((item, i) => {
      let zOsoba=item.getOsoblje();
      let zTermin=item.getTermin();
      let zSala=item.getSala();
      zauzecaListaPromisea.push(Promise.all([zOsoba,zTermin,zSala]).then(function(x){return new Promise(function(resolve,reject){resolve(x);});}));
    });
    return Promise.all(zauzecaListaPromisea).then(function(listaZauzeca){
      let niz=JSON.parse(JSON.stringify(listaZauzeca));
      niz.forEach((z, i) => {
        let rez={};
        z.forEach((item, i) => {
          if(item.hasOwnProperty('ime')) {rez.predavac=item.uloga+' '+item.ime+' '+item.prezime+' id: '+item.id;}
          if(item.hasOwnProperty('naziv')) rez.naziv=item.naziv;
          if(item.hasOwnProperty('redovni')) {
            rez.pocetak=item.pocetak.substr(0,5);rez.kraj=item.kraj.substr(0,5);
            if(item.redovni) {
              rez.dan=item.dan;rez.semestar=item.semestar;
              odg.periodicna.push(rez);
            }
            else{
              rez.datum=item.datum;
              odg.vanredna.push(rez);
            }
          }
        });
      });
      //console.log(odg);
      //res.send(JSON.stringify(odg));
      return new Promise(function(resolve,reject){resolve(odg);});
    });

  });
}

function dopustiUpis(idZahtjeva){
  if(nizZahtjeva.indexOf(idZahtjeva)==0) {
    return new Promise(function(resolve,reject){
      /*
      //za testiranje(sa 2 tab-a)
      if(idZahtjeva==0) setTimeout(function(){resolve(1);},50000);
      else resolve(1);
      */
      resolve(1);
    });
  }
  else {
    console.log('br zahtjeva ikad '+ukupanBrojZahtjeva+' ,redni broj zahtjeva: '+nizZahtjeva.indexOf(idZahtjeva));
    return new Promise(function(resolve,reject){
      setTimeout(function(){resolve(dopustiUpis(idZahtjeva));},50);
    });
  }
}

app.get('/zauzeca',function(req,res){
  vratiZauzeca().then(function(x){
    res.send(JSON.stringify(x));
  });
});

//prepraviti za predavaca u odnosu na spiralu 3
//mozemo dodati da postavlja ukupanBrojZahtjeva na 0(zbog prekoracenja number, iako..), ako je br trenutnih 0;
app.post('/zauzeca',function(req,res){
  //imamo nekoliko opcija:iskoristiti obj pa s tim raditi(najjednostavnije-iskoristi sa prosle spirale), ili bez kreiranja obj(malo efikasnije),
  //ili nesto od ovog + uporedo red i van...
  //+paziti, asinhronost i vise korisnika
  //kad bi bas pazili mozda bi trebali pauzirati dok se ne zavrse ajax zahtjevi ostalih korisnika(koji su prije poslali zahtjev) za upis ili porediti sa ostalim ili..
  if(nizZahtjeva.length==0) ukupanBrojZahtjeva=0;
  let idZahtjeva=ukupanBrojZahtjeva;
  ukupanBrojZahtjeva++;
  nizZahtjeva.push(idZahtjeva);
  dopustiUpis(idZahtjeva).then(function(){
    console.log('dopusten zahtjev '+idZahtjeva);
    vratiZauzeca().then(function(obj){
      let tijelo=req.body,period=true;
      if(tijelo['period']=='false') period=false;

      let nalazi=false,z,per,van;
      per=obj.periodicna;
      van=obj.vanredna;

      let zDanSedmica,zDanMjesec,zMjesec,zSemestar,zDatum,zPocetak,zPocetakH,zPocetakM,zKraj,zKrajH,zKrajM,zSala,zPredavac;
      let oDanSedmica=tijelo['oDanSedmica'],oDanMjesec=tijelo['oDanMjesec'],oMjesec=tijelo['oMjesec'],oSemestar=tijelo['oSemestar'];
      let oDatum=tijelo['oDatum'],oPocetak=tijelo['oPocetak'],oKraj=tijelo['oKraj'],oSala=tijelo['oSala'],oPredavac=tijelo['oPredavac'],oPredavacId=tijelo['oPredavacId'];
      let bPocetak=oPocetak,bKraj=oKraj;
      //oDanSedmica je od 1, dok oMjesec od 0

      oPocetak=oPocetak.split(":");
      oKraj=oKraj.split(":");
      let oPocetakH=parseInt(oPocetak[0]),oPocetakM=parseInt(oPocetak[1]),oKrajH=parseInt(oKraj[0]),oKrajM=parseInt(oKraj[1]);

      //da je oPocetak<oKraj testirat cemo u browseru
      for(let i=0;i<per.length;i++){
        z=per[i];
        zDanSedmica=z.dan+1;zSemestar=z.semestar;zPocetak=z.pocetak;zKraj=z.kraj;zSala=z.naziv;zPredavac=z.predavac;
        //zDanSedmica je od 1-uvecali

        zPocetak=zPocetak.split(":");
        zKraj=zKraj.split(":");
        zPocetakH=parseInt(zPocetak[0]);
        zPocetakM=parseInt(zPocetak[1]);
        zKrajH=parseInt(zKraj[0]);
        zKrajM=parseInt(zKraj[1]);
        //mozda je bolje zbog efikasnosti ovo izvan, ali ovako manje pisanja
        //dodat pretvaranja u int-vidjet kasnije zbog automatske konverzije..
        if(period){
          if(oSala==zSala && oSemestar==zSemestar && oDanSedmica==zDanSedmica &&
            !((zKrajH<oPocetakH || zKrajH==oPocetakH && zKrajM<=oPocetakM)||(oKrajH<zPocetakH || oKrajH==zPocetakH && oKrajM<=zPocetakM))){
            nalazi=true;
            break;
          }
        }
        else{
          if(oSala==zSala && oDanSedmica==zDanSedmica &&
            !((zKrajH<oPocetakH || zKrajH==oPocetakH && zKrajM<=oPocetakM)||(oKrajH<zPocetakH || oKrajH==zPocetakH && oKrajM<=zPocetakM))&&
            (zSemestar=="zimski" && (oMjesec>=9 && oMjesec <=11 || oMjesec==0) || zSemestar=="ljetni" && oMjesec>=1 && oMjesec <=5)){
            nalazi=true;
            break;
          }
        }
      }
      //
      if(nalazi){
        oDatum=oDatum.split(".");
        res.send("Nije moguće rezervisati salu "+oSala+" za navedeni datum "+oDatum[0]+"/"+oDatum[1]+"/"+oDatum[2]+" i termin od "+bPocetak+" do "+bKraj+"!"+
                  " Prvo preklapanje je sa rezervacijom koju je izvršio "+zPredavac+".");
        nizZahtjeva.shift();
        return;//ne treba mi promise
      }
      //
      for(let i=0;i<van.length;i++){
        z=van[i];
        zDatum=z.datum;zSemestar=z.semestar;zPocetak=z.pocetak;zKraj=z.kraj;zSala=z.naziv;zPredavac=z.predavac;
        zDatum=zDatum.split(".");
        zDanMjesec=parseInt(zDatum[0]);
        //zMjesec=parseInt(zDatum[1]);//sad ne umanjujemo,zasto sam to napisao-valjda kad sam mislio da cu samo datum slati kao parametar, treba umanjiti
        zMjesec=parseInt(zDatum[1])-1;

        zPocetak=zPocetak.split(":");
        zKraj=zKraj.split(":");
        zPocetakH=parseInt(zPocetak[0]);
        zPocetakM=parseInt(zPocetak[1]);
        zKrajH=parseInt(zKraj[0]);
        zKrajM=parseInt(zKraj[1]);

        let vzDatum=new Date(2022,zMjesec,zDanMjesec);
        zDanSedmica=vzDatum.getDay()
        if(zDanSedmica==0) zDanSedmica=7;//zDanSedmica je od 1

        if(period){
          if(oSala==zSala && oDanSedmica==zDanSedmica &&
            !((zKrajH<oPocetakH || zKrajH==oPocetakH && zKrajM<=oPocetakM)||(oKrajH<zPocetakH || oKrajH==zPocetakH && oKrajM<=zPocetakM))&&
            (oSemestar=="zimski" && (zMjesec>=9 && zMjesec <=11 || zMjesec==0) || oSemestar=="ljetni" && zMjesec>=1 && zMjesec <=5)){
            console.log('ne moze, postoji zauzece '+zDatum[0]+'.'+zDatum[1]);
            nalazi=true;
            break;
          }
        }
        else{
          if(oSala==zSala && oDanMjesec==zDanMjesec && oMjesec==zMjesec &&
            !((zKrajH<oPocetakH || zKrajH==oPocetakH && zKrajM<=oPocetakM)||(oKrajH<zPocetakH || oKrajH==zPocetakH && oKrajM<=zPocetakM))){
            nalazi=true;
            break;
          }
        }
      }
      if(nalazi){
        oDatum=oDatum.split(".");
        res.send("Nije moguće rezervisati salu "+oSala+" za navedeni datum "+oDatum[0]+"/"+oDatum[1]+"/"+oDatum[2]+" i termin od "+bPocetak+" do "+bKraj+"!"+
                  " Prvo preklapanje je sa rezervacijom koju je izvršio "+zPredavac+".");
        nizZahtjeva.shift();
        return;//ne treba mi promise
      }
      else{
        let promiseOsoba=db.Osoblje.findByPk(oPredavacId);
        let promiseSala=db.Sala.findOne({where:{naziv:oSala}});
        let promiseTermin;
        if(period) promiseTermin=db.Termin.create({redovni:true,dan:oDanSedmica-1,datum:null,semestar:oSemestar,pocetak:bPocetak,kraj:bKraj});
        else promiseTermin=db.Termin.create({redovni:false,dan:null,datum:oDatum,semestar:null,pocetak:bPocetak,kraj:bKraj});

        Promise.all([promiseOsoba,promiseSala,promiseTermin]).then(function(x){
          let rOsoba=x[0],rSala=x[1],rTermin=x[2];//bice ocuvan poredak
          db.Rezervacija.create().then(function(r){
            Promise.all([r.setOsoblje(rOsoba),r.setSala(rSala),r.setTermin(rTermin)]).then(function(){
                vratiZauzeca().then(function(x){res.send(JSON.stringify(x));nizZahtjeva.shift();});
            });
          });
        });
      }
    });
  });
});

app.post('/podaciReset',function(req,res){
  db.sequelize.sync({force:true}).then(function(){
      inicializacija().then(function(){
          console.log("Gotovo kreiranje tabela i ubacivanje pocetnih podataka!");
          res.send('podaci resetovani');
      });
  });
});

let port = process.env.PORT;
if (port == null || port == "") {
  port = 8080;
}
app.listen(port);
