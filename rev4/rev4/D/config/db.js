
const Sequelize = require("sequelize");

const sequelize = new Sequelize("DBWT19", "root", "root", {
    host: "127.0.0.1",
    dialect: "mysql",
    logging: false
    
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;


//import modela

db.osoblje = sequelize.import('../models/Osoblje.js');
db.rezervacija = sequelize.import('../models/Rezervacija.js');
db.termin = sequelize.import('../models/Termin.js');
db.sala = sequelize.import('../models/Sala.js');



//Relacija 1:m (osoblje-rezervacija)
db.osoblje.hasMany(db.rezervacija, {
    foreignKey: 'osoba',
     type: Sequelize.INTEGER
});
db.rezervacija.belongsTo(db.osoblje, {
    foreignKey: 'osoba',
     type: Sequelize.INTEGER
});

//Relacija 1:m  (sala-rezervacija)
db.sala.hasMany(db.rezervacija, {
    foreignKey: 'sala',
    type: Sequelize.INTEGER
});
db.rezervacija.belongsTo(db.sala, {
    foreignKey: 'sala', 
    type: Sequelize.INTEGER
});

//rezervacija-termin
db.termin.hasOne(db.rezervacija,{
    foreignKey: 'termin',
    unique: true,
    type: Sequelize.INTEGER
  });
db.rezervacija.belongsTo(db.termin, {
  foreignKey: 'termin',
  unique: true,
  type: Sequelize.INTEGER
});

//sala osoblje(1:1)
db.osoblje.hasOne(db.sala, {
    foreignKey: 'zaduzenaOsoba',
     type: Sequelize.INTEGER
  });
db.sala.belongsTo(db.osoblje, {
  foreignKey: 'zaduzenaOsoba',
   type: Sequelize.INTEGER
});



module.exports = db;



