const db = require('./config/db.js')
db.sequelize.sync({ force: true }).then(function () {
    inicializacija_tabela().then(function () {
        console.log("Kreirane tabele i ubaceni podaci!! ");
        // process.exit();
    });
});
function inicializacija_tabela() {
    let losoba = [];
    let lsala = [];
    let ltermina = [];
    let lrezervacija = [];
    let os1, os2, os3;
    let term1, term2;

    let o1, o2, o3;
    let ss1, ss2;
    var brojac = 0;

    return new Promise( resolve=> {
        os1 = db.osoblje.create({
            id: 1,
            ime: "Neko",
            prezime: "Nekić",
            uloga: "profesor"
        })

        losoba.push(os1);

        os2 = db.osoblje.create({
            id: 2,
            ime: "Drugi",
            prezime: "Neko",
            uloga: "asistent"
        })

        losoba.push(os2);

        os3 = db.osoblje.create({
            id: 3,
            ime: "Test",
            prezime: "Test",
            uloga: "asistent"
        })

        losoba.push(os3);
        var porukica = "spaseno je: ";
        console.log(porukica);
        //SADA TREBA PROVJERA PROMISEA
        Promise.all(losoba)
            .then((osobe) => {
                //VRATI PO Id-u pojedinacne osobe
                o1 = osobe.filter(osobice => {
                    return osobice.id == 1
                })[0];

                o2 = osobe.filter(osobice => {
                    return osobice.id == 2
                })[0];

                let lista_osoba=[];
                //nakon kreiranih svih osoba ubaciti iste u sale


                lsala.push(db.sala.create({
                    id: 1,
                    naziv: "1-11"
                })
                    .then((rezultat) => {
                        let p = rezultat;
                        console.log("rezultat : " + rezultat);
                        return rezultat.setOsoblje(o1)
                            .then(rez => {
                                return new Promise((resolve) => {
                                    resolve(rezultat);
                                    brojac++;
                                    console.log("ubacena prva sala!")
                                })
                            })
                    })
                )
                var brojac2 = 0; //broji ubacene podatke
                
                lsala.push(db.sala.create({
                    id: 2,
                    naziv: "1-15"
                }).then(rezultat => {
                    return rezultat.setOsoblje(o2)
                        .then(rez => {
                            return new Promise(resolve => {
                                console.log("ubacena druga sala!")
                                resolve(rezultat);
                                brojac++;
                            }
                            )
                        })
                })
                );

                lista_osoba.push(os1);
                lista_osoba.push(os2);
                lista_osoba.push(o3);


                //dohvatiti sale sve po id
                Promise.all(lsala).then(sale => {
                    ssala = sale.filter(rezultat => {
                        return rezultat.id == 1
                    })[0];
                    sssala = sale.filter(rezultat => {
                        return rezultat.id == 2
                    })[0];
                    let broj_dodanih_osoba= lista_osoba.length;
                   // console.log(broj_dodanih_osoba);
                    ///sad ubaciti i termine
                    ///tek onda ubaciti sve u rezervacija
                    let t1 = db.termin.create({
                        id: 1,
                        redovni: false,
                        dan: null,
                        datum: "01.01.2020",
                        semestar: null,
                        pocetak: "12:00",
                        kraj: "13:00"
                    });
                    
                    ltermina.push(t1);

                    let vrati = porukica + brojac;
                    let t2 = db.termin.create({
                        id: 2,
                        redovni: true,
                        dan: 0,
                        datum: null,
                        semestar: "zimski",
                        pocetak: "13:00",
                        kraj: "14:00"
                    })


                    ltermina.push(t2);

                    //treba i osoba tri za rezervaciju 2
                    o3 = osobe.filter(osobice => {
                        return osobice.id == 3
                    })[0];
                    brojac=0;
                    Promise.all(ltermina).then(rezultati => {
                        term1 = rezultati.filter(rezultat => {
                            // console.log(rezultat.id)
                            brojac++;
                            return rezultat.id == 1
                        })[0];

                        term2 = rezultati.filter(rezultat => {
                            brojac++;
                            console.log("ubaceno ovdje " + brojac); //provjera da li prolazi pravilan broj puta
                            return rezultat.id == 2
                        })[0];

                        lrezervacija.push(db.rezervacija.create({
                            id: 1
                        }).then(rez => {
                            return Promise.all([
                                rez.setTermin(term1),
                                rez.setSala(ssala),
                                rez.setOsoblje(o1)])
                                .then(() => {
                                    console.log("ubacena prva rezervacija! ")
                                    return new Promise(resolve => {
                                        resolve(rez);
                                    })
                                })
                        }));

                     
                        let t3 = ({
                            id: 3,
                            redovni: true,
                            dan: 0,
                            datum: null,
                            semestar: "zimski",
                            pocetak: "13:00",
                            kraj: "14:00"
                        })

                        lrezervacija.push(db.rezervacija.create({
                            id: 2
                        }).then(rez => {
                            return Promise.all([
                                rez.setTermin(term2),
                                rez.setSala(ssala),
                                rez.setOsoblje(o3)])
                                .then(() => {
                                    console.log("ubacena druga rezervacija! ")
                                    return new Promise(resolve => { 
                                        resolve(rez); 
                                    })
                                })
                        }));

                        Promise.all(lrezervacija)
                            .then(rezervacije => { 
                                resolve(rezervacije) 
                            }).then(()=>{
                                //console.log(porukica+brojac);

                                console.log("Dodani podaci !")
                            })
                    })
                    .catch(error=> { console.log(error) })
                })
                .catch(error=> { console.log(error) })
            })
            .catch(error=> { console.log(error) })
    })
}
//module.exports = baza;

