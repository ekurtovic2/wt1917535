//VRATITI PORT NA 8080..........................
const express = require('express');
const fs = require('fs');
const app = express();
var path = require('path');
const db = require('./config/db.js');
const baza = require('./baza.js')

db.sequelize.authenticate().then(() => console.log('Database connected...'))
    .catch(err => console.log(err));



//assets + css files
app.use('/public', express.static('public'));
app.use('/Assets', express.static('Assets'));
app.use('/Modules', express.static('Modules'));
app.use(express.json())

//Pomocne varijable
let ukupnoSlika = 0;
var trenutnaPozicijaNaSlikama = 0;
let jsonSlike = {};
let key = 'url';
jsonSlike[key] = [];
var sedmica_dani = ["Ponedjeljak", "Utorak", "Srijeda", "Cetvrtak", "Petak", "Subota", "Nedjelja"];


function poklapanjeVremena1(pocetak1, kraj1, pocetak2, kraj2) {

    //pocetak 1
    let pocetak1_p = pocetak1.split(":");
    let pocetak1_s = parseInt(pocetak1_p[0]);
    let pocetak1_m = parseInt(pocetak1_p[1]);

    //kraj1
    let kraj1_p = kraj1.split(":");
    let kraj1_s = parseInt(kraj1_p[0]);
    let kraj1_m = parseInt(kraj1_p[1]);

    //pocetak2
    let pocetak2_p = pocetak2.split(":");
    let pocetak2_s = parseInt(pocetak2_p[0]);
    let pocetak2_m = parseInt(pocetak2_p[1]);

    //kraj2
    let kraj2_p = kraj2.split(":");
    let kraj2_s = parseInt(kraj2_p[0]);
    let kraj2_m = parseInt(kraj2_p[1]);

    //if (pocetak1 > kraj1) return false;
    if (pocetak1_s > kraj1_s || (pocetak1_s == kraj1_s) && pocetak1_m > kraj1_m) {
        return false;
    }


    //if (pocetak1 <= pocetak2 && kraj1 >= kraj2) return true;
    if ((pocetak1_s < pocetak2_s || (pocetak1_s == pocetak2_s && pocetak1_m <= pocetak2_m))
        && (kraj1_s > kraj2_s || (kraj1_s == kraj2_s && kraj1_m >= kraj2_m))) {
        return true;
    }

    //else if (pocetak1 >= pocetak2 && pocetak1 <= kraj2) return true;
    else if ((pocetak1_s > pocetak2_s || (pocetak2_s === pocetak1_s && pocetak1_m >= pocetak2_m))
        &&
        (pocetak1_s < kraj2_s || (pocetak1_s == kraj2_s && pocetak1_m < kraj2_m))) {

        return true;
    }
    // else if (pocetak2 <= kraj1 && kraj2 >= kraj1) return true;
    else if ((pocetak2_s < kraj1_s || (pocetak2_s == kraj1_s && pocetak2_m <= kraj1_m)) &&
        (kraj1_s < kraj2_s || (kraj1_s == kraj2_s && kraj1_m <= kraj2_m))) {

        return true;
    }
    else return false;
}


//express routes

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'Pocetna/pocetna.html'));
})
app.get('/Pocetna/pocetna.html', function (req, res) {
    res.sendFile(path.join(__dirname, 'Pocetna/pocetna.html'));
})
app.get('/Sale/sale.html', function (req, res) {
    res.sendFile(path.join(__dirname, '/Sale/sale.html'));
})
app.get('/Unos/unos.html', function (req, res) {
    res.sendFile(path.join(__dirname, '/Unos/unos.html'));
})
app.get('/Rezervacija/rezervacija.html', function (req, res) {
    res.sendFile(path.join(__dirname, '/Rezervacija/rezervacija.html'));
})

app.get('/Osoblje/osoblje.html', function (req, res) {
    res.sendFile(path.join(__dirname, '/Osoblje/osoblje.html'));
})
app.get('/get/osoblje', function (req, res) {

    db.osoblje.findAll().then(rezultat => {
        res.json(rezultat);
    }).catch(error => res.send("Greška prilikom čitanja iz baze!"));

});

app.get('/slika', function (req, res) {
    fs.readFile('./Modules/slike.json', (error, data) => {
        if (error) throw error;
        else {

            jsonSlike[key] = [];

            if (0 === parseInt(req.query.strana))
                trenutnaPozicijaNaSlikama = 0;
            let object = JSON.parse(JSON.stringify(JSON.parse(data)));

            ukupnoSlika = object.slike.length;
            var razlika = ukupnoSlika - trenutnaPozicijaNaSlikama;
            if (razlika > 3) {
                jsonSlike[key].push(object.slike[trenutnaPozicijaNaSlikama].src.toString());
                jsonSlike[key].push(object.slike[trenutnaPozicijaNaSlikama + 1].src.toString());
                jsonSlike[key].push(object.slike[trenutnaPozicijaNaSlikama + 2].src.toString());
                trenutnaPozicijaNaSlikama += 3;
            }
            else {
                jsonSlike[key] = [];
                for (let i = 0; i < razlika; i++) {
                    jsonSlike[key].push(object.slike[trenutnaPozicijaNaSlikama + i].src.toString());
                }
            }
            res.setHeader('Content-Type', 'application/json');
            res.json(jsonSlike);
        }
    })

})

app.get('/ukupanBrojSlika', function (req, res) {
    fs.readFile('./Modules/slike.json', (error, data) => {
        if (error) throw error;
        else {

            let object = JSON.parse(JSON.stringify(JSON.parse(data)));

            ukupnoSlika = object.slike.length;

            let jsonObjekat = {};
            let kljuc = 'brojSlika';
            jsonObjekat[kljuc] = ukupnoSlika;
            res.setHeader('Content-Type', 'application/json');
            res.json(jsonObjekat);

        }


    })

});

   
   
app.post('/post/rezervacija', function (req, res) {
    let id_osobe;
    let nesto = {};
    let periodicne = [];
    let vanredne = [];
    let vanredna_u_periodicnoj = [];
    let periodicna_u_vanrednoj =[];
    let greska = false;


    db.rezervacija.findAll({
        include: [{
            model: db.termin
        }]
    }).then(rezervacije => {
        //treba proci kroz sve redove i izdvojiti u nizove (response.periodicna i response.vanredna)

        if (req.body.redovni) {

            periodicne = rezervacije.filter((rezervacija) => {
                return rezervacija.Termin.redovni === true && rezervacija.sala == req.body.naziv && rezervacija.Termin.dan === req.body.dan && req.body.semestar == rezervacija.Termin.semestar && poklapanjeVremena1(rezervacija.Termin.pocetak, rezervacija.Termin.kraj, req.body.pocetak, req.body.kraj);
            });
            periodicna_u_vanrednoj = rezervacije.filter((rezervacija) => {
                if(rezervacija.Termin.redovni==false){
                let dan_za_rezervaciju = new Date(dajDay(rezervacija.Termin.datum).datum);
                dan_za_rezervaciju=dan_za_rezervaciju.getDay()-1;
                if(dan_za_rezervaciju==-1) dan_za_rezervaciju=6;
                return  dan_za_rezervaciju==req.body.dan && rezervacija.sala == req.body.naziv && dan_za_rezervaciju ==req.body.dan && poklapanjeVremena1(rezervacija.Termin.pocetak, rezervacija.Termin.kraj, req.body.pocetak, req.body.kraj);
            }});
            if (periodicne.length) { greska = true; id_osobe = periodicne[0].osoba }
            else if(periodicna_u_vanrednoj.length) { greska = true; id_osobe = periodicna_u_vanrednoj[0].osoba }
        }
        else {
            vanredne = rezervacije.filter((rezervacija) => {
                return rezervacija.Termin.redovni === false && rezervacija.Termin.datum === req.body.datum && rezervacija.sala == req.body.naziv && poklapanjeVremena1(rezervacija.Termin.pocetak, rezervacija.Termin.kraj, req.body.pocetak, req.body.kraj);

            });
            let datum_rezervacije = new Date(dajDay(req.body.datum).datum);
            let dan_rezervacije = datum_rezervacije.getDay() - 1;
            if(dan_rezervacije==-1) dan_rezervacije = 6;
            
            vanredna_u_periodicnoj = rezervacije.filter((rezervacija) => {
                    let semestar_rezervacije = dajDay(req.body.datum).semestar;
                    return  semestar_rezervacije== rezervacija.Termin.semestar && rezervacija.Termin.redovni==true && rezervacija.Termin.dan == dan_rezervacije && rezervacija.sala == req.body.naziv && poklapanjeVremena1(rezervacija.Termin.pocetak, rezervacija.Termin.kraj, req.body.pocetak, req.body.kraj);
            });

            if (vanredne.length) { greska = true; id_osobe = vanredne[0].osoba }
            else if  (vanredna_u_periodicnoj.length) { greska = true; id_osobe = vanredna_u_periodicnoj[0].osoba }

        }
    }).then(tr => {
        if (greska == false) {
            let trazena_sala;
            db.sala.findOne({ where: { id: req.body.naziv } }).then(sala1 => {
                trazena_sala = sala1.id;
                const noviTermin = db.termin.build({
                    redovni: req.body.redovni,
                    dan: req.body.dan,
                    datum: req.body.datum,
                    semestar: req.body.semestar,
                    pocetak: req.body.pocetak,
                    kraj: req.body.kraj
                }).save().then(x => {
                    const novaRezervacija = db.rezervacija.build({
                        termin: x.id,
                        sala: trazena_sala,
                        osoba: req.body.predavac
                    }).save().then(f => {
                        //////////ovdje uspjesno
                        db.rezervacija.findAll({
                            include: [{
                                model: db.termin
                            }]
                        }).then(rezervacije => {
                            const response = {};
                            response.periodicna = rezervacije.filter((rezervacija) => {
                                return rezervacija.Termin.redovni === true;
                            });
                            response.vanredna = rezervacije.filter((rezervacija) => {
                                return rezervacija.Termin.redovni === false;
                            });
                            sve_rezervacije = response;
                            res.json(response);
                        });

                    });
                })
            });

        }

        else if (greska) { //vrati id osobe koja je zauzela

            db.osoblje.findOne({ where: { id: id_osobe } }).then(osoba => {
                greska = false;
                nesto.osoba_detalji = osoba;
                nesto.sveRezervacije = sve_rezervacije;

                res.json(nesto);
            })
        }
    })
});

app.get('/get/zauzeca', function (req, res) {

    db.rezervacija.findAll({
        include: [{
            model: db.termin
        }]
    }).then(rezervacije => {
        //treba proci kroz sve redove i izdvojiti u nizove (response.periodicna i response.vanredna)
        const response = {};
        response.periodicna = rezervacije.filter((rezervacija) => {
            return rezervacija.Termin.redovni === true;
        });
        response.vanredna = rezervacije.filter((rezervacija) => {
            return rezervacija.Termin.redovni === false;
        });

        res.json(response);
    });
});

app.get('/dajPodatkeJson', function (req, res) {
    db.rezervacija.findAll({
        include: [{
            model: db.termin
        }]
    }).then(rezervacije => {
        const response = {};
        response.periodicna = rezervacije.filter((rezervacija) => {
            return rezervacija.Termin.redovni === true;
        });
        response.vanredna = rezervacije.filter((rezervacija) => {
            return rezervacija.Termin.redovni === false;
        });

        res.json(response);
    });

});

app.get('/osoblje/sala',function(req,res){

    let pomocna=[];
    let osobljee=[];
    let salee =[];
        db.rezervacija.findAll({
            include: [{
                model: db.termin
            }]
        }).then(rezervacije => {
            let trenutni=new Date();
            let datum_sadasnji = dajSadasnjiDatum(trenutni);
            let vrijeme_sadasnje = dajSadasnjeVrijeme(trenutni);
            let dan_sadasnji = trenutni.getDay() - 1;
            if(dan_sadasnji==-1) dan_rezervacije = 6;
            let sadasnji_semestar = trenutni.getMonth();
            if(sadasnji_semestar==0 || sadasnji_semestar>=9) sadasnji_semestar="zimski"
            else if (sadasnji_semestar>=1 && sadasnji_semestar<7) sadasnji_semestar="ljetni";
            
            
            pomocna = rezervacije.filter((rezervacija) => {
                if(rezervacija.Termin.redovni==false){
                return datum_sadasnji==rezervacija.Termin.datum && preklapanje(rezervacija.Termin.pocetak,rezervacija.Termin.kraj,vrijeme_sadasnje);
            }
            else{
                //mora se poklopiti dan i semestar sadasnji i vrijeme
                return dan_sadasnji==rezervacija.Termin.dan && sadasnji_semestar==rezervacija.Termin.semestar && preklapanje(rezervacija.Termin.pocetak,rezervacija.Termin.kraj,vrijeme_sadasnje);
           
            }
            
        });
          
        }).then(r=>{
            //posalji i osoblje
            db.osoblje.findAll({
                attributes: ['id','ime', 'prezime']
            }).then(rezultat => {
                rezultatt=JSON.parse(JSON.stringify(rezultat));
                for(let t=0;t<rezultatt.length;t++)
                osobljee.push(rezultatt[t]);
                
            
            })
        .then(rez=>{
            //dohvati sve sale
            db.sala.findAll().then(c=>{
                rezultatt1=JSON.parse(JSON.stringify(c));
                for(let t=0;t<rezultatt1.length;t++)
                salee.push(rezultatt1[t]);
            })
        .then(zadnji=>{
            // za svaku osobu izdvoji termin i nadji salu
            let response={};
            let vratiObjekat=[];
            let moje_ime ="";
            for(let os=0;os<osobljee.length;os++){
                let nadjeno=false;
                let moja_sala="u kancelariji";
                for(let term=0;term<pomocna.length;term++){
                    if (pomocna[term].osoba==osobljee[os].id){
                        nadjeno=true;
                        for(let salica=0;salica<salee.length;salica++){
                            if(salee[salica].id==pomocna[term].sala){
                                moja_sala=salee[salica].naziv;
                                break;
                            }
                        }
                        break;

                    }
                }
                //kreiraj objekat
                let ubaci={};
                ubaci.ime_osobe=osobljee[os].ime + " "+ osobljee[os].prezime
                ubaci.u_sali=moja_sala;
                vratiObjekat.push(ubaci);


            }
            response.odgovor=vratiObjekat;
            res.json(response);
        })
    })
        
})
})



function dajDay(datum1) {
    let tmp= datum1.split(".");
    let objekat={};
    objekat.datum=tmp[2]+"-"+tmp[1]+"-"+tmp[0];
    let mjesec2 =tmp[1];
    let sem="nema";
    if(mjesec2=="01" || mjesec2=="10" || mjesec2=="11" || mjesec2=="12") sem="zimski";
    else if(mjesec2=="02" || mjesec2=="03" || mjesec2=="04" || mjesec2=="05" || mjesec2=="06") sem="ljetni";
    objekat.semestar=sem;
    return objekat;

}
function dajSadasnjiDatum(t){
    let dayy=t.getDate();
    if(dayy<10)dayy="0"+dayy; //ovo je day

    //month
    let month=t.getMonth()+1;
    if(month<10) month="0"+month;

    return dayy+"."+month+"."+t.getFullYear();
    
}
function dajSadasnjeVrijeme(t){
    let sati=t.getHours();
    if(sati<10) sati="0"+sati;
    let minutice=t.getMinutes();
    if(minutice<10) minutice="0"+minutice;
    return sati+":"+minutice;
}
function preklapanje(pocetak,kraj,sada){
pocet=pocetak.split(":");
kra=kraj.split(":");
sad=sada.split(":");

if(pocet[0]==sad[0] && pocet[1]==sad[1])return true; 
else if(kra[0]==sad[0] && kra[1]==sad[1]) return true; 
//minute vece od pocetka, sat isti
else if(pocet[0]==sad[0] && pocet[1]<sad[1] && sad[0]<kra[0] ) return true; 
else if(pocet[0]==sad[0] && pocet[1]<sad[1] && sad[0]==kra[0] && sad[1]<kra[1] ) return true; 

else if(pocet[0]<sad[0] && kra[0]==sad[0] && kra[1]>sad[1]) return true; 
else if(pocet[0]==sad[0] && pocet[1]<sad[1] && kra[0]==sad[0] && kra[1]>sad[1]) return true; 

//sati veci od pocetka, manji od kraaja
else if(pocet[0]<sad[0] && kra[0]>sad[0]) return true; 
return false;
}
app.listen(8080);




