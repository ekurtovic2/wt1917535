// Event listener + pozivi

var prikazaniMjesec,divKalendar,btn_next,btn_previous,niz_sala,pocetak_termina,kraj_termina;

var checkbox;

var divOsoblje=document.getElementById("osobe");

divKalendar=document.getElementById("calendar");
checkbox=document.getElementById("checkbox-periodicna");

Kalendar.iscrtajKalendar(divKalendar,prikazaniMjesec);

//dugmadi,elementi i eventi

btn_next=document.getElementById("next-month");
btn_previous=document.getElementById("previous-month");


if(prikazaniMjesec==11) {btn_next.disabled=true;}
if(prikazaniMjesec==0) {btn_previous.disabled=true;}

niz_sala=document.getElementById("sale");

pocetak_termina=document.getElementById("pocetak");
kraj_termina=document.getElementById("kraj");


var  p_tekst,k_tekst;

//pocetak termina

pocetak_termina.addEventListener("change",function(ev){
  p_tekst=pocetak_termina.value.split(":");
  p_tekst=p_tekst[0]+":"+p_tekst[1];
  if(typeof(k_tekst)!="undefined")
    Kalendar.obojiZauzeca(divKalendar,prikazaniMjesec,naziv_sale,p_tekst,k_tekst);
});
//kraj termina

kraj_termina.addEventListener("change",function(ev){
  k_tekst=kraj_termina.value.split(":");
  k_tekst=k_tekst[0]+":"+k_tekst[1];
  if(typeof(p_tekst)!="undefined")
    Kalendar.obojiZauzeca(divKalendar,prikazaniMjesec,naziv_sale,p_tekst,k_tekst);
});
//sljedeci i prethodni dugmici
btn_next.addEventListener("click",function(ev){
  prikazaniMjesec++;
  Kalendar.iscrtajKalendar(divKalendar,prikazaniMjesec);
  if(prikazaniMjesec==11) {btn_next.disabled=true;}
  if(prikazaniMjesec!=0) {btn_previous.disabled=false;}
    
  oboji();
});
btn_previous.addEventListener("click",function(ev){
  prikazaniMjesec--;
  Kalendar.iscrtajKalendar(divKalendar,prikazaniMjesec);
  
  if(prikazaniMjesec!=11) {btn_next.disabled=false;}
  if(prikazaniMjesec==0) {btn_previous.disabled=true;}
  oboji();
});

//naziv sale

niz_sala.addEventListener("change",function(ev){
  naziv_sale=niz_sala.options[niz_sala.selectedIndex].id;
  oboji();
});


divOsoblje.addEventListener("change",function(ev){
  id_predavaca=divOsoblje.options[divOsoblje.selectedIndex].id;
  oboji();
  
});

function oboji (){
   if(typeof(p_tekst)!="undefined" && typeof(k_tekst)!="undefined")
      Kalendar.obojiZauzeca(divKalendar,prikazaniMjesec,naziv_sale,p_tekst,k_tekst);
}

