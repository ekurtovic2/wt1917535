

prikazaniMjesec = new Date().getMonth();

var sedmica_dani = ["Ponedjeljak","Utorak", "Srijeda", "Cetvrtak","Petak","Subota","Nedjelja"];
var naziv_sale="1";
var id_predavaca = "1";

var godina_trenutna= new Date().getFullYear();
let periodicna_zauzeca = [];
let vanredna_zauzeca = [];
let osobe = [];

let Kalendar = (function () {
  let filler;
  let current_date;
  let months = ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", "August", "Septembar", "Oktobar", "November", "December"];
  let nmb_days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  
  regex_vanredna_datum = /^[0-9][0-9]\.[0-9][0-9]\.2019$/;
  regex_vanredna_vrijeme = reg = /^[0-9][0-9]:[0-9][0-9]$/
  regex_periodicna = /^[0-9][0-9]:[0-9][0-9]$/;


  function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj) {
    
    iscrtajKalendarImpl(kalendarRef, mjesec);
    
    //postoji li u vanrednom
    let postoji = false;
    let datumV;
    let dan;
    
    for (let j = 0; j < vanredna_zauzeca.length; j++) {
      postoji = false;
      if (poklapanjeVremena(pocetak, kraj, vanredna_zauzeca[j].Termin.pocetak, vanredna_zauzeca[j].Termin.kraj) && vanredna_zauzeca[j].sala == sala ) {
        
        let dV = vanredna_zauzeca[j].Termin.datum.split(".");
        let dd = dV[1];
        if(parseInt(dV[2]) === godina_trenutna){
        d = parseInt(dd);
        if (dd - 1 === mjesec) {
          postoji = true;
          datumV = vanredna_zauzeca[j].Termin.datum.split(".");
          dan = datumV[0];
          dan = parseInt(dan);
          if (postoji) {
            //nadji taj element i oboji ga
            let parent = kalendarRef.getElementsByClassName("date-grid")[0];
            if (filler + dan - 1 >= 0) {
              let childrenn = parent.children[filler + dan -1];
              let slobodna = childrenn.children[1];
              slobodna.className = "zauzeta";
              obojeno_vanredno = true;
            }
          }
        }
      }

        
      }

    }
    //postoji li u periodicnom
    let parent = kalendarRef.getElementsByClassName("date-grid")[0];
    let elementi_datum = parent.children;

    for (let j = 0; j < periodicna_zauzeca.length; j++) {
      postoji = false;
     //console.log(poklapanjeVremena(pocetak, kraj, periodicna_zauzeca[j].Termin.pocetak, periodicna_zauzeca[j].Termin.kraj) && periodicna_zauzeca[j].sala == sala)
      if (poklapanjeVremena(pocetak, kraj, periodicna_zauzeca[j].Termin.pocetak, periodicna_zauzeca[j].Termin.kraj) && periodicna_zauzeca[j].sala == sala  ) {
         

        for (let p = periodicna_zauzeca[j].Termin.dan; p < parent.children.length; p += 7) {
          if (elementi_datum[p].className === "date-grid-element") {
            

            if (periodicna_zauzeca[j].Termin.semestar === "zimski" && (mjesec === 0 || mjesec > 8)) {
              let slobodna = elementi_datum[p].children[1];
              slobodna.className = "zauzeta";
              obojeno_periodicno = true;
            }

            else if (periodicna_zauzeca[j].Termin.semestar === "ljetni" && (mjesec > 0 && mjesec < 6)) {
              let slobodna = elementi_datum[p].children[1];
              slobodna.className = "zauzeta";
              obojeno_periodicno = true;
            }


          }
        }




      }
    }
    
  }
  function ucitajPodatkeImpl(periodicna, vanredna) {
    vanredna_zauzeca = vanredna;
    periodicna_zauzeca = periodicna;
    
  }
  function iscrtajKalendarImpl(kalendarRef, mjesec) {
    

    let c_month = mjesec;
    current_date = new Date();

    ///Ispisivanje naziva mjeseca
    document.getElementById("month-indicator").innerHTML = months[c_month];

    //brisanje grida sa datumima ukoliko je vec ucitan
    el = kalendarRef.getElementsByClassName("second-grid");
    if (el.length != 0) kalendarRef.removeChild(el[0]);

    //Iscrtavanje grida sa datumima
    let second_grid = document.createElement("div");
    second_grid.className = "second-grid";
    second_grid.id = "second-grid";
    kalendarRef.appendChild(second_grid);

    let date_grid = document.createElement("div");
    date_grid.className = "date-grid";
    date_grid.id = "date-grid";
    document.getElementById("second-grid").appendChild(date_grid);

    //iscrtavanje datuma
    //Dohvatiti broj dana mjeseca
    let num_days = nmb_days[c_month];
    //Odrediti dan prvog u mjesecu
    let prvi = getFirstDay(c_month);
    let i, j, granica;
    //Pomjeriti prvi na odgovarajuci dan
    if (prvi === 0) granica = 6;
    else if (prvi === 1) granica = 0;
    else if (prvi === 2) granica = 1;
    else if (prvi === 3) granica = 2;
    else if (prvi === 4) granica = 3;
    else if (prvi === 5) granica = 4;
    else granica = 5;

    //Određivanje tekuće sedmice (pon i ned date)

    let current_week_mon, current_week_sun;
    if (current_date.getDay === 0)
      current_week_mon = current_date.getDate() - 6;
    else if (current_date.getDay === 1) current_week_mon = current_date.getDate();
    else if (current_date.getDay === 2) current_week_mon = current_date.getDate() - 1;
    else if (current_date.getDay === 3) current_week_mon = current_date.getDate() - 2;
    else if (current_date.getDay === 4) current_week_mon = current_date.getDate() - 3;
    else if (current_date.getDay === 5) current_week_mon = current_date.getDate() - 4;
    else current_week_mon = current_date.getDate() - 5;
    current_week_sun = current_week_mon + 6;

    prva_sedmica = false;

    let dan_prvog_u_mjesecu = getFirstDay(current_date.getMonth());
    if (current_date.getDate() < 6 && dan_prvog_u_mjesecu !== 1) {
      prva_sedmica = true;

      if (getFirstDay(current_date.getMonth()) === 0) br_datuma = 1;
      else if (dan_prvog_u_mjesecu === 2) br_datuma = 6;
      else if (dan_prvog_u_mjesecu === 3) br_datuma = 5;
      else if (dan_prvog_u_mjesecu === 4) br_datuma = 4;
      else if (dan_prvog_u_mjesecu === 5) br_datuma = 3;
      else { br_datuma = 2; }

    }
    filler = granica;
    for (j = 0; j < granica; j++) {

      var divv = document.createElement("div");
      divv.className = "prazan_div";
      document.getElementById("date-grid").appendChild(divv);
    }
    for (i = 1; i <= num_days; i++) {

      let grid_element = document.createElement("div");
      grid_element.className = "date-grid-element";
      grid_element.id = i;
     
      
      let grid_date = document.createElement("div");
      grid_date.className = "date";
      grid_element.appendChild(grid_date);
      grid_date.innerHTML = i;

      let grid_date_slobodna = document.createElement("div");
      grid_date_slobodna.className = "slobodna";
      grid_element.appendChild(grid_date_slobodna);

      grid_element.addEventListener("click", (ev)=>{
        let djeca= grid_element.children[1].className;
        if(naziv_sale!== "undefined" && typeof(p_tekst)!== "undefined" && typeof(k_tekst)!=="undefined" && djeca!=="zauzeta" && ValidanTermin(p_tekst,k_tekst) ){
          let mjes = c_month + 1;
          let zauzmi_string
          if(mjes<10) mjes= "0" + mjes;
          if(grid_element.id<10)
          {zauzmi_string = "0"+grid_element.id + "."+ mjes + "." + current_date.getFullYear();}
          else{
             zauzmi_string = grid_element.id + "."+ mjes + "." + current_date.getFullYear();
          }
          let button_ok = confirm("Da li želite rezervisati " + zauzmi_string+". ?");
          
          
          if (button_ok && checkbox.checked == false){
            pozoviVanredna(zauzmi_string);
          }
          else if (button_ok && checkbox.checked==true){
            let upisiSemestar="";
            let sadasnji_datum = new Date (godina_trenutna,c_month,parseInt(grid_element.id),0,0,0,0);
            dan_sedmica =sadasnji_datum.getDay()-1;
            if(dan_sedmica==-1) dan_sedmica =6;
            if (c_month == 0 || c_month>8){
              upisiSemestar="zimski";
              pozoviPeriodicna(upisiSemestar,dan_sedmica);
            }
            else if (c_month>=1 && c_month<6) {
              upisiSemestar="ljetni";
              pozoviPeriodicna(upisiSemestar,dan_sedmica);
            }
            else{
              console.log(c_month);
              alert("U ovom mjesecu nije moguće napraviti periodicnu rezervaciju! ")
            }
          }
        }
      });

      document.getElementById("date-grid").appendChild(grid_element);

    }

  }
  function getFirstDay(c_month1,) {
    var FirstDay = new Date(godina_trenutna, c_month1, 1);
    return FirstDay.getDay();
  }
  return {
    obojiZauzeca: obojiZauzecaImpl,
    ucitajPodatke: ucitajPodatkeImpl,
    iscrtajKalendar: iscrtajKalendarImpl
  }
}());
////////////////////////////////////////////////////
////////////////////////////////////////////////////
function poklapanjeVremena(pocetak1, kraj1, pocetak2, kraj2) {

  //pocetak 1
  let pocetak1_p = pocetak1.split(":");
  let pocetak1_s = parseInt(pocetak1_p[0]);
  let pocetak1_m = parseInt(pocetak1_p[1]);

  //kraj1
  let kraj1_p = kraj1.split(":");
  let kraj1_s = parseInt(kraj1_p[0]);
  let kraj1_m = parseInt(kraj1_p[1]);

  //pocetak2
  let pocetak2_p = pocetak2.split(":");
  let pocetak2_s = parseInt(pocetak2_p[0]);
  let pocetak2_m = parseInt(pocetak2_p[1]);

  //kraj2
  let kraj2_p = kraj2.split(":");
  let kraj2_s = parseInt(kraj2_p[0]);
  let kraj2_m = parseInt(kraj2_p[1]);

  //if (pocetak1 > kraj1) return false;
  if (pocetak1_s > kraj1_s || (pocetak1_s == kraj1_s) && pocetak1_m > kraj1_m) {
    return false;
  }


  //if (pocetak1 <= pocetak2 && kraj1 >= kraj2) return true;
  if ((pocetak1_s < pocetak2_s || (pocetak1_s == pocetak2_s && pocetak1_m <= pocetak2_m))
    && (kraj1_s > kraj2_s || (kraj1_s == kraj2_s && kraj1_m >= kraj2_m))) {
    return true;
  }

  //else if (pocetak1 >= pocetak2 && pocetak1 <= kraj2) return true;
  else if ((pocetak1_s > pocetak2_s || (pocetak2_s === pocetak1_s && pocetak1_m >= pocetak2_m))
    &&
    (pocetak1_s < kraj2_s || (pocetak1_s == kraj2_s && pocetak1_m < kraj2_m))) {

    return true;
  }
  // else if (pocetak2 <= kraj1 && kraj2 >= kraj1) return true;
  else if ((pocetak2_s < kraj1_s || (pocetak2_s == kraj1_s && pocetak2_m <= kraj1_m)) &&
    (kraj1_s < kraj2_s || (kraj1_s == kraj2_s && kraj1_m <= kraj2_m))) {

    return true;
  }
  else return false;
}

function ValidanTermin(pocetak,kraj){

  
  let pocetak2_p = pocetak.split(":");
  let pocetak2_s = parseInt(pocetak2_p[0]);
  let pocetak2_m = parseInt(pocetak2_p[1]);

  //kraj2
  let kraj2_p = kraj.split(":");
  let kraj2_s = parseInt(kraj2_p[0]);
  let kraj2_m = parseInt(kraj2_p[1]);
  if (pocetak2_s > kraj2_s || (pocetak2_s == kraj2_s && pocetak2_m >= kraj2_m)) {
    return false;
  }
  return true;
}