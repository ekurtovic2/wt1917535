

let pozivi = (function () {
    // neki privatni atributi

    function dohvatiOsobljeImpl() {
        var ajax_osoblje = new XMLHttpRequest();
        let url_ucitaj_osoblje = "http://localhost:8080/get/osoblje";
        ajax_osoblje.onreadystatechange = function () {
            if (ajax_osoblje.status == 200) {
                if (ajax_osoblje.readyState == 4) {
                    //ucitaj podatke u globalnu varijablu i display ih
                    json_podaci = JSON.parse(ajax_osoblje.responseText);

                    for (var z = 0; z < json_podaci.length; z++) {
                        osobe.push(json_podaci[z].ime + " " + json_podaci[z].prezime);

                        let option = document.createElement("option");
                        option.text = json_podaci[z].ime + " " + json_podaci[z].prezime;
                        option.id = json_podaci[z].id;//vise osoba s istim imenom i prezimenom..
                        divOsoblje.add(option);

                    }



                }
            }

            if (ajax_osoblje.readyState == 4 && ajax_osoblje.status == 404) { console.log("Greska: nepoznat URL"); }
        }


        ajax_osoblje.open("GET", url_ucitaj_osoblje, true);
        ajax_osoblje.setRequestHeader("Content-Type", "application/json");
        ajax_osoblje.send();


    }

    function ucitajPodatkeImpl() {
        var ajax_ucitaj = new XMLHttpRequest();
        let url_ucitajPodatke = "http://localhost:8080/get/zauzeca";
        ajax_ucitaj.onreadystatechange = function () {
            if (ajax_ucitaj.status == 200) {
                if (ajax_ucitaj.readyState == 4) {
                    json_podaci = JSON.parse(ajax_ucitaj.responseText);
                    Kalendar.ucitajPodatke(json_podaci.periodicna, json_podaci.vanredna);

                }
            }

            if (ajax_ucitaj.readyState == 4 && ajax_ucitaj.status == 404) { console.log("Greska: nepoznat URL"); }
        }


        ajax_ucitaj.open("GET", url_ucitajPodatke, true);
        ajax_ucitaj.setRequestHeader("Content-Type", "application/json");
        ajax_ucitaj.send();

    }

    function getPicsImpl() {
        var ajax_ucitajSlike = new XMLHttpRequest();
        let url_ucitajSlike = "http://localhost:8080/slika";
        let params = "strana=" + broj_stranice;
        ajax_ucitajSlike.onreadystatechange = function () {
            if (ajax_ucitajSlike.status == 200) {
                if (ajax_ucitajSlike.readyState == 4) {
                    let resObjekat = JSON.parse(ajax_ucitajSlike.responseText);
                    //   slika = ajax_ucitajSlike.responseText.split(",");
                    let container = document.createElement("div");
                    container.className = "grid-container";
                    container.id = "grid-container" + broj_stranice;
                    document.getElementById("sadrzaj").appendChild(container);

                    for (let i = 0; i < resObjekat.url.length; i++) {
                        let b = document.createElement("img");
                        b.className = "grid-img";
                        b.src = resObjekat.url[i];
                        document.getElementById("grid-container" + broj_stranice).appendChild(b);
                    }



                }
            }

            if (ajax_ucitajSlike.readyState == 4 && ajax_ucitajSlike.status == 404) { console.log("Greska: nepoznat URL"); }
        }


        ajax_ucitajSlike.open("GET", url_ucitajSlike + "?" + params, true);
        ajax_ucitajSlike.send();


    }

    function getSveImpl() {
        var ajax_ucitajSlike = new XMLHttpRequest();
        let url_dajSlike = "http://localhost:8080/ukupanBrojSlika";
        ajax_ucitajSlike.onreadystatechange = function () {
            if (ajax_ucitajSlike.status == 200) {
                if (ajax_ucitajSlike.readyState == 4) {
                    brojslika = parseInt(JSON.parse(ajax_ucitajSlike.responseText).brojSlika, 10)
                    // brojslika = parseInt(ajax_ucitajSlike.responseText, 10);

                    if (brojslika % 3 !== 0) {
                        brojStr = Math.floor(brojslika / 3) + 1
                    }
                    else {
                        brojStr = Math.floor(brojslika / 3);
                    }
                    if (brojStr !== 1)
                        sljedeca.disabled = false;

                }

            }
        }

        if (ajax_ucitajSlike.readyState == 4 && ajax_ucitajSlike.status == 404) { console.log("Greska: nepoznat URL"); }



        ajax_ucitajSlike.open("GET", url_dajSlike, true);
        ajax_ucitajSlike.send();

    }

    function upisi_zauzeceVanrednoImpl(datum_string) {
        let JSONObj = {
            "redovni": false,
            "dan": null,
            "datum": datum_string,
            "semestar": null,
            "pocetak": p_tekst,
            "kraj": k_tekst,
            "naziv": naziv_sale,
            "predavac": id_predavaca
        };
        let xhttp = new XMLHttpRequest();
        let url_post = "/post/rezervacija";
        xhttp.onreadystatechange = function () {
            if (xhttp.status == 200) {
                if (xhttp.readyState == 4) {

                    //popraviti nanize
                    json_podaci = JSON.parse(xhttp.responseText);

                    if (json_podaci.hasOwnProperty('osoba_detalji')) {
                        IspisiGresku(json_podaci)
                    }
                    else
                        Kalendar.ucitajPodatke(json_podaci.periodicna, json_podaci.vanredna);

                    Kalendar.obojiZauzeca(divKalendar, prikazaniMjesec, naziv_sale, p_tekst, k_tekst);

                }
            }

            if (xhttp.readyState == 4 && xhttp.status == 404) { console.log("Greska: nepoznat URL"); }

        }


        xhttp.open("POST", url_post, true);
        xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhttp.send(JSON.stringify(JSONObj));
    }

    function upisiPeriodicnaImpl(upisiSemestar, dan_sedmica) {
        let JSONObj = {
            "redovni": true,
            "dan": dan_sedmica,
            "datum": null,
            "semestar": upisiSemestar,
            "pocetak": p_tekst,
            "kraj": k_tekst,
            "naziv": naziv_sale,
            "predavac": id_predavaca
        };

        let xhttpP = new XMLHttpRequest();
        let url_postP = "/post/rezervacija";
        xhttpP.onreadystatechange = function () {
            if (xhttpP.status == 200) {
                if (xhttpP.readyState == 4) {

                    //popraviti nanize
                    json_podaci = JSON.parse(xhttpP.responseText);

                    if (json_podaci.hasOwnProperty('osoba_detalji')) {
                        IspisiGresku(json_podaci)
                    }
                    else
                        Kalendar.ucitajPodatke(json_podaci.periodicna, json_podaci.vanredna);

                    Kalendar.obojiZauzeca(divKalendar, prikazaniMjesec, naziv_sale, p_tekst, k_tekst);

                }
            }

            if (xhttpP.readyState == 4 && xhttpP.status == 404) { console.log("Greska: nepoznat URL"); }

        }


        xhttpP.open("POST", url_postP, true);
        xhttpP.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhttpP.send(JSON.stringify(JSONObj));
    }

    function IspisiGresku(json_podaci1) {
        alert("Nije moguce napraviti rezervaciju! Termin je prije Vas rezervisao " + json_podaci1.osoba_detalji.uloga + " " + json_podaci1.osoba_detalji.ime + " " + json_podaci1.osoba_detalji.prezime + ".");
        Kalendar.ucitajPodatke(json_podaci1.sveRezervacije.periodicna, json_podaci1.sveRezervacije.vanredna);

    }

    function svoOsobljeImpl() {
        var ajax_osoblje = new XMLHttpRequest();
        let url_ucitaj_osoblje = "http://localhost:8080/osoblje/sala";
        ajax_osoblje.onreadystatechange = function () {
            if (ajax_osoblje.status == 200) {
                if (ajax_osoblje.readyState == 4) {
                    //ucitaj podatke u globalnu varijablu i display ih
                    json_podaci = JSON.parse(ajax_osoblje.responseText);
                    if (document.getElementById("sadrzaj").childElementCount > 0) { brisiHeader(); }

                    napraviHeader();
                    for (let b = 0; b < json_podaci.odgovor.length; b++) {
                        let wrapper1 = document.createElement("div");
                        wrapper1.id = "blok-osoba"
                        let osoba_ime_div = document.createElement("div");
                        osoba_ime_div.id = "ime_osoba"
                        osoba_ime_div.innerHTML = json_podaci.odgovor[b].ime_osobe;

                        let osoba_sala_div = document.createElement("div");
                        osoba_sala_div.id = "sala_osoba"
                        osoba_sala_div.innerHTML = json_podaci.odgovor[b].u_sali;
                        wrapper1.appendChild(osoba_ime_div);
                        wrapper1.appendChild(osoba_sala_div);
                        document.getElementById("sadrzaj").appendChild(wrapper1);

                    }
                }

            }

            if (ajax_osoblje.readyState == 4 && ajax_osoblje.status == 404) { console.log("Greska: nepoznat URL"); }
        }

        ajax_osoblje.open("GET", url_ucitaj_osoblje, true);
        ajax_osoblje.setRequestHeader("Content-Type", "application/json");
        ajax_osoblje.send();
    }

    function napraviHeader() {
        let wrapperr = document.createElement("div");
        wrapperr.id = "blok-osoba"
        let osoba_ime_div1 = document.createElement("div");
        osoba_ime_div1.id = "ime_osoba"
        osoba_ime_div1.innerHTML = "OSOBA:";

        let osoba_sala_div1 = document.createElement("div");
        osoba_sala_div1.id = "sala_osoba"
        osoba_sala_div1.innerHTML = "SALA:";
        wrapperr.appendChild(osoba_ime_div1);
        wrapperr.appendChild(osoba_sala_div1);
        document.getElementById("sadrzaj").appendChild(wrapperr);
    }
    function brisiHeader() {
        var e = document.getElementById("sadrzaj");

        //e.firstElementChild can be used. 
        var child = e.lastElementChild;
        while (child) {
            e.removeChild(child);
            child = e.lastElementChild;
        }
    }




    return {
        getPics: getPicsImpl,
        ucitajPodatke: ucitajPodatkeImpl,
        getSve: getSveImpl,
        upisi_zauzeceVanredno: upisi_zauzeceVanrednoImpl,
        upisiPeriodicna: upisiPeriodicnaImpl,
        dohvatiOsoblje: dohvatiOsobljeImpl,
        svoOsoblje: svoOsobljeImpl
    }

})();