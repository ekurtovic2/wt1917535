var Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Sala', {
        
        naziv:{
            type: Sequelize.STRING
        }
    }, {
		tableName: 'Sala'
	});
};