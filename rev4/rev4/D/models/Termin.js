var Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Termin', {
		
        redovni: {
			type: Sequelize.BOOLEAN
        },
        
		dan: {
			type: Sequelize.INTEGER
			
		},
		datum: {
            type: Sequelize.STRING
        },
        semestar: {
            type: Sequelize.STRING
        },
        pocetak: {
            type: Sequelize.TIME
        },
        kraj: {
            type: Sequelize.TIME
        }
	}, {
		tableName: 'Termin'
	});
};