
var mjesec=10;
var divKalendar=document.getElementById("days");
var btnSljedeci=document.getElementById("sljedeci");
var btnPrethodni=document.getElementById("prethodni");

var periodicnoZauzece1 = {dan:"0", semestar:"ljetni", pocetak:"09:00",kraj:"12:00",naziv:"VA2",predavac:"Javier Mascherano"};
var periodicnoZauzece2 = {dan:"6", semestar:"zimski", pocetak:"12:02",kraj:"15:00", naziv:"VA1",predavac:"Javier Saviola"};
var periodicnoZauzece3 = {dan:"2", semestar:"zimski", pocetak:"09:00",kraj:"12:00", naziv:"VA2",predavac:"Javier Solana"};
var periodicnoZauzece4 = {dan:"3", semestar:"ljetni", pocetak:"14:15",kraj:"17:15", naziv:"VA1",predavac:"Javier Zanetti"};

var periodicnaTmp=[periodicnoZauzece1, periodicnoZauzece2,periodicnoZauzece3,periodicnoZauzece4];
//var periodicnaTmp2=[periodicnoZauzece4];

var vandrednoZauzece1={datum:"06:06:2019",pocetak:"09:00", kraj:"11:00",naziv:"VA1",predavac:"Fernando Redondo"};
var vandrednoZauzece2={datum:"05:05:2019",pocetak:"09:00", kraj:"11:00",naziv:"VA1",predavac:"Fernando Torres"};
var vandrednoZauzece3={datum:"03:03:2019",pocetak:"12:00", kraj:"14:00",naziv:"VA2",predavac:"Fernando Alonso"};
var vandrednoZauzece4={datum:"04:04:2019",pocetak:"12:00", kraj:"14:00",naziv:"VA2",predavac:"Fernando Alonso"};


var vanrednaTmp=[vandrednoZauzece1,vandrednoZauzece2,vandrednoZauzece3,vandrednoZauzece4];
//var vanrednaTmp2=[vandrednoZauzece3,vandrednoZauzece4];


Kalendar.ucitajPodatke(periodicnaTmp,vanrednaTmp);

Kalendar.iscrtajKalendar(divKalendar,mjesec); 




function oboji() {

Kalendar.iscrtajKalendar(divKalendar,mjesec);
var sale = document.getElementById("sale");
var sala = sale.options[sale.selectedIndex].innerHTML;
var pocetak=document.getElementById("pocetak");
var kraj=document.getElementById("kraj");
if(pocetak.value!="" && kraj.value!="")
    Kalendar.obojiZauzeca(divKalendar, mjesec, sala, pocetak.value, kraj.value);
}


function next() {
    mjesec++;
    if(mjesec==11)
    {
        btnSljedeci.disabled = true;
    }
    else 
    {
       if(mjesec==1)btnPrethodni.disabled=false;
    }
     Kalendar.iscrtajKalendar(divKalendar,mjesec); 
     oboji();
     
}

function previous(){
    mjesec--;
    if(mjesec==0)
    {
        btnPrethodni.disabled = true;
    }
    else
    {
      if(mjesec==10)  btnSljedeci.disabled=false;
    }
    Kalendar.iscrtajKalendar(divKalendar,mjesec);
    oboji();
}

function ucitaj(){  //sluzi za učitavanje novih podataka, poziva se clickom na checkbox periodicna 
                    //ne treba raditi kao zadatak ali zbog provjere učitavanja novih podataka napisano
    Kalendar.ucitajPodatke(periodicnaTmp2, vanrednaTmp2);
    Kalendar.iscrtajKalendar(divKalendar,mjesec);
}
