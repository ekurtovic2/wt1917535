//U implementaciji za dodjeljivanje klase elementu korišteno className="klasa" (bez +=)
//cime se dodjeljuje nova klasa tj. prepisuje stara
//nije koristena classList, obzirom da je manja podrzana u broswerima
//izmedju ostalog ista zahtijeva konstantno uklanjanja/dodavanje klase
//u suprotnom mozemo imati dodijeljenje obje klase
//sto je nekad i cilj, medjutim u testovima bi zahtijevalo dodatnu disciplinu provjere da li je to jedina klasa dodijeljena
//jer test moze prolazit, iako element nije obojen kako treba

let assert = chai.assert;
describe('Kalendar', function() {
    describe('obojiZauzeca()', function() {
        it('Sve treba biti zeleno jer nema ucitanih podataka', function() {

            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), new Date().getMonth());
            let datumi = document.getElementsByClassName("date").item(0);
            let i;

            //mozemo provjeriti broj djece sa klasom zauzeta
            assert.equal(datumi.getElementsByClassName("zauzeta").length, 0);
            //i sa klasom slobodna

            assert.equal(datumi.getElementsByClassName("slobodna").length, datumi.children.length);

            //a moze i jedno po dijete
            for( i=0;i<datumi.children.length;i++){
                //svakoj sali pridružena klasa slobodna
                assert.equal(datumi.children[i].className, "slobodna");
                //formalnije da u class listi nema klase zauzeta
                assert.equal(datumi.children[i].classList.contains("zauzeta"), false);
            }
        });
        it('Zauzeti termini trebaju ostati crveni iako su duplicirani', function() {

            const zauzeca = [
                {dan:1,  semestar: "zimski", pocetak: "13:30", kraj:"15:00" , naziv:"VA1" , predavac: "Vensada"},
                {dan:3,  semestar: "zimski", pocetak:"17:00" , kraj:"18:30" , naziv:"VA1" , predavac: "Vensada"},
                {dan:0,  semestar: "zimski", pocetak:"09:00" , kraj:"10:00" , naziv:"VA1" , predavac: "Vensada"},
                {dan:6,  semestar: "zimski", pocetak:"11:30" , kraj:"13:00" , naziv:"VA1" , predavac: "Vensada"},
                {dan:6,  semestar: "zimski", pocetak:"11:30" , kraj:"13:00" , naziv:"VA1" , predavac: "Vensada"}];
            //ponavlja se posljednji slog redovnih
            const zauzecaVanredna = [

                {datum: "04.11.2019", pocetak: "11:00", kraj: "13:00", naziv: "VA1", predavac: "Dinko Osmankovic"},
                {datum: "04.11.2019", pocetak: "11:00", kraj: "13:00", naziv: "VA1", predavac: "Dinko Osmankovic"},
            ];
            //2 ista periodicna
            let kalendar=document.getElementById("kalendar");
            Kalendar.ucitajPodatke(zauzeca, zauzecaVanredna);
            Kalendar.iscrtajKalendar(kalendar,10);
            Kalendar.obojiZauzeca(kalendar,10,"VA1", "11:00","13:30");

            let dates = document.getElementsByClassName("date").item(0);
            let i=0;
            //test redovni na 6. novembar
            for( i=0;i<5;i++){
                if(i*7+9<dates.children.length) {
                    assert.equal(dates.children[i*7+9].className, "zauzeta");
                }
            }
            //test vanredni
            //provjeriti plus i minus za mjesec
            assert.equal(dates.children[3].className, "zauzeta");
        });

        it('Zauzeca ucitana za zimski semestar ne smiju se ocitovati na ljetnom, testni primjer april', function() {

            //periodicna za zimski semestar
            const zauzeca = [
                {dan:1,  semestar: "zimski", pocetak: "13:30", kraj:"15:00" , naziv:"VA1" , predavac: "Vensada"},
                {dan:3,  semestar: "zimski", pocetak:"17:00" , kraj:"18:30" , naziv:"VA1" , predavac: "Vensada"},
                {dan:0,  semestar: "zimski", pocetak:"09:00" , kraj:"10:00" , naziv:"VA1" , predavac: "Vensada"},
                {dan:6,  semestar: "zimski", pocetak:"11:30" , kraj:"13:00" , naziv:"VA1" , predavac: "Vensada"}];
            const zauzecaVanredna = [

                {datum: "04.10.2019", pocetak: "11:00", kraj: "13:00", naziv: "VA1", predavac: "Dinko Osmankovic"},
                {datum: "11.09.2019", pocetak: "16:00", kraj: "19:00", naziv: "VA1", predavac: "Emir Sokic"},
            ];
            let kalendar=document.getElementById("kalendar");
            Kalendar.ucitajPodatke(zauzeca, zauzecaVanredna);
            Kalendar.iscrtajKalendar(kalendar,3);
            Kalendar.obojiZauzeca(kalendar,3,"VA1", "11:00","13:30");

            let dates = document.getElementsByClassName("date").item(0);
            let i=0;
            //nijedan datum u aprilu nije obojen, obzirom da su ucitana zauzeca za zimski semestar
            for( i=0;i<dates.children.length;i++){
                //class listi ne sadrzi klasu zauzeta
                assert.equal(dates.children[i].classList.contains("zauzeta"), false);
            }
        });

        it('Zauzeca ucitana za drugi mjesec ne smiju se obojiti na mjesecu koji ne sadrzi to zauzece', function() {

            const zauzecaVanredna = [

                //stavimo nekoliko datuma u razlicitim mjesecima
                //(isti dan zbog lakseg testiranja)
                //Provjerit cemo da li se data zauzeca prikazuju u januaru, obzirom da je on u istom semestru
                //kao mjeseci navedeni ispod
                {datum: "11.10.2019", pocetak: "11:00", kraj: "13:00", naziv: "VA1", predavac: "Dinko Osmankovic"},
                {datum: "11.09.2019", pocetak: "11:00", kraj: "13:00", naziv: "VA1", predavac: "Emir Sokic"},
                {datum: "11.11.2019", pocetak: "11:00", kraj: "13:00", naziv: "VA1", predavac: "Samim Konjicija"}
                ,
            ];
            let kalendar=document.getElementById("kalendar");
            Kalendar.ucitajPodatke([], zauzecaVanredna);
            Kalendar.iscrtajKalendar(kalendar,0);
            Kalendar.obojiZauzeca(kalendar,0,"VA1", "11:00","13:30");

            let dates = document.getElementsByClassName("date").item(0);
            let i=0;
            //11 januar ne smije biti obojen u crveno obzirom da su ucitana zauzeca za druge mjesece
            //class listi ne sadrzi klasu zauzeta
            assert.equal(dates.children[10].classList.contains("zauzeta"), false);

        });
        it('Zauzet citav mjesec, sve crveno  - sve vanredna, primjer Januar', function() {

            let zauzecaVanredna=[];
            let i,x="0";
            for(i=1;i<32;i++){
                zauzecaVanredna.push({datum: x+i+".01.2019", pocetak: "11:00", kraj: "13:00", naziv: "VA1", predavac: "Dinko Osmankovic"});
                if(i===9) x="";
            }

            let kalendar=document.getElementById("kalendar");
            Kalendar.ucitajPodatke([], zauzecaVanredna);
            Kalendar.iscrtajKalendar(kalendar,0);
            Kalendar.obojiZauzeca(kalendar,0,"VA1", "11:00","13:00");

            let dates = document.getElementsByClassName("date").item(0);
            for(i=0;i<31;i++){
                assert.equal(dates.children[i].classList.contains("zauzeta"), true);
                assert.equal(dates.children[i].classList.contains("slobodna"), false);
            }
        });
        it('Zauzet citav mjesec, sve crveno  - sve redovna, primjer Januar', function() {

            let zauzecaRedovna=[];
            let i;
            for(i=0;i<7;i++){
                zauzecaRedovna.push({dan:i,  semestar: "zimski", pocetak: "13:30", kraj: "14:00" , naziv:"VA1" , predavac: "Vensada"});
            }


            let kalendar=document.getElementById("kalendar");
            Kalendar.ucitajPodatke( zauzecaRedovna,[]);
            Kalendar.iscrtajKalendar(kalendar,0);
            Kalendar.obojiZauzeca(kalendar,0,"VA1", "13:00","15:00");

            let dates = document.getElementsByClassName("date").item(0);
            for(i=0;i<31;i++){
                assert.equal(dates.children[i].className, "zauzeta");
            }
        });

        it('Uzastopni pozivi funkcije obojiZauzeca -- iste boje, primjer Maj', function() {

            let zauzecaRedovna=[];
            let i;
            for(i=0;i<7;i+=2){
                zauzecaRedovna.push({dan:i,  semestar: "ljetni", pocetak: "13:30", kraj: "14:00" , naziv:"VA1" , predavac: "Vensada"});
            }


            let kalendar=document.getElementById("kalendar");
            Kalendar.ucitajPodatke( zauzecaRedovna,[]);
            Kalendar.iscrtajKalendar(kalendar,4);
            Kalendar.obojiZauzeca(kalendar,4,"VA1", "13:00","15:00");
            let dates = document.getElementsByClassName("date").item(0);
            let bojeDatuma=[];
            for(i=0;i<31;i++){
                bojeDatuma[i]=dates.children[i].className;
            }
            //opet poziv nad istim podacima, treba ostat isto
            Kalendar.obojiZauzeca(kalendar,4,"VA1", "13:00","15:00");
            for(i=0;i<31;i++){
                assert.equal(dates.children[i].className, bojeDatuma[i]);
            }
        });

        it('Uzastopni pozivi funkcije obojiZauzeca sa drugim vrijednostima - druge boje', function() {

            let zauzecaRedovna=[];
            let i;
            //Namjestiti cemo suprotno obojene kolone, pa sve celije uporediti
            //neparne sedmice
            for(i=0;i<7;i+=2){
                zauzecaRedovna.push({dan:i,  semestar: "ljetni", pocetak: "13:30", kraj: "14:00" , naziv:"VA1" , predavac: "Vensada"});
            }


            let kalendar=document.getElementById("kalendar");
            Kalendar.ucitajPodatke( zauzecaRedovna,[]);
            Kalendar.iscrtajKalendar(kalendar,5);
            Kalendar.obojiZauzeca(kalendar,5,"VA1", "13:00","15:00");
            let dates = document.getElementsByClassName("date").item(0);
            let bojeDatuma=[];
            for(i=0;i<30;i++){
                bojeDatuma[i]=dates.children[i].className;
            }
            zauzecaRedovna=[];
            //parne sedmice
            for(i=1;i<7;i+=2){
                zauzecaRedovna.push({dan:i,  semestar: "ljetni", pocetak: "13:30", kraj: "14:00" , naziv:"VA1" , predavac: "Vensada"});
            }
            Kalendar.ucitajPodatke( zauzecaRedovna,[]);
            //opet poziv nad istim podacima, treba ostat isto
            Kalendar.obojiZauzeca(kalendar,5,"VA1", "13:30","14:00");
            for(i=0;i<30;i++){
                assert.notEqual(dates.children[i].className, bojeDatuma[i]);
            }
        });

        it('Test vremenskog preklapanja', function() {

            const zauzecaVanredna = [

                {datum: "04.10.2019", pocetak: "11:00", kraj: "13:00", naziv: "VA1", predavac: "Dinko Osmankovic"},
                {datum: "05.10.2019", pocetak: "13:15", kraj: "14:00", naziv: "VA1", predavac: "Dinko Osmankovic"},
                {datum: "06.10.2019", pocetak: "14:00", kraj: "17:00", naziv: "VA1", predavac: "Dinko Osmankovic"},
                {datum: "07.10.2019", pocetak: "17:15", kraj: "18:15", naziv: "VA1", predavac: "Dinko Osmankovic"},
                {datum: "08.10.2019", pocetak: "19:00", kraj: "20:00", naziv: "VA1", predavac: "Dinko Osmankovic"},
                {datum: "10.10.2019", pocetak: "11:00", kraj: "13:00", naziv: "VA1", predavac: "Dinko Osmankovic"}
            ];

            let kalendar=document.getElementById("kalendar");
            Kalendar.ucitajPodatke( [],zauzecaVanredna);
            Kalendar.iscrtajKalendar(kalendar,9);
            //preklapanje u minutu, pocetak prije zauzeca
            Kalendar.obojiZauzeca(kalendar,9,"VA1", "10:00","11:01");
            let dates = document.getElementsByClassName("date").item(0);

            assert.equal(dates.children[3].className, "zauzeta");
            assert.equal(dates.children[3].classList.contains("slobodna"), false);

            //preklapanje u minutu, pocetak minutu prije kraja zauzeca
            Kalendar.obojiZauzeca(kalendar,9,"VA1", "12:59","13:01");
            assert.equal(dates.children[3].className, "zauzeta");
            assert.equal(dates.children[3].classList.contains("slobodna"), false);

            //ne smije se obojiti
            Kalendar.obojiZauzeca(kalendar,9,"VA1", "13:00","13:15");
            assert.equal(dates.children[4].className, "slobodna");
            assert.equal(dates.children[4].classList.contains("zauzeta"), false);
            assert.equal(dates.children[3].className, "slobodna");
            assert.equal(dates.children[3].classList.contains("zauzeta"), false);

            // interval zauzetosti sale nadskup intervala na formi
            Kalendar.obojiZauzeca(kalendar,9,"VA1", "15:00","16:00");
            assert.equal(dates.children[5].className, "zauzeta");
            assert.equal(dates.children[5].classList.contains("slobodna"), false);

            // interval na formi nadskup intervala zauzeca
            Kalendar.obojiZauzeca(kalendar,9,"VA1", "17:00","19:00");
            assert.equal(dates.children[6].className, "zauzeta");
            assert.equal(dates.children[6].classList.contains("slobodna"), false);

            //jednaki intervali
            Kalendar.obojiZauzeca(kalendar,9,"VA1", "17:15","18:15");
            assert.equal(dates.children[6].className, "zauzeta");
            assert.equal(dates.children[6].classList.contains("slobodna"), false);

            //sijeku se
            Kalendar.obojiZauzeca(kalendar,9,"VA1", "17:45","19:15");
            assert.equal(dates.children[6].className, "zauzeta");
            assert.equal(dates.children[6].classList.contains("slobodna"), false);

            //sijeku se 2

            Kalendar.obojiZauzeca(kalendar,9,"VA1", "10:00","11:30");
            assert.equal(dates.children[9].className, "zauzeta");
            assert.equal(dates.children[9].classList.contains("slobodna"), false);



        });
        it('Razni: druga sala, drugi mjesec', function() {

            const zauzecaVanredna = [

                {datum: "04.10.2019", pocetak: "11:00", kraj: "13:00", naziv: "VA1", predavac: "Dinko Osmankovic"},
                {datum: "05.10.2019", pocetak: "13:15", kraj: "14:00", naziv: "VA1", predavac: "Dinko Osmankovic"},
                {datum: "06.10.2019", pocetak: "14:00", kraj: "17:00", naziv: "VA1", predavac: "Dinko Osmankovic"},
                {datum: "07.10.2019", pocetak: "17:15", kraj: "18:15", naziv: "VA1", predavac: "Dinko Osmankovic"},
                {datum: "08.10.2019", pocetak: "19:00", kraj: "20:00", naziv: "VA1", predavac: "Dinko Osmankovic"},
                {datum: "10.10.2019", pocetak: "11:00", kraj: "13:00", naziv: "VA1", predavac: "Dinko Osmankovic"}
            ];

            let kalendar=document.getElementById("kalendar");
            Kalendar.ucitajPodatke( [],zauzecaVanredna);
            Kalendar.iscrtajKalendar(kalendar,9);
            let dates = kalendar.children[2];

            //druga sala, ne smije se nijedan obojiti
            Kalendar.obojiZauzeca(kalendar,9,"VA2", "10:00","12:00");
            assert.equal(dates.getElementsByClassName("slobodna").length, dates.children.length);

            //drugi mjesec
            Kalendar.obojiZauzeca(kalendar,10,"VA2", "10:00","12:00");
            assert.equal(dates.getElementsByClassName("slobodna").length, dates.children.length);

        });

    });

    describe('iscrtajKalendar()', function() {
        it('Mjesec sa 30 dana', function() {
            //septembar
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 8);
            let dates = document.getElementsByClassName("date").item(0);
            assert.equal(30, dates.children.length);
            //april
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 3);
            assert.equal(30, dates.children.length);

            //juni
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 5);
            assert.equal(30, dates.children.length);
        });
        it('Mjesec sa 31 danom', function() {
            //decembar
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 11);
            let dates = document.getElementsByClassName("date").item(0);
            assert.equal(31, dates.children.length);

            //januar
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 0);
            assert.equal(31, dates.children.length);

            //juli
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"), 6);
            assert.equal(31, dates.children.length);
        });

        it('Prvi dan u petak', function() {
            //trenutni je novembar, ali fiksiramo na 10
            let kalendarRef=document.getElementById("kalendar");
            Kalendar.iscrtajKalendar(kalendarRef, 10);
            let dates = kalendarRef.children[2];
            //prvi dan je prvo dijete
            //style grid column vraca broj kolone kao npr grid-column: 5/auto
            let redniBroj=dates.children[0].style.gridColumn.split('/')[0];
            //Redni broj nam govori za koliko je pomjereno udesno tj u kojoj koloni je prva vrijednost u gridu
            //Grid column indeksira od 1
            //Petak je 5 kolona, pa gridColumn treba biti 5
            assert.equal(5, redniBroj);
            //a mozemo  i ovako
            assert.equal(new Date(2019,10).getDay(), redniBroj);
        });
        it('Posljednji dan u subotu', function() {
            //trenutni je novembar, ali fiksiramo na 10
            //Ovaj test se treba pustiti na širini >600px da bi uopce bila subota
            let kalendarRef=document.getElementById("kalendar");
            Kalendar.iscrtajKalendar(kalendarRef, 10);
            let dates = kalendarRef.children[2];
            //subota posljednji dan, treba biti posljednje dijete
            let lastChildIndex=dates.lastChild.textContent; //ako je ispravno posljednje dijete treba biti 29
            //posmatramo u odnosu na prvi dan koliko odstupa kolona
            let redniBroj=dates.children[0].style.gridColumn.split('/')[0];
            let offsetOdPrvog=8-redniBroj; //npr ako je u petak 7-5=2 --> ima 3 viska u prvom tj 7-5+1=3 , a to su pet,sub i ned
            //a onda modul po 7
            assert.equal(6, (lastChildIndex-offsetOdPrvog)%7);
            //da bismo znali da je subota, uvjerimo se da je kolona za 1 veca od petka
            assert.equal(1, (lastChildIndex-offsetOdPrvog)%7-redniBroj);

            //Iznad je test da li je posljednji dan 30 i koliki je offset od prvog dana (treba biti 1 jer je u petak)
            //Treba jos pokazati da se graficki posljednji dan nalazi na mjestu subote tj ispod labele subota
            //ako je ispravno lastChildIndex je 30 jer se uzima tekst,a  onda je indeks -1 od te vrijednost tj 29
            let subota = document.getElementsByClassName("slobodna")[lastChildIndex-1].getBoundingClientRect();
            let subotaNaziv=document.querySelectorAll(".day div")[5].getBoundingClientRect();

            //trebaju biti isto poravnate udesno, i imati istu x koordinatu
            assert.equal(subotaNaziv.right, subota.right);
            assert.equal(subotaNaziv.x, subota.x);



        });

        it('Test januar', function() {
            let kalendarRef=document.getElementById("kalendar");
            Kalendar.iscrtajKalendar(kalendarRef, 0);
            let dates = kalendarRef.children[2];
            let redniBroj=dates.children[0].style.gridColumn.split('/')[0];

            //pocinje u utorak
            assert.equal(2, redniBroj);
            for(var i=0;i<dates.children.length;i++){
                assert.equal(i+1, dates.children[i].textContent);
            }
        });

        it('Naziv mjeseca', function() {
            //Januar
            let kalendarRef=document.getElementById("kalendar");
            Kalendar.iscrtajKalendar(kalendarRef, 0);
            assert.equal("Januar", kalendar.children[0].textContent);

            //Maj
            Kalendar.iscrtajKalendar(kalendar,4);
            assert.equal("Maj", kalendar.children[0].textContent);

            //Decembar

            Kalendar.iscrtajKalendar(kalendar,11);
            assert.equal("Decembar", kalendar.children[0].textContent);
        });

        it('Svi mjeseci duzina', function() {

            let kalendarRef=document.getElementById("kalendar");
            for(var i=0;i<12;i++){
                Kalendar.iscrtajKalendar(kalendarRef, i);
                let dates=document.getElementsByClassName("date").item(0);
                assert.equal(new Date(2019, i+1,0).getDate(), dates.children.length);
            }
        });

        it('Pogresni parametri', function() {

            const zauzeca = [
                {dan: 1, semestar: "zimski", pocetak: "13:30", kraj: "15:00", naziv: "VA1", predavac: "Vensada"},
                {dan: 3, semestar: "zimski", pocetak: "17:00", kraj: "18:30", naziv: "VA1", predavac: "Vensada"},
                {dan: 0, semestar: "zimski", pocetak: "09:00", kraj: "10:00", naziv: "VA1", predavac: "Vensada"},
                {dan: 6, semestar: "zimski", pocetak: "11:30", kraj: "13:00", naziv: "VA1", predavac: "Vensada"},
                {dan: 2, semestar: "ljetni", pocetak: "13:30", kraj: "15:00", naziv: "VA2", predavac: "Vensada"},
                {dan: 4, semestar: "zimski", pocetak: "13:15", kraj: "14:00", naziv: "VA1", predavac: "Vensada"}];
            const zauzecaVanredna = [
                {datum: "03.12.2019", pocetak: "13:00", kraj: "15:00", naziv: "VA1", predavac: "Dinko Osmankovic"},
                {datum: "01.10.2019", pocetak: "13:00", kraj: "15:00", naziv: "VA1", predavac: "Dinko Osmankovic"},
                {datum: "30.09.2019", pocetak: "13:00", kraj: "15:00", naziv: "VA1", predavac: "Dinko Osmankovic"},
                {datum: "04.11.2019", pocetak: "13:00", kraj: "15:00", naziv: "VA1", predavac: "Dinko Osmankovic"},
                {datum: "16.8.2019", pocetak: "11:30", kraj: "12:45", naziv: "VA1", predavac: "Dinko Osmankovic"}];
            Kalendar.ucitajPodatke(zauzeca, zauzecaVanredna);
            Kalendar.iscrtajKalendar(document.getElementById("kalendar"),10);
            Kalendar.obojiZauzeca(document.getElementById("kalendar"),12,"VA1","13:30","14:40");

            //ne smije se nista obojiti obzirom na neispravnost parametra mjesec
            assert.equal(30, document.getElementsByClassName("slobodna").length);


            Kalendar.obojiZauzeca(document.getElementById("kalendar"),1,"VA1","3:30","14:40");

            //ne smije se nista obojiti obzirom na neispravnost parametra pocetak

            assert.equal(30, document.getElementsByClassName("slobodna").length);

            Kalendar.obojiZauzeca(document.getElementById("kalendar"),7,"VA1","10:30","14:40");

            //neispravnost zauzeca, pogresan daum 16.8 (umjesto 16.08)
            assert.equal(document.getElementsByClassName("date").item(0).children[15].className, "slobodna");
        });

    });

});
