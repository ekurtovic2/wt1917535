const Sequelize = require("sequelize");

module.exports = function(sequelize,DataTypes){
    const Rezervacija = sequelize.define("Rezervacija",{},
        {
            tableName:'Rezervacija'
        });
    return Rezervacija;
};