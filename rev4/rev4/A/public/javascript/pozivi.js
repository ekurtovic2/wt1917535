let Pozivi = (function(){

    function getZauzecaImpl(fnCallback) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {// Anonimna funkcija
            if (ajax.readyState === 4 && ajax.status === 200){
                let jsonRez =JSON.parse (ajax.responseText);
                fnCallback(jsonRez.periodicna, jsonRez.vanredna);
            }
            else if (ajax.readyState === 4)
                fnCallback(ajax.statusText,null);
        };
        ajax.open("GET","/zauzeca",true);
        ajax.send();
    }
    function getSlikuImpl(niz, fnCallback) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {// Anonimna funkcija
            if (ajax.readyState === 4 && ajax.status === 200){
                let jsonRez=JSON.parse(ajax.response)
                fnCallback(jsonRez);
            }
            else if (ajax.readyState === 4)
                fnCallback(ajax.statusText,null);
        };
        ajax.open("POST","/slike",true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(niz);
    }

    function upisiZauzeceImpl(zauzece,path,fnCallback) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {// Anonimna funkcija
            if (ajax.readyState === 4 && ajax.status === 200){
                let jsonRez = JSON.parse(ajax.responseText);
                fnCallback(jsonRez)
            }
            else if (ajax.readyState === 4) {
                console.log("greska");
                let jsonRez= JSON.parse(ajax.responseText);
                alert(jsonRez.poruka);
                //azurira se browser nakon greske
                fnCallback(jsonRez.zauzeca)
            }
        };
        ajax.open("POST",path,true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify(zauzece));
    }

    function getOsobljeImpl(fnCallback){
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {// Anonimna funkcija
            if (ajax.readyState === 4 && ajax.status === 200){
                let jsonRez = JSON.parse(ajax.responseText);
                fnCallback(jsonRez)
            }
            else if (ajax.readyState === 4) {
                console.log("greska");
            }
        };
        ajax.open("GET","/osoblje",true);
        ajax.send();

    }
    function getSaleImpl(fnCallback){
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {// Anonimna funkcija
            if (ajax.readyState === 4 && ajax.status === 200){
                let jsonRez = JSON.parse(ajax.responseText);
                fnCallback(jsonRez)
            }
            else if (ajax.readyState === 4) {
                console.log("greska");
            }
        };
        ajax.open("GET","/sale",true);
        ajax.send();
    }
    return {
        getZauzeca: getZauzecaImpl,
        upisiZauzece: upisiZauzeceImpl,
        getSlike: getSlikuImpl,
        getOsoblje: getOsobljeImpl,
        getSale: getSaleImpl
    }
}());
