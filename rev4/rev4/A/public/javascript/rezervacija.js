window.onload=function(){
    Pozivi.getZauzeca(Rezervacija.initialize);

};
let Rezervacija = (function () {

    let mjesec, buttons,forma, salaInput, pocetakInput, krajInput,pocetak=-1,kraj=-1,sala="1-13",periodicna=false, year=new Date().getFullYear(),osoba="Neko";
    mjesec = new Date().getMonth();
    let osobljeNiz=[];
    buttons=document.getElementById("inner").children[1];
    let check=document.getElementById("check");
    forma=document.getElementById("forma");
    salaInput=forma.children[0].children[1];
    pocetakInput=forma.children[2].children[1];
    krajInput=forma.children[3].children[1];
    let osobljeSel=forma.children[4].children[1];

    function initialize(zauzecaRed, zauzecaVan) {
        Kalendar.ucitajPodatke(zauzecaRed, zauzecaVan);
        buttons.children[0].disabled = true;
        buttons.children[1].disabled = true;
        Pozivi.getOsoblje(handleOsoblje);
        Pozivi.getSale(handleSale);
        buttonHandle();
    }

    function addListener(){
        let datumi=document.getElementById("kalendar").children[2].children;
        for(let i=0;i<datumi.length;i++){
            let child=datumi[i];
            child.addEventListener("click", function () {
                if(child.className==="zauzeta"){
                    alert(`Osoba ${Kalendar.getZaduzenaOsoba(i)} je već rezervisala salu u tom terminu!`);
                    return;
                }
                if(pocetak===-1 || kraj===-1) return;
                let pocetakMin=Kalendar.parseMinutes(pocetak), krajMin=Kalendar.parseMinutes(kraj);
                if(parseInt(pocetakMin) >= parseInt(krajMin)){
                    alert("Neispravan termin!");
                    return;
                }
                let day=i+1;
                if(day<10) day="0"+day;
                let month=mjesec+1;
                if(month<10) month="0"+month;
                let semestar=Kalendar.odrediSemestar(mjesec);
                if(semestar==='nijedan' && periodicna){
                    alert("Nemoguća periodična rezervacija izvan semestara.");
                    return;
                }
                let msg = confirm("Da li zelite izvrsiti rezervaciju na ovaj datum ("+day+"."+(month)+"."+year+")" );
                if (msg ===true) {
                    let ime=osobljeNiz[osobljeSel.selectedIndex].ime, prezime=osobljeNiz[osobljeSel.selectedIndex].prezime;
                    //vanredna
                    if(!periodicna) {
                        let zauzece = {datum:`${day}.${month}.${year}`,pocetak:pocetak,kraj:kraj,naziv:sala,ime:ime, prezime:prezime};
                        Pozivi.upisiZauzece(zauzece,"/upisiVanrednoZauzece",handleResponse);
                    }
                    //redovna
                    //ovo je format za slanje tijela!!!!!!
                    else{
                        let prviDan=new Date(year,mjesec,day).getDay();
                        if(prviDan===0) prviDan=7; prviDan--;
                        let zauzecePer= {
                            dan: prviDan,
                            semestar: semestar,
                            pocetak: pocetak,
                            kraj: kraj,
                            naziv: sala,
                            ime:ime,
                            prezime:prezime
                        };
                        let objekat={dan:day,mjesec:month, godina:year, zauzece:zauzecePer};
                        Pozivi.upisiZauzece(objekat,"/upisiRedovnoZauzece",handleResponse);
                    }
                }
            },false);
        }
    }

    function handleOsoblje(osoblje){
        osobljeNiz=osoblje;
        for(let k=0;k<osoblje.length;k++){
            let option = document.createElement("option");
            option.value = osoblje[k].ime+" "+osoblje[k].prezime;
            option.text = osoblje[k].ime+" "+osoblje[k].prezime;
            osobljeSel.add(option);
        }
    }

    function handleSale(sale){
        for(let k=0;k<sale.length;k++){
            let option = document.createElement("option");
            option.value = sale[k].naziv;
            option.text = sale[k].naziv;
            salaInput.add(option);
        }
        if(sale.length>0) sala=salaInput.value;
    }

    function handleResponse(zauzeca){
        Kalendar.ucitajPodatke(zauzeca.periodicna, zauzeca.vanredna);
        Kalendar.obojiZauzeca(document.getElementById("kalendar"), mjesec, sala
            ,pocetak, kraj);
    }
    function crtaj() {
        Kalendar.iscrtajKalendar(document.getElementById("kalendar"), mjesec);
    }
    function pokupiPodatkeSFome() {
        if(pocetak===-1 || kraj===-1) return;
        Kalendar.obojiZauzeca(document.getElementById("kalendar"), mjesec, sala
            , pocetak, kraj);
    }
    function buttonHandle() {
        if (mjesec > 0) buttons.children[0].disabled = false;
        if (mjesec < 11) buttons.children[1].disabled = false;
        crtaj();
        addListener();
        pokupiPodatkeSFome();
    }
    salaInput.addEventListener("input", function() {
        sala = salaInput.value;
        pokupiPodatkeSFome();
    }, false);
    osobljeSel.addEventListener("input", function() {
        osoba = osobljeSel.value;
        pokupiPodatkeSFome();
    }, false);

    check.addEventListener( 'change', function() {
        periodicna = this.checked;
    });

    pocetakInput.addEventListener("input", function() {
        pocetak = pocetakInput.value;
        pokupiPodatkeSFome();
    }, false);
    krajInput.addEventListener("input", function() {
        kraj = krajInput.value;
        pokupiPodatkeSFome();
    }, false);

    buttons.children[0].addEventListener("click", function () {
        if (--mjesec === 0) this.disabled = true;
        buttonHandle();
    }, false);

    buttons.children[1].addEventListener("click", function () {
        if (++mjesec === 11) this.disabled = true;
        buttonHandle();
    }, false);
    return {
        initialize: initialize
    }
}());





