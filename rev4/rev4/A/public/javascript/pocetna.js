let frame=document.getElementsByClassName("frame")[0];
let buttons=document.getElementById("buttons").children;
let i=0,kraj=false;
let divs=[],ucitano=[];

window.onload=function(){
    Pozivi.getSlike(JSON.stringify(ucitano),ucitajSlike)
    buttons[0].disabled=true;

};
function buttonHandle(){
    buttons[0].disabled = i === 0;
    if(kraj) buttons[1].disabled = i===divs.length-1;
    else buttons[1].disabled=false;

}
buttons[1].addEventListener("click", function () {
    i++;
    buttonHandle();
    if(i<divs.length) handle(i);
    else Pozivi.getSlike(JSON.stringify(ucitano),ucitajSlike);
}, false);

buttons[0].addEventListener("click", function () {
    i--;
    buttonHandle();
    handle(i);
}, false);

function handle(indeks){
    frame.removeChild(frame.children[1]);
    frame.appendChild(divs[indeks]);
}

function ucitajSlike(responseArr){
    let response=responseArr.arr;
    kraj=responseArr.kraj;
    let div=document.createElement("div");
    div.className="grid-container";
    for(let i=0;i<response.length;i++){
        let divItem=document.createElement("div");
        divItem.className="grid-item";
        let img=document.createElement("img");
        let src=document.createAttribute("src"), extension=response[i].img.split('.').pop();
        src.value=`data:image/${extension};base64,`+response[i].data;
        ucitano.push(response[i].img);
        img.setAttributeNode(src);
        divItem.appendChild(img);
        div.appendChild(divItem)
    }
    divs.push(div);
    if(frame.children.length>1) frame.removeChild(frame.children[1]);
    frame.appendChild(div);
    buttons[1].disabled = kraj;
}

