let Kalendar = (function(){

    let periodicnaZauzeca=[], vanrednaZauzeca=[];
    let zauzecaOsobe=[];

    function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj){

        if(!(mjesec>=0 && mjesec<12) && validateDate(pocetak) && validateDate(kraj)) return;

        let dates = kalendarRef.children[2], firstDay=getFirstDay(mjesec);
        firstDay--;
        refresh(dates);
        periodicnaZauzeca.forEach((zauzece)=> {
            if (odrediSemestar(mjesec)===zauzece.semestar && sala===zauzece.naziv &&
                provjeriPreklapanje(pocetak,kraj,zauzece.pocetak,zauzece.kraj)) {
                if(firstDay<=zauzece.dan) {
                    dates.children[zauzece.dan - firstDay].className="zauzeta";
                    zauzecaOsobe[zauzece.dan - firstDay]=zauzece.ime+" "+zauzece.prezime+" ("+zauzece.uloga+")";
                }
                for(var i=0;i<5;i++){
                    if((i+1)*7+zauzece.dan-firstDay<dates.children.length) {
                        dates.children[(i +1)* 7 + zauzece.dan  - firstDay].className="zauzeta";
                        zauzecaOsobe[(i +1)* 7 + zauzece.dan  - firstDay]=zauzece.ime+" "+zauzece.prezime+" ("+zauzece.uloga+")";
                    }
                }
            }
        });
        let year=new Date().getFullYear();
        vanrednaZauzeca.forEach((zauzece)=>{
            let date=parseDate(zauzece.datum);
            if(provjeriPreklapanje(pocetak, kraj, zauzece.pocetak, zauzece.kraj)
                && zauzece.naziv===sala && date.getMonth()===mjesec && date.getFullYear()===year) {
                dates.children[date.getDate()-1].className=("zauzeta");
                zauzecaOsobe[date.getDate()-1]=zauzece.ime+" "+zauzece.prezime+" ("+zauzece.uloga+")";

            }
        });
    }
    function refresh(dates) {
        let redColored = dates.getElementsByClassName("zauzeta");
        let i;
        for(i=redColored.length-1; i>=0;i--){
            redColored[i].className=("slobodna");
            zauzecaOsobe[i]="slobodna";

        }

    }
    function parseDate(dateStr){
        let parts = dateStr.split(".");
        return new Date(parts[2], parts[1]-1,parts[0]);
    }

    function odrediSemestar(mjesec){
        if((mjesec>8 && mjesec <12) || mjesec===0) return "zimski";
        else if(mjesec>0 && mjesec<6) return "ljetni";
        return "nijedan";
    }
    function provjeriPreklapanje(pocetak, kraj, pocetakZauzece, krajZauzece){
        return (Math.max(parseMinutes(pocetak),parseMinutes(pocetakZauzece))-
            Math.min(parseMinutes(kraj),parseMinutes(krajZauzece))) <0;
    }
    function parseMinutes(time){
        var minParts=time.split(":");
        return  minParts[0]*60+parseInt(minParts[1])
    }
    function ucitajPodatkeImpl(periodicna, vanredna){
        periodicnaZauzeca=[];
        vanrednaZauzeca=[];
       for(var i=0;i<periodicna.length;i++){
            if(validatePeriodicno(periodicna[i])) periodicnaZauzeca.push(periodicna[i]);
        }
        for(var i=0;i<vanredna.length;i++){
            if(validateVanredno(vanredna[i])) vanrednaZauzeca.push(vanredna[i]);
        }
    }
    function clearChildren(parent){
        while(parent.lastChild) {
            parent.removeChild(parent.lastChild);
        }
    }
    function validatePeriodicno(periodicno){
        return validateTime(periodicno.pocetak) && validateTime(periodicno.kraj) &&
            /[0-6]/.test(periodicno.dan);
    }

    function validateVanredno(vanredno){
        return validateTime(vanredno.pocetak) && validateTime(vanredno.kraj) &&
            validateDate(vanredno.datum);
    }

    function createWeek(){
        let dayNames=["PON","UTO","SRI","ČET","PET", "SUB","NED"],days=document.createElement("div");
        for(var i=0;i<7;i++){
            let day=document.createElement("div");
            day.textContent=dayNames[i];
            days.appendChild(day);
        }
        days.className="day";
        return days;
    }

    function validateTime(time){
        var patt =  /^[0-1][0-9]:[0-5][0-9]$|^[2][0-3]:[0-5][0-9]$|^[2][3]:[0][0]$/;
        return patt.test(time);
    }

    function validateDate(date){
        var patt = /^([0-2][0-9]|(3)[0-1])(\.)(((0)[0-9])|((1)[0-2]))(\.)\d{4}$/i;
        return patt.test(date);
    }

    function getZaduzenaOsobaImpl(dan){
        return zauzecaOsobe[dan];
    }

    function getFirstDay(mjesec){
        let firstDay=(new Date(new Date().getFullYear(), mjesec)).getDay();
        if(firstDay===0) firstDay=7;
        return firstDay;
    }
    function iscrtajKalendarImpl(kalendarRef, mjesec){
        clearChildren(kalendarRef);
        let names=["Januar", "Februrar","Mart","April","Maj","Juni","Juli","August","Septembar","Oktobar","Novembar","Decembar"];
        let month=document.createElement("h4"), dates=document.createElement("div"),i;
        month.textContent=names[mjesec];
        kalendarRef.appendChild(month);
        kalendarRef.appendChild(createWeek());
        zauzecaOsobe=[];
        let year=new Date().getFullYear();
        for(i=0; i<new Date(year, mjesec+1,0).getDate();i++){
            //
            zauzecaOsobe.push("slobodna");
            //
            let div1 = document.createElement("div");
            let div2 = document.createElement("div");
            div2.textContent=i+1;
            div1.appendChild(div2);
            div1.className="slobodna";
            dates.appendChild(div1);
        }
        dates.children[0].style.gridColumn=getFirstDay(mjesec).toString();
        dates.className="date";
        kalendarRef.appendChild(dates);

    }
    return {
        obojiZauzeca: obojiZauzecaImpl,
        ucitajPodatke: ucitajPodatkeImpl,
        iscrtajKalendar: iscrtajKalendarImpl,
        odrediSemestar: odrediSemestar,
        parseMinutes:parseMinutes,
        getZaduzenaOsoba:getZaduzenaOsobaImpl
    }
}());


