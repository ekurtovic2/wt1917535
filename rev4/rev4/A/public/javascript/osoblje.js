window.onload=function(){
    Pozivi.getOsoblje(handleResponseOsoblje);
};
let osobljeNiz=[];
let tabela=document.getElementById("tabelaOsoblje");

setInterval(function () {
     Pozivi.getOsoblje(handleResponseOsoblje);
}, 30000);

function filtrirajZauzeca(periodicna, vanredna){
    let today=new Date(Date.now());
    let podaciOSalama={};
    podaciOSalama.osobe=[];
    podaciOSalama.sale=[];
    let year=today.getFullYear(), month=makeTwoDigit(today.getMonth()+1), day=makeTwoDigit(today.getDate());
    let hour=makeTwoDigit(today.getHours());let minutes=makeTwoDigit(today.getMinutes()), semestar=Kalendar.odrediSemestar(today.getMonth());
    let date=`${day}.${month}.${year}`;
    let time=(hour)*60+parseInt(minutes);
    let dayOfWeek=today.getDay();
    if(dayOfWeek===0) dayOfWeek=7;
    dayOfWeek--;

    for(let i=0;i<periodicna.length;i++){
        let zauzece=periodicna[i];
        let pocetak=Kalendar.parseMinutes(zauzece.pocetak), kraj=Kalendar.parseMinutes(zauzece.kraj);
        if(parseInt(pocetak)<=parseInt(time)&& parseInt(kraj)>parseInt(time) && zauzece.dan===dayOfWeek
        && semestar===zauzece.semestar){
            azurirajSalu(zauzece);
        }
    }
    for(let i=0; i<vanredna.length;i++){
        let zauzece=vanredna[i];
        let pocetak=Kalendar.parseMinutes(zauzece.pocetak), kraj=Kalendar.parseMinutes(zauzece.kraj);
        if(parseInt(pocetak)<=parseInt(time)&& parseInt(kraj)>parseInt(time) && date===zauzece.datum){
            azurirajSalu(zauzece);
        }
    }
   refresh();
}


function makeTwoDigit(value){
    if(parseInt(value)<10) value="0"+value;
    return value;
}

function handleResponseOsoblje(osoblje){
    osobljeNiz=osoblje;
    for (let i=0;i<osoblje.length;i++){
        osobljeNiz[i].sala="kancelarija";
    }
    Pozivi.getZauzeca(filtrirajZauzeca);
}

function refresh(){
    while(tabela.lastChild && tabela.children.length>1){
        tabela.removeChild(tabela.lastChild)
    }
    createTable();
}

function azurirajSalu(zauzece){
    for(let k=0;k<osobljeNiz.length;k++){
        if (osobljeNiz[k].ime===zauzece.ime && osobljeNiz[k].prezime===zauzece.prezime){
            osobljeNiz[k].sala=zauzece.naziv;
        }
    }
}

function createTable() {
    for(let k=0;k<osobljeNiz.length;k++){
        let row=document.createElement("tr");
        let td1=document.createElement("td");
        let td2=document.createElement("td");
        let td3=document.createElement("td");
        td1.textContent=osobljeNiz[k].ime+" "+osobljeNiz[k].prezime;
        td2.textContent=osobljeNiz[k].sala;
        td3.textContent=osobljeNiz[k].uloga;
        row.appendChild(td1);
        row.appendChild(td2);
        row.appendChild(td3);
        tabela.appendChild(row);
    }
}
