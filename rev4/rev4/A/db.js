const Sequelize = require("sequelize");
const sequelize = new Sequelize("DBWT19","root","root",{host:"localhost",dialect:"mysql",logging:false});
const db={};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//import modela
db.Osoblje = sequelize.import(__dirname+'/Osoblje.js');
db.Termin = sequelize.import(__dirname+'/Termin.js');
db.Sala = sequelize.import(__dirname+'/Sala.js');
db.Rezervacija = sequelize.import(__dirname+'/Rezervacija.js');

//relacije

//1 na više Osoblje-Rezervacija
db.Osoblje.hasMany(db.Rezervacija,  {
    foreignKey: 'osoba',
    targetKey: 'id',
    as: 'rezervacije'
});
db.Rezervacija.belongsTo(db.Osoblje,{
    foreignKey: {
        name: 'osoba'
    },
    targetKey: 'id',
    as: 'osobe'
});

//1 na 1
db.Termin.hasOne(db.Rezervacija,{
    foreignKey: {
        name: 'termin',
        unique: true
    },
    targetKey: 'id',
    as: 'rezervacija'
});
db.Rezervacija.belongsTo(db.Termin,{
    foreignKey: {
        name: 'termin',
        unique: true
    },
    targetKey: 'id',
    as: 'termini'
});

//vise na 1
db.Sala.hasMany(db.Rezervacija, {
    foreignKey: 'sala',
    targetKey: 'id',
    as :'rezervacije'
});
db.Rezervacija.belongsTo(db.Sala,{
    foreignKey: {
        name: 'sala'
    },
    targetKey: 'id',
    as: 'sale'
});

//1 na 1
db.Osoblje.hasOne(db.Sala,{
    foreignKey: 'zaduzenaOsoba',
    targetKey: 'id',
    as: 'sale'
});
db.Sala.belongsTo(db.Osoblje, {
    foreignKey: 'zaduzenaOsoba',
    targetKey: 'id',
    as: 'osobe'
});
module.exports=db;