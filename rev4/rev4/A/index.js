const Sequelize = require('sequelize');
const express = require('express');
const fs = require('fs');
const app = express();
const bodyParser = require("body-parser");
const db = require('./db.js');
app.use(express.static('public'));
app.use(bodyParser.json());
const path=require('path');

app.get('/', function (req, res, next) {
    let options = {
        root: path.resolve(__dirname,""),
        dotfiles: 'deny',
        headers: {
            'x-timestamp': Date.now(),
            'x-sent': true
        }
    };
    res.sendFile("public/pocetna.html",options, function (err) {
        if (err) {
            next(err)
        } else {
            console.log('Poslana: ', 'pocetna')
        }
    })

});
app.get('/osoblje', function (req, res, next) {
    db.Osoblje.findAll().then(function (resultSet) {
        res.status(200);
        res.send((resultSet));
    })
});
app.get('/zauzeca', function (req, res, next) {
    getZauzeca(function(jsonRez){
        res.send(jsonRez)
    });
});
app.post('/upisiVanrednoZauzece', function (req, res, next) {
    getZauzeca( function (data) {
        let zauzeca = JSON.parse(data), zauzeceVan=req.body, date=parseDate(zauzeceVan.datum);
        for(let i=0;i<zauzeca.vanredna.length;i++){
            let zauzece=zauzeca.vanredna[i];
            let date2=parseDate(zauzece.datum);
            let isti=date.getMonth()===date2.getMonth() && date.getDate()===date2.getDate() && date.getFullYear()===date2.getFullYear();
            if(provjeriPreklapanje(zauzeceVan.pocetak, zauzeceVan.kraj, zauzece.pocetak, zauzece.kraj) && isti) {
                let check=provjeriZauzece(zauzeceVan,zauzece,zauzeceVan.datum);
                if(check.greska){
                    res.status(300);
                    let response={poruka:check.poruka, zauzeca:zauzeca};
                    res.send(JSON.stringify(response, null, 4));
                    return;
                }
            }
        }
        let semestar=odrediSemestar(date.getMonth()); let dan=odrediDan(date);
        for(let i=0;i<zauzeca.periodicna.length;i++){
            let zauzece=zauzeca.periodicna[i];
            if (semestar === zauzece.semestar && zauzece.dan===dan &&
                provjeriPreklapanje(zauzeceVan.pocetak, zauzeceVan.kraj, zauzece.pocetak, zauzece.kraj)) {
                let check=provjeriZauzece(zauzeceVan,zauzece,zauzeceVan.datum);
                if(check.greska){
                    res.status(300);
                    let response={poruka:check.poruka, zauzeca:zauzeca};
                    res.send(JSON.stringify(response, null, 4));
                    return;
                }
            }
        }
        zauzeca.vanredna.push(zauzeceVan);
        let termin={redovni:false, dan:null, datum:zauzeceVan.datum, semestar:null, pocetak:zauzeceVan.pocetak, kraj:zauzeceVan.kraj};
        upisiZauzece(termin, zauzeceVan, function (done) {
            if(done){
                getZauzeca(function(jsonRez){
                    res.send(jsonRez)
                });
            }

        });
    });
});

app.post('/upisiRedovnoZauzece', function (req, res, next) {
    getZauzeca(function ( data) {
        let zauzeca = JSON.parse(data);let json=req.body, zauzeceRed=json.zauzece;
        if(json['mjesec']===undefined) zauzeceRed=json; //ako se ne posalje datum
        for(let i=0;i<zauzeca.periodicna.length;i++){
            let zauzece=zauzeca.periodicna[i];
            if (zauzeceRed.semestar === zauzece.semestar && zauzece.dan===zauzeceRed.dan &&
                provjeriPreklapanje(zauzeceRed.pocetak, zauzeceRed.kraj, zauzece.pocetak, zauzece.kraj)) {
                let check=provjeriZauzece(zauzeceRed,zauzece,`${json.dan}.${json.mjesec}.${json.godina}`);
                if(check.greska){
                    res.status(300);
                    let response={poruka:check.poruka, zauzeca:zauzeca};
                    res.send(JSON.stringify(response, null, 4));
                    return;
                }
            }
        }
        for(let i=0;i<zauzeca.vanredna.length;i++){
            let zauzece=zauzeca.vanredna[i], date=parseDate(zauzece.datum);
            let semestar=odrediSemestar(date.getMonth()),dan=odrediDan(date);
            if (semestar === zauzeceRed.semestar && zauzeceRed.dan===dan &&
                provjeriPreklapanje(zauzeceRed.pocetak, zauzeceRed.kraj, zauzece.pocetak, zauzece.kraj) && new Date().getFullYear()===date.getFullYear()) {
                let check=provjeriZauzece(zauzeceRed,zauzece,`${json.dan}.${json.mjesec}.${json.godina}`);
                if(check.greska){
                    res.status(300);
                    let response={poruka:check.poruka, zauzeca:zauzeca};
                    res.send(JSON.stringify(response, null, 4));
                    return;
                }
            }
        }
        zauzeca.periodicna.push(zauzeceRed);
        let termin={redovni:true, dan:zauzeceRed.dan, datum:null, semestar:zauzeceRed.semestar, pocetak:zauzeceRed.pocetak, kraj:zauzeceRed.kraj};
        upisiZauzece(termin, zauzeceRed, function (done) {
            if(done){
                getZauzeca(function(jsonRez){
                    res.send(jsonRez)
                });
            }
        });
    });
});

//Provjeravamo da li je osoba tad zauzela neku drugu salu
//Ako je zauzela periodicno u drugoj sali
//ne moze nijdan datum vanrednog tog dana u tom terminu
//Ako ima vanredno
//Ne moze se periodicno u drugoj sali u čije dane spada i dan kada vanredno

function provjeriZauzece(zauzeceRed, zauzece, datum){
    let uslovSala=zauzeceRed.naziv === zauzece.naziv;
    let uslovOsoba=zauzece.ime===zauzeceRed.ime&& zauzece.prezime===zauzeceRed.prezime;
    let response=`Nije moguće rezervisati salu ${zauzeceRed.naziv} za navedeni datum ${datum} i termin od ${zauzeceRed.pocetak} do ${zauzeceRed.kraj}!`;
    if(uslovSala) response+=` Salu je rezervisala osoba: ${zauzece.ime} ${zauzece.prezime} (${zauzece.uloga}).`;
    if(uslovOsoba && !uslovSala) response+= ` ISTA osoba (${zauzece.ime} ${zauzece.prezime} - ${zauzece.uloga}) je već rezervisala salu ${zauzece.naziv} u tom terminu.`;
    if (uslovOsoba || uslovSala){
        return {greska:true, poruka: response}
    }
    return {greska:false, poruka: ""}
}

app.get('/sale', function (req, res, next) {
    db.Sala.findAll().then(function (resultSet) {
        res.send((resultSet));
    })
});
app.post('/slike', function (req, res, next) {
    let json=req.body,arr = [];
    /*Koristimo slike koje nisu public, vec im samo server ima pristup*/
    /*Ispis ispod sluzi da bi se moglo lakse pratiti da li se pravi novi zahtjev*/
    console.log("Poslan zahtjev serveru");

    fs.readdir("imagesServer",{ encoding: null }, function(err, files) {
        if (err) throw err;
        let kraj=false;
        for (let i = 0; i < files.length; i++) {
            if (!json.includes(files[i])) arr.push(files[i]);
            if (arr.length === 3) break;
        }
        if(files.length===json.length+arr.length) kraj=true;
        if (arr.length===0 || files.length===0) {
            res.send(JSON.stringify({arr:arr, kraj:true}));
            return;
        }
        fs.readFile(`imagesServer/${arr[0]}`, { encoding: 'base64' }, function (err,data){
            arr[0]={img:arr[0],data:data};
            if(1<arr.length){
                fs.readFile(`imagesServer/${arr[1]}`, { encoding: 'base64' }, function (err,data){
                    arr[1]={img:arr[1],data:data};
                    if(2<arr.length){
                        fs.readFile(`imagesServer/${arr[2]}`, { encoding: 'base64' }, function (err,data){
                            arr[2]={img:arr[2],data:data};
                            res.send(JSON.stringify({arr:arr, kraj:kraj}));
                        })
                    }
                    else{
                        res.send(JSON.stringify({arr:arr, kraj:kraj}));
                    }
                })
            }
           else  res.send(JSON.stringify({arr:arr, kraj:kraj}));
        });
    });
    });
let parseDate=function(dateStr){
    let parts = dateStr.split(".");
    return new Date(parts[2], parts[1]-1,parts[0]);
};

let parseMinutes= function(time)
{
    let minParts = time.split(":");
    return minParts[0] * 60 + minParts[1];
};
let provjeriPreklapanje=function(pocetak, kraj, pocetakZauzece, krajZauzece){
    return (Math.max(parseMinutes(pocetak),parseMinutes(pocetakZauzece))-
        Math.min(parseMinutes(kraj),parseMinutes(krajZauzece))) <0;
};

let odrediSemestar=    function(mjesec){
    if((mjesec>8 && mjesec <12) || mjesec===0) return "zimski";
    else if(mjesec>0 && mjesec<6) return "ljetni";
    return "nijedan";
};

let odrediDan=function(date) {
    let prviDan = date.getDay();
    if (prviDan === 0) prviDan = 7; prviDan--;
    return prviDan;
};

let parseFromDatabase=function(time){
    let minParts = time.split(":");
    return minParts[0]+":"+minParts[1];
}

db.sequelize.sync({force:true}).then(function(){
    inicializacija().then(function(){
        console.log("Gotovo kreiranje tabela i ubacivanje pocetnih podataka!");
        app.listen(8080);
        app.emit("bazaSpremna");
    });
});

function inicializacija(){
    var osobeListaPromisea=[];
    var terminiListaPromisea=[];
    var saleListaPromisea=[];
    var rezervacijeListaPromisea=[];
    return new Promise(function(resolve,reject){
        osobeListaPromisea.push(db.Osoblje.create({ime:'Neko', prezime: 'Nekic', uloga:'profesor'}));
        osobeListaPromisea.push(db.Osoblje.create({ime:'Drugi', prezime: 'Neko', uloga:'asistent'}));
        osobeListaPromisea.push(db.Osoblje.create({ime:'Test', prezime: 'Test', uloga:'asistent'}));
        Promise.all(osobeListaPromisea).then(function(osoblje){
            terminiListaPromisea.push(db.Termin.create({redovni:false, dan:null, datum:'01.01.2020', semestar:null, pocetak:'12:00', kraj:'13:00'}));
            terminiListaPromisea.push(db.Termin.create({redovni:true, dan:0, datum:null, semestar:'zimski', pocetak:'13:00', kraj:'14:00'}));
            Promise.all(terminiListaPromisea).then(function(termini){
                saleListaPromisea.push(db.Sala.create({naziv:'1-11', zaduzenaOsoba:1}));
                saleListaPromisea.push(db.Sala.create({naziv:'1-15', zaduzenaOsoba:2}));
                Promise.all(saleListaPromisea).then(function(sale){
                    rezervacijeListaPromisea.push(db.Rezervacija.create({termin:1, sala: 1, osoba:1}));
                    rezervacijeListaPromisea.push(db.Rezervacija.create({termin:2, sala: 1, osoba:3}));
                    Promise.all(rezervacijeListaPromisea).then(function(b){resolve(b);}).catch(function(err){console.log("Rezervacije greska "+err);});
                }).catch(function(err){console.log("Sale greska "+err);});
            }).catch(function(err){console.log("Termini greska "+err);});
        }).catch(function(err){console.log("Osobe greska "+err);});
    });
}

function getZauzeca(fnCallback){
    let jsonRez={};
    jsonRez.periodicna=[];
    jsonRez.vanredna=[];
    let brojac=0;
    db.Rezervacija.findAll().then(function (resultSet) {
        resultSet.forEach(rezervacija=>{
            rezervacija.getTermini().then(function (termin) {
                let pocetakTermina=parseFromDatabase(termin.pocetak);
                let krajTermina=parseFromDatabase(termin.kraj);
                rezervacija.getSale().then(function (sala) {
                    rezervacija.getOsobe().then(function(osoba)
                    {
                        if (termin.redovni) {
                            let redovno = {
                                dan: termin.dan, semestar: termin.semestar, pocetak: pocetakTermina,
                                kraj: krajTermina, naziv: sala.naziv, ime: osoba.ime, prezime:osoba.prezime, uloga:osoba.uloga};
                            jsonRez.periodicna.push(redovno);
                        } else {
                            let vanredno = {
                                datum: termin.datum, pocetak: pocetakTermina, kraj: krajTermina, naziv: sala.naziv,
                                ime: osoba.ime, prezime:osoba.prezime,uloga:osoba.uloga};
                            jsonRez.vanredna.push(vanredno);
                        }
                        brojac++;
                        if (brojac=== resultSet.length) {
                            fnCallback(JSON.stringify(jsonRez));
                        }
                    })
                })

            })
        })
    })
}

function upisiZauzece(terminUpis,zauzece,fnCallback){
    db.Termin.create(terminUpis)
        .then(function (termin) {
            db.Sala.findOne({where: {naziv:zauzece.naziv}})
                .then(function (sala) {
                    db.Osoblje.findOne({
                        where: {
                            [Sequelize.Op.and]: [
                                { ime: zauzece.ime },
                                { prezime: zauzece.prezime }
                            ]
                        }
                    }).then(function (osoba) {
                        db.Rezervacija.create().then(function (rezervacija) {
                            rezervacija.setTermini(termin).then(function (rezer) {
                                rezer.setSale(sala).then(function (rez) {
                                    rez.setOsobe(osoba).then(function (end) {
                                        fnCallback(true);
                                    })
                                });
                            })
                        })
                    })
                })
        })
}
module.exports = app;




