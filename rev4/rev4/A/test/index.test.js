const app = require('../index');
var request = require("supertest");
var assert = require('chai').assert;
var agent = request.agent(app);
const chai = require("chai");
const chaiHttp = require("chai-http");
const { expect } = chai;
chai.use(chaiHttp);

var supertest = require("supertest")(app);
before(done=> {
    app.on("bazaSpremna", function(){
        done();
    });
});

describe("Osoblje",()=>{
    it("Status i velicina",done =>{
        agent
            .get("/osoblje")
            .expect(200)
            .expect(function(res) {
                assert.lengthOf(res.body,3);
            })
            .end(done);
    });

    it("Sadrzaj osoblje", done=> {
        supertest
            .get("/osoblje")
            .expect(200)
            .expect(function(res) {
                assert.lengthOf(res.body,3);
                let string=JSON.stringify(res.body);
                assert(string.includes("Neko"), true);
                assert(string.includes("Nekic"), true);
                assert(string.includes("Drugi"), true);
                assert(string.includes("Neko"), true);
                assert(string.includes("Test"), true);
                assert(string.includes("profesor"), true);
                assert(string.includes("asistent"), true);
            })
            .end(done);
    });
});


describe("Zauzeca - citanje", ()=> {

    it("status", done => {
        agent
            .get("/zauzeca")
            .expect(200, done);
    });

    it("Sadrzaj zauzece", done=> {
        supertest
            .get("/zauzeca")
            .expect(200)
            .expect(function (res) {
                let jsonRez = JSON.parse(res.text);
                let periodicna = jsonRez.periodicna;
                let vanredna = jsonRez.vanredna;
                assert.lengthOf(periodicna, 1);
                assert.lengthOf(vanredna, 1);
                assert.equal(JSON.stringify(periodicna[0]), JSON.stringify({
                    dan: 0,
                    semestar: "zimski",
                    pocetak: "13:00",
                    kraj: "14:00",
                    naziv: "1-11",
                    ime: "Test",
                    prezime: "Test",
                    uloga: "asistent"
                }));
                assert.equal(JSON.stringify(vanredna[0]), JSON.stringify({
                    datum: "01.01.2020",
                    pocetak: "12:00",
                    kraj: "13:00",
                    naziv: "1-11",
                    ime: "Neko",
                    prezime: "Nekic",
                    uloga: "profesor"
                }));
            })
            .end(done);
    });
});
describe("Zauzeca - upis", ()=>{

        it("Upis zauzeca - vanredno", done=> {
            let zauzece= {datum:"18.02.2020",pocetak:"12:00",kraj:"13:00",naziv:"1-11",ime:"Neko",prezime:"Nekic"};
            supertest
                .post("/upisiVanrednoZauzece")
                .send((zauzece))
                .expect(200)
                .expect(function (res) {
                    let jsonRez = JSON.parse(res.text);
                    let periodicna = jsonRez.periodicna;
                    let vanredna = jsonRez.vanredna;
                    assert.lengthOf(periodicna, 1);
                    assert.lengthOf(vanredna, 2);
                    assert.equal(JSON.stringify(periodicna[0]), JSON.stringify({
                        dan: 0,
                        semestar: "zimski",
                        pocetak: "13:00",
                        kraj: "14:00",
                        naziv: "1-11",
                        ime: "Test",
                        prezime: "Test",
                        uloga: "asistent"
                    }));
                    assert.equal(JSON.stringify(vanredna[0]), JSON.stringify({
                        datum: "01.01.2020",
                        pocetak: "12:00",
                        kraj: "13:00",
                        naziv: "1-11",
                        ime: "Neko",
                        prezime: "Nekic",
                        uloga: "profesor"
                    }));
                    assert.equal(JSON.stringify(vanredna[1]), JSON.stringify({
                        datum:"18.02.2020",
                        pocetak:"12:00",
                        kraj:"13:00",
                        naziv:"1-11",
                        ime:"Neko",
                        prezime:"Nekic",
                        uloga: "profesor"}));
                })
                .end(done);
        });

    it("Upis zauzeca - redovno", done=> {
        let zauzece= {
            dan: 3,
            semestar: "zimski",
            pocetak: "12:30",
            kraj: "14:15",
            naziv: "1-15",
            ime: "Drugi",
            prezime: "Neko",
            uloga: "asistent"
        };
        let day="06";
        let month="02";
        let year="2020";
        let posaljiPost={dan:day,mjesec:month, godina:year, zauzece:zauzece};
        supertest
            .post("/upisiRedovnoZauzece")
            .send(posaljiPost)
            .expect(200)
            .expect(function (res) {
                let jsonRez = JSON.parse(res.text);
                let periodicna = jsonRez.periodicna;
                let vanredna = jsonRez.vanredna;
                assert.lengthOf(periodicna, 2);
                assert.lengthOf(vanredna, 2);
                assert.equal(JSON.stringify(periodicna[0]), JSON.stringify({
                    dan: 0,
                    semestar: "zimski",
                    pocetak: "13:00",
                    kraj: "14:00",
                    naziv: "1-11",
                    ime: "Test",
                    prezime: "Test",
                    uloga: "asistent"
                }));
                assert.equal(JSON.stringify(periodicna[1]), JSON.stringify(posaljiPost.zauzece));
                assert.equal(JSON.stringify(vanredna[0]), JSON.stringify({
                    datum: "01.01.2020",
                    pocetak: "12:00",
                    kraj: "13:00",
                    naziv: "1-11",
                    ime: "Neko",
                    prezime: "Nekic",
                    uloga: "profesor"
                }));
                assert.equal(JSON.stringify(vanredna[1]), JSON.stringify({
                    datum:"18.02.2020",
                    pocetak:"12:00",
                    kraj:"13:00",
                    naziv:"1-11",
                    ime:"Neko",
                    prezime:"Nekic",
                    uloga: "profesor"}));
            })
            .end(done);
    });
    });

describe("Sale",()=> {
    it("Status i velicina", done => {
        agent
            .get("/sale")
            .expect(200)
            .expect(function (res) {
                assert.lengthOf(res.body,2);
            })
            .end(done);
    });

    it("Sadrzaj sale", done=> {
        supertest
            .get("/sale")
            .expect(200)
            .expect(function (res) {
                let string = JSON.stringify(res.body);
                assert(string.includes("1-11"), true);
                assert(string.includes("1-15"), true);
            })
            .end(done);
    });
});

describe("Neuspjesno upisivanje zauzeca", () =>{
    it("Preklapanje vanrednog sa vanrednim", done=> {
        let zauzece= {datum:"01.01.2020",pocetak:"12:30",kraj:"14:10",naziv:"1-11",ime:"Test",prezime:"Test"}
        supertest
            .post("/upisiVanrednoZauzece")
            .send((zauzece))
            .expect(300) //status kad se ne unese zauzece
            .expect(function(res) {
                let tekst=JSON.parse(res.text);
                let jsonRez=tekst.poruka;
                assert.equal(jsonRez, `Nije moguće rezervisati salu ${zauzece.naziv} za navedeni datum ${zauzece.datum} i termin od ${zauzece.pocetak} do ${zauzece.kraj}!`
                    +` Salu je rezervisala osoba: Neko Nekic (profesor).`);
            })
            .end(done);
    });

    it("Preklapanje vanrednog sa periodicnim", done=> {
        let zauzece= {datum:"20.01.2020",pocetak:"12:30",kraj:"14:10",naziv:"1-11",ime:"Test",prezime:"Test"}
        supertest
            .post("/upisiVanrednoZauzece")
            .send((zauzece))
            .expect(300) //status kad se ne unese zauzece
            .expect(function(res) {
                let tekst=JSON.parse(res.text);
                let jsonRez=tekst.poruka;
                assert.equal(jsonRez, `Nije moguće rezervisati salu ${zauzece.naziv} za navedeni datum ${zauzece.datum} i termin od ${zauzece.pocetak} do ${zauzece.kraj}!`
                    +` Salu je rezervisala osoba: Test Test (asistent).`);
            })
            .end(done);
    });

    it("Preklapanje periodicnog sa vanrednim", done=> {
        let zauzece= {
            dan: 2,
            semestar: "zimski",
            pocetak: "12:30",
            kraj: "14:15",
            naziv: "1-11",
            ime: "Drugi",
            prezime: "Neko"
        }
        let day="01";
        let month="01";
        let year="2020";
        let posaljiPost={dan:day,mjesec:month, godina:year, zauzece:zauzece};
        supertest
            .post("/upisiRedovnoZauzece")
            .send(posaljiPost)
            .expect(300) //status kad se ne unese zauzece
            .expect(function(res) {
                let tekst=JSON.parse(res.text);
                let jsonRez=tekst.poruka;
                assert.equal(jsonRez, `Nije moguće rezervisati salu ${zauzece.naziv} za navedeni datum ${day}.${month}.${year} i termin od ${zauzece.pocetak} do ${zauzece.kraj}!`
                    +` Salu je rezervisala osoba: Neko Nekic (profesor).`);
            })
            .end(done);
    });

    it("Preklapanje periodicnog sa periodicnim", done=> {
        let zauzece= {
            dan: 0,
            semestar: "zimski",
            pocetak: "12:30",
            kraj: "14:15",
            naziv: "1-11",
            ime: "Drugi",
            prezime: "Neko"
        }
        let day="06";
        let month="01";
        let year="2020";
        let posalji={dan:day,mjesec:month, godina:year, zauzece:zauzece};
        supertest
            .post("/upisiRedovnoZauzece")
            .send(posalji)
            .expect(300) //status kad se ne unese zauzece
            .expect(function(res) {
                let tekst=JSON.parse(res.text);
                let jsonRez=tekst.poruka;
                assert.equal(jsonRez, `Nije moguće rezervisati salu ${zauzece.naziv} za navedeni datum ${day}.${month}.${year} i termin od ${zauzece.pocetak} do ${zauzece.kraj}!`
                    +` Salu je rezervisala osoba: Test Test (asistent).`);
            })
            .end(done);
    });

    it("Osoba istovremeno u dvije sale - izuzetak vanredno", done => {
        let zauzece= {datum:"01.01.2020",pocetak:"12:30",kraj:"14:10",naziv:"1-15",ime:"Neko",prezime:"Nekic"}
        supertest
            .post("/upisiVanrednoZauzece")
            .send((zauzece))
            .expect(300) //status kad se ne unese zauzece
            .expect(function(res) {
                let tekst=JSON.parse(res.text);
                let jsonRez=tekst.poruka;
                assert.equal(jsonRez, `Nije moguće rezervisati salu ${zauzece.naziv} za navedeni datum ${zauzece.datum} i termin od ${zauzece.pocetak} do ${zauzece.kraj}!`
                    +` ISTA osoba (Neko Nekic - profesor) je već rezervisala salu 1-11 u tom terminu.`);
            })
            .end(done);
    });

    it("Osoba istovremeno u dvije sale - izuzetak periodicno", done => {
        let zauzece= {
            dan: 0,
            semestar: "zimski",
            pocetak: "12:30",
            kraj: "14:15",
            naziv: "1-15",
            ime: "Test",
            prezime: "Test"
        }
        let day="06";
        let month="01";
        let year="2020";
        let posalji={dan:day,mjesec:month, godina:year, zauzece:zauzece};
        supertest
            .post("/upisiRedovnoZauzece")
            .send(posalji)
            .expect(300) //status kad se ne unese zauzece
            .expect(function(res) {
                let tekst=JSON.parse(res.text);
                let jsonRez=tekst.poruka;
                assert.equal(jsonRez, `Nije moguće rezervisati salu ${zauzece.naziv} za navedeni datum ${day}.${month}.${year} i termin od ${zauzece.pocetak} do ${zauzece.kraj}!`
                    +` ISTA osoba (Test Test - asistent) je već rezervisala salu 1-11 u tom terminu.`);
            })
            .end(done);
    });
});




