const Sequelize = require("sequelize");

if(process.env.NODE_ENV_PRODUCTION){
    console.log(process.env.JAWSDB_URL);
    var sequelize = new Sequelize(process.env.JAWSDB_URL);
    //var sequelize = new Sequelize('mysql://esv892w1qgrvp7q8:z6ilefna3oyojo1i@eanl4i1omny740jw.cbetxkdyhwsb.us-east-1.rds.amazonaws.com:3306/fpomua71vv25uupl');
}
else{
    console.log('nije ispunjena jednakost');
    var sequelize = new Sequelize("DBWT19","root","",{host:"127.0.0.1",dialect:"mysql",logging:false});
    //var sequelize = new Sequelize('mysql://esv892w1qgrvp7q8:z6ilefna3oyojo1i@eanl4i1omny740jw.cbetxkdyhwsb.us-east-1.rds.amazonaws.com:3306/fpomua71vv25uupl');
}  
const db={};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//import modela
db.Osoblje = sequelize.import(__dirname+'/Osoblje.js');
db.Rezervacija = sequelize.import(__dirname+'/Rezervacija.js');
db.Termin = sequelize.import(__dirname+'/Termin.js');
db.Sala = sequelize.import(__dirname+'/Sala.js');

//relacije, u parovima ili ne?
//Osoblje - jedan na više - Rezervacija
db.Osoblje.hasMany(db.Rezervacija,{foreignKey:'osoba'});
db.Rezervacija.belongsTo(db.Osoblje,{foreignKey:'osoba'});

//Rezervacija ​- jedan na jedan -​ Termin
db.Termin.hasOne(db.Rezervacija,{foreignKey:'termin'});
db.Rezervacija.belongsTo(db.Termin,{foreignKey:'termin'});

//Rezervacija - više na jedan - Sala
db.Sala.hasMany(db.Rezervacija,{foreignKey:'sala'});
db.Rezervacija.belongsTo(db.Sala,{foreignKey:'sala'});

//Sala - jedan na jedan - Osoblje
db.Osoblje.hasOne(db.Sala,{foreignKey:'zaduzenaOsoba'});
db.Sala.belongsTo(db.Osoblje,{foreignKey:'zaduzenaOsoba'});


module.exports=db;
