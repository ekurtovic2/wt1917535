let assert = chai.assert;
var kl=document.getElementById("kalendar");
var aaFirst,aaDana;
aaFirst = [2,5,5,1,3,6,1,4,7,2,5,7];//pon-1...
aaDana=[31,28,31,30,31,30,31,31,30,31,30,31];
Kalendar.iscrtajKalendar(kl,10);
var p=[],v=[],kl;
describe('Kalendar', function() {
 describe('obojiZauzeca i ucitajPodatke-pozivamo sve u trenutnom mjesecu(novembru), klikom na strelicu pored testa se prikazuje se nj prikaz;crvenom je prikazano zauzece', function() {
   it('Pozivanje obojiZauzeca kada podaci nisu učitani: očekivana vrijednost da se ne oboji niti jedan dan u crveno', function() {
     //kl=testovi[0];
     Kalendar.obojiZauzeca(kl,10,"0-01","12:40","13:30");
     let obojeniDani = kl.getElementsByClassName("zauzeta");
     assert.equal(obojeniDani.length, 0,"br obojenih dana u crveno treba biti 0");
   });
   it('Pozivanje obojiZauzeca gdje u zauzećima postoje duple vrijednosti za zauzeće istog termina:'+
   'očekivano je da se dan oboji bez obzira što postoje duple vrijednosti, 23.11 treba biti obojen(u crveno)', function() {
      p=[{dan:5,semestar:"zimski",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"}];
      v=[{datum:"23.11.2019",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"}];
      //kl=testovi[1];
      Kalendar.ucitajPodatke(p,v);
      Kalendar.obojiZauzeca(kl,10,"0-01","12:35","15:00");
      let obojeniDani = kl.getElementsByClassName("zauzeta");
      var obojen23=false;
      for(var i=0;i<obojeniDani.length;i++){
        if(obojeniDani[i].innerHTML=="23") {
          obojen23=true;break;
        }
      }
      assert.equal(obojen23, true,"23.11 treba biti obojen");
     });
     it('Pozivanje obojiZauzece kada u podacima samo postoji periodično zauzeće za drugi semestar: očekivano je da se ne oboji zauzeće(u crveno)', function() {
        p=[{dan:5,semestar:"ljetni",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"}];
        v=[];
        Kalendar.ucitajPodatke(p,v);
        Kalendar.obojiZauzeca(kl,10,"0-01","12:35","15:00");
        let obojeniDani = kl.getElementsByClassName("zauzeta");
        assert.equal(obojeniDani.length, 0,"br obojenih dana u crveno treba biti 0");
       });
     it('Pozivanje obojiZauzece kada u podacima postoji zauzeće termina ali u drugom mjesecu: očekivano je da se ne oboji zauzeće(u crveno)', function() {
        p=[];
        v=[{datum:"23.02.2019",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"}];
        //kl=testovi[1];
        Kalendar.ucitajPodatke(p,v);
        Kalendar.obojiZauzeca(kl,10,"0-01","12:35","15:00");
        let obojeniDani = kl.getElementsByClassName("zauzeta");
        assert.equal(obojeniDani.length, 0,"br obojenih dana u crveno treba biti 0");
       });
     it('Pozivanje obojiZauzece kada u podacima postoji zauzeće termina ali u drugom mjesecu: očekivano je da se ne oboji zauzeće(u crveno)', function() {
        p=[];
        v=[{datum:"23.02.2019",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"}];
        //kl=testovi[1];
        Kalendar.ucitajPodatke(p,v);
        Kalendar.obojiZauzeca(kl,10,"0-01","12:35","15:00");
        let obojeniDani = kl.getElementsByClassName("zauzeta");
        assert.equal(obojeniDani.length, 0,"br obojenih dana u crveno treba biti 0");
       });
     it('Pozivanje obojiZauzece kada u podacima postoji zauzeće termina ali u drugom mjesecu: očekivano je da se ne oboji zauzeće(u crveno)', function() {
        p=[];
        v=[{datum:"23.02.2019",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"}];
        //kl=testovi[1];
        Kalendar.ucitajPodatke(p,v);
        Kalendar.obojiZauzeca(kl,10,"0-01","12:35","15:00");
        let obojeniDani = kl.getElementsByClassName("zauzeta");
        assert.equal(obojeniDani.length, 0,"br obojenih dana u crveno treba biti 0");
       });
     it('Pozivanje obojiZauzece kada u podacima postoji zauzeće termina ali u drugom mjesecu: očekivano je da se ne oboji zauzeće(u crveno)', function() {
        p=[];
        v=[{datum:"23.02.2019",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"}];
        //kl=testovi[1];
        Kalendar.ucitajPodatke(p,v);
        Kalendar.obojiZauzeca(kl,10,"0-01","12:35","15:00");
        let obojeniDani = kl.getElementsByClassName("zauzeta");
        assert.equal(obojeniDani.length, 0,"br obojenih dana u crveno treba biti 0");
       });
     it('Pozivanje obojiZauzece kada su u podacima svi termini u mjesecu zauzeti: očekivano je da se svi dani oboje (u crveno)', function() {
        p=[{dan:0,semestar:"zimski",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"},
          {dan:1,semestar:"zimski",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"},
          {dan:2,semestar:"zimski",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"},
          {dan:3,semestar:"zimski",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"},
          {dan:4,semestar:"zimski",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"},
          {dan:5,semestar:"zimski",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"},
          {dan:6,semestar:"zimski",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"}];
        v=[];
        //kl=testovi[1];
        Kalendar.ucitajPodatke(p,v);
        Kalendar.obojiZauzeca(kl,10,"0-01","12:35","15:00");
        let obojeniDani = kl.getElementsByClassName("zauzeta");
        assert.equal(obojeniDani.length, 30,"svi dani trebaju biti obojeni u crveno");
       });
     it('Dva puta uzastopno pozivanje obojiZauzece: očekivano je da boja zauzeća ostane ista, samo 23.11 treba biti obojen', function() {
        v=[{datum:"23.11.2019",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"}];
        p=[];
        //kl=testovi[1];
        Kalendar.ucitajPodatke(p,v);
        Kalendar.obojiZauzeca(kl,10,"0-01","12:35","15:00");
        Kalendar.obojiZauzeca(kl,10,"0-01","12:35","15:00");
        let obojeniDani = kl.getElementsByClassName("zauzeta");
        var istina=false;
        for(var i=0;i<obojeniDani.length;i++){
          if(obojeniDani[i].innerHTML!="23") {istina=false;break;}
          else istina=true;
        }
        assert.equal(istina, true,"samo 23.11 treba biti obojen");
       });
       it('Dva puta uzastopno pozivanje obojiZauzece: očekivano je da boja zauzeća ostane ista, samo 23.11 treba biti obojen', function() {
          v=[{datum:"23.11.2019",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"}];
          p=[];
          //kl=testovi[1];
          Kalendar.ucitajPodatke(p,v);
          Kalendar.obojiZauzeca(kl,10,"0-01","12:35","15:00");
          Kalendar.obojiZauzeca(kl,10,"0-01","12:35","15:00");
          let obojeniDani = kl.getElementsByClassName("zauzeta");
          var istina=false;
          for(var i=0;i<obojeniDani.length;i++){
            if(obojeniDani[i].innerHTML!="23") {istina=false;break;}
            else istina=true;
          }
          assert.equal(istina, true,"samo 23.11 treba biti obojen");
         });
       it('Pozivanje ucitajPodatke, obojiZauzeca, ucitajPodatke - drugi podaci, obojiZauzeca: očekivano da se'+
       'zauzeća iz prvih podataka ne ostanu obojena, tj. primjenjuju se samo posljednje učitani podaci, treba ostat samo 21.11 obojen u crveno', function() {
          p=[{dan:5,semestar:"zimski",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"}];
          v=[];
          Kalendar.ucitajPodatke(p,v);
          Kalendar.obojiZauzeca(kl,10,"0-01","12:35","15:00");
          v=[{datum:"21.11.2019",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"}];
          p=[];
          Kalendar.ucitajPodatke(p,v);
          Kalendar.obojiZauzeca(kl,10,"0-01","12:35","15:00");
          //kl=testovi[1];
          let obojeniDani = kl.getElementsByClassName("zauzeta");
          var istina=false;
          for(var i=0;i<obojeniDani.length;i++){
            if(obojeniDani[i].innerHTML!="21") {istina=false;break;}
            else istina=true;
          }
          assert.equal(istina, true,"samo 21.11 treba ostat biti obojen");
         });
       it('Proizvoljan 1:postoji jedno zauzece u novembru ali u drugo vrijeme, tj odabrano vrijeme (poc:12:35, kraj:15:00) ne lezi u rasponu zauzeca'+
       ', ocekivano da ne bude niti jedan dan obojen', function() {
          v=[{datum:"21.11.2019",pocetak:"16:00",kraj:"17:00",naziv:"0-01",predavac:"Emir"}];
          Kalendar.ucitajPodatke(p,v);
          Kalendar.obojiZauzeca(kl,10,"0-01","12:35","15:00");
          //kl=testovi[1];
          let obojeniDani = kl.getElementsByClassName("zauzeta");
          assert.equal(obojeniDani.length, 0,"br obojenih dana u crveno treba biti 0");
         });
       it('Proizvoljan 2:postoji jedno zauzece u novembru u podudarno vrijeme ali za drugu salu(ne 0-01), ocekivano da ne bude niti jedan dan obojen', function() {
          v=[{datum:"21.11.2019",pocetak:"12:35",kraj:"15:00",naziv:"1-01",predavac:"Emir"}];
          Kalendar.ucitajPodatke(p,v);
          Kalendar.obojiZauzeca(kl,10,"0-01","12:35","15:00");
          //kl=testovi[1];
          let obojeniDani = kl.getElementsByClassName("zauzeta");
          assert.equal(obojeniDani.length, 0,"br obojenih dana u crveno treba biti 0");
         });
 });
 describe('iscrtajKalendar',function(){
   it('Pozivanje iscrtajKalendar za mjesec sa 30 dana: očekivano je da se prikaže 30 dana, novembar ima 30 dana', function() {
     Kalendar.iscrtajKalendar(kl,10);
     let brDanaBez = kl.getElementsByClassName("bez").length;
     assert.equal(37-brDanaBez, 30,"broj dana treba biti 30");
   });
   it('Pozivanje iscrtajKalendar za mjesec sa 31 dan: očekivano je da se prikaže 31 dan, decembar ima 31 dan', function() {
     Kalendar.iscrtajKalendar(kl,11);
     let brDanaBez = kl.getElementsByClassName("bez").length;
     assert.equal(37-brDanaBez, 31,"broj dana treba biti 31");
   });
   it('Pozivanje iscrtajKalendar za trenutni mjesec: očekivano je da je 1. dan u petak, trenutni mjesec je novembar', function() {
     Kalendar.iscrtajKalendar(kl,10);
     let el=kl.getElementsByClassName("days")[0].children;
     let istina=false,x,prvi=aaFirst[10];
     for(var i=1;i<=el.length;i++){
       if(el[i-1].innerHTML!=" "){
         if(i==prvi) istina=true;//iz formule x=i-first+1, za x=1
         break;
       }
     }
     assert.equal(istina, true,"1. dan treba biti u petak");
   });
   it('Pozivanje iscrtajKalendar za trenutni mjesec: očekivano je da je 30. dan u subotu, trenutni mjesec je novembar', function() {
     Kalendar.iscrtajKalendar(kl,10);
     let el=kl.getElementsByClassName("days")[0].children;
     let istina=false,x,prvi=aaFirst[10];
     for(var i=1;i<=el.length;i++){
       if(el[i-1].innerHTML==30){
         if(i==6 || i==13 || i==20 || i==27 || i==34) istina=true;
         break;
       }
     }
     assert.equal(istina, true,"30. dan treba biti u subotu");
   });
   it('Pozivanje iscrtajKalendar za januar: očekivano je da brojevi dana idu od 1 do 31 počevši od utorka', function() {
     Kalendar.iscrtajKalendar(kl,0);
     let el=kl.getElementsByClassName("days")[0].children;
     let istina=true,x,prvi=aaFirst[0];
     for(var i=1;i<=el.length;i++){
       x=el[i-1].innerHTML;
       if(x==1 && i!=prvi || x!=" " && !(x>=1 && x<=31)) {istina=false;break;}//iz formule x=i-first+1, za x=1
     }
     assert.equal(istina, true,"prvi dan treba biti utorak, te mjesec treba imati 31 dan");
   });
   it('Proizvoljan 1: Pozivanje iscrtajKalendar za februar: očekivano je ima 22. pada u petak', function() {
     Kalendar.iscrtajKalendar(kl,1);
     let el=kl.getElementsByClassName("days")[0].children;
     let istina=false,x,prvi=aaFirst[1];
     for(var i=1;i<=el.length;i++){
       if(el[i-1].innerHTML==22 && (i==5 || i==12 || i==19 || i==26 || i==33)) {istina=true;break;}
     }
     assert.equal(istina, true,"22. treba biti u petak");
   });
   it('Proizvoljan 1: Pozivanje iscrtajKalendar za februar: očekivano je ima 4 ponedjeljka', function() {
     Kalendar.iscrtajKalendar(kl,1);
     let el=kl.getElementsByClassName("days")[0].children;
     let istina=false,x,prvi=aaFirst[1],br=0;
     for(var i=1;i<=el.length;i++){
       if(el[i-1].innerHTML!=" " && (i==1 || i==8 || i==15 || i==22 || i==29 || i==36)) br++;
     }
     assert.equal(br, 4,"treba imati 4 ponedjeljka");
   });



 })
});
