var divSadrzaj,divGrid,dpSljedeci,dpPrethodni,redniBroj=0,prethodne=[],slika1,slika2,slika3,sveUcitane=false,maxRedniBroj=-1;
//maxRedniBroj-za slucaj da dugo traje ucitavanje zadnjih slika pa da se poremeti redniBroj ako se ispritiskaju sljedeci ili prethodni...
var prikazanaJedna=false,prikazaneDvije=false;

divSadrzaj=document.getElementById("sadrzaj");
divGrid=document.getElementById("grid");
dpSljedeci=document.getElementById("sljedeci");
dpPrethodni=document.getElementById("prethodni");
slika1=document.getElementById("slika1");
slika2=document.getElementById("slika2");
slika3=document.getElementById("slika3");
dpPrethodni.disabled=true;

Pozivi.ucitajSlikeAjax(divGrid,redniBroj);



dpSljedeci.addEventListener("click",function(ev){
  redniBroj++;
  if(maxRedniBroj!=-1 && redniBroj>maxRedniBroj) redniBroj=maxRedniBroj;

  let brNeprikazanih=0;

  if(redniBroj*3>prethodne.length-1) Pozivi.ucitajSlikeAjax(divGrid,redniBroj);
  else {
    if(prethodne.length-(redniBroj+1)*3>=-2 && prethodne.length-(redniBroj+1)*3<=0 && sveUcitane==true) dpSljedeci.disabled=true;

    if(redniBroj*3<prethodne.length) slika1.src=prethodne[redniBroj*3];
    if(redniBroj*3+1<prethodne.length) slika2.src=prethodne[redniBroj*3+1];
    else{
      slika2.src="";//slika2.style.visibility = "hidden";
      slika2.style.display="none";
      brNeprikazanih++;
    }
    if(redniBroj*3+2<prethodne.length) slika3.src=prethodne[redniBroj*3+2];
    else{
      slika3.src="";//slika3.style.visibility = "hidden";
      slika3.style.display="none";
      brNeprikazanih++;
    }

    if(brNeprikazanih==1) {
      prikazanaJedna=false;prikazaneDvije=true;
      divGrid.classList.remove("klasaJedna");
      divGrid.classList.add("klasaDvije");
    }
    else if(brNeprikazanih==2){
      prikazanaJedna=true;prikazaneDvije=false;
      divGrid.classList.add("klasaJedna");
      divGrid.classList.remove("klasaDvije");
    }


  }

  //if(redniBroj==3) {dpSljedeci.disabled=true;}
  if(redniBroj!=0) {dpPrethodni.disabled=false;}//suvisan uslov, uklonit kasnije


},false);
dpPrethodni.addEventListener("click",function(ev){
  redniBroj--;
  if(maxRedniBroj!=-1 && redniBroj>maxRedniBroj) redniBroj=maxRedniBroj;

  if(slika2.style.display="none") slika2.style.display="";
  if(slika3.style.display="none") slika3.style.display="";
  prikazanaJedna=false;prikazaneDvije=false;//izgleda suvisne, obrisat kasnije
  //najbolje sa klasama
  //sad cemo koristiti classList za razliku od spirale 2 ovu, lagano promijeiti ako treba podrzavati ie9
  divGrid.classList.remove("klasaJedna");
  divGrid.classList.remove("klasaDvije");

  slika1.src=prethodne[redniBroj*3];
  slika2.src=prethodne[redniBroj*3+1];
  slika3.src=prethodne[redniBroj*3+2];

  if(redniBroj==0) {dpPrethodni.disabled=true;}
  if(redniBroj!=3) {dpSljedeci.disabled=false;}//suvisan uslov,uklonit kasnije

},false);
