let Pozivi = (function(){
  //wrap cemo sa promise radi testova, mogli i instalirat supertest il ...
  //privatni
  //metode
  function ucitajZauzecaAjaxImpl(stranica=0){
    return new Promise(function(resolve,reject){
      var ajax=new XMLHttpRequest();
      ajax.onreadystatechange=function(){
        if (ajax.readyState == 4 && ajax.status == 200){
          if(stranica===0){
            let obj,per,van;
            console.log("Na pocetku ucitani podaci: "+ajax.responseText);
            obj=JSON.parse(ajax.responseText);
            per=obj.periodicna;
            van=obj.vanredna;
            Kalendar.ucitajPodatke(per,van);
            resolve(ajax.responseText);
          }
          else{
            console.log('pozvana');

            for(let i=0;i<osobljeDjeca.length;i++){
              osobljeDjeca[i].textContent=pocPredavaci[i];
            }

            let obj,per,van;
            console.log("Na pocetku ucitani podaci: "+ajax.responseText);
            obj=JSON.parse(ajax.responseText);
            per=obj.periodicna;
            van=obj.vanredna;
            //mozemo ovdje ili na serveru, mrsko mi tamo..
            //mozemo kroz sve predavace ili kroz sva zauzeca(vjer efikasnije)
            let trDatum=new Date();
            let trMjesec=trDatum.getMonth(),trDanUmjesecu=trDatum.getDate(),trDanUsedmici=trDatum.getDay();//trMjesec od 0, trDanUsedmici od 1-ned 0, prepravit u 7
            let trSat=trDatum.getHours(),trMinute=trDatum.getMinutes();
            if(trDanUsedmici==0) trDanUsedmici=7;
            if(trSat<=9) trSat='0'+trSat;
            if(trMinute<=9) trMinute='0'+trMinute;

            console.log('mj,dan,danSedmica,sat,min'+trMjesec+' '+trDanUmjesecu+' '+trDanUsedmici+' '+trSat+' '+trMinute);

            for(let i=0;i<per.length;i++){
              z=per[i];
              zDanSedmica=z.dan+1;zSemestar=z.semestar;zPocetak=z.pocetak;zKraj=z.kraj;zSala=z.naziv;zPredavac=z.predavac;//zDanSedmica je od 1-uvecali

              zPocetak=zPocetak.split(":");
              zKraj=zKraj.split(":");
              zPocetakH=parseInt(zPocetak[0]);
              zPocetakM=parseInt(zPocetak[1]);
              zKrajH=parseInt(zKraj[0]);
              zKrajM=parseInt(zKraj[1]);

              if(trDanUsedmici==zDanSedmica && (zPocetakH<trSat || zPocetakH==trSat && zPocetakM<=trMinute) && (zKrajH>trSat || zKrajH==trSat && zKrajM>=trMinute) &&
                (zSemestar=="zimski" && (trMjesec>=9 && trMjesec <=11 || trMjesec==0) || zSemestar=="ljetni" && trMjesec>=1 && trMjesec <=5)){
                for(let p=0;p<osobljeDjeca.length;p++){
                  let tekst=osobljeDjeca[p].textContent;
                  if(tekst==zPredavac) osobljeDjeca[p].textContent+=' je u sali '+zSala;
                  else if(tekst.slice(0,zPredavac.length)==zPredavac) osobljeDjeca[p].textContent+=' ili u sali '+zSala;
                }
              }
            }

            for(let i=0;i<van.length;i++){
              z=van[i];
              zDatum=z.datum;zSemestar=z.semestar;zPocetak=z.pocetak;zKraj=z.kraj;zSala=z.naziv;zPredavac=z.predavac;
              zDatum=zDatum.split(".");
              zDanMjesec=parseInt(zDatum[0]);
              zMjesec=parseInt(zDatum[1])-1;

              zPocetak=zPocetak.split(":");
              zKraj=zKraj.split(":");
              zPocetakH=parseInt(zPocetak[0]);
              zPocetakM=parseInt(zPocetak[1]);
              zKrajH=parseInt(zKraj[0]);
              zKrajM=parseInt(zKraj[1]);

              let vzDatum=new Date(2022,zMjesec,zDanMjesec);
              zDanSedmica=vzDatum.getDay()
              if(zDanSedmica==0) zDanSedmica=7;

              if(trDanUmjesecu==zDanMjesec && trMjesec==zMjesec &&
                (zPocetakH<trSat || zPocetakH==trSat && zPocetakM<=trMinute) && (zKrajH>trSat || zKrajH==trSat && zKrajM>=trMinute)){
                for(let p=0;p<osobljeDjeca.length;p++){
                  let tekst=osobljeDjeca[p].textContent;
                  if(tekst==zPredavac) osobljeDjeca[p].textContent+=' je u sali '+zSala;
                  else if(tekst.slice(0,zPredavac.length)==zPredavac) osobljeDjeca[p].textContent+=' ili u sali '+zSala;
                }
              }
            }
            //mogli i prvo ovo..
            for(let i=0;i<osobljeDjeca.length;i++){
              if(pocPredavaci[i]===osobljeDjeca[i].textContent) osobljeDjeca[i].textContent+=' je u kancelariji';
            }

          }
        }
        if (ajax.readyState == 4 && ajax.status == 404)
          console.log('greska 404');
      }
      ajax.open("GET", "/zauzeca", true);
      ajax.send();
    });
  }
  function upisiZauzeceAjaxImpl(divKalendar,period,oDanSedmica,oDanMjesec,oMjesec,oSemestar,oDatum,oPocetak,oKraj,oSala,oPredavac,oPredavacId){
    return new Promise(function(resolve,reject){
      console.log("zahtjev za upis: "+period+" "+oDanSedmica+" "+oDanMjesec+" "+oMjesec+" "+oSemestar+" "+oDatum+" "+oPocetak+" "+oKraj+" "+oSala+" "+oPredavac);
      //provjera da li postoji zauzece
      var ajax=new XMLHttpRequest();
      ajax.onreadystatechange=function(){
        if (ajax.readyState == 4 && ajax.status == 200){
          let odg=ajax.responseText,obj,per,van;
          //nastaviti,2 slucaja
          if(odg.charAt(0)=='{'){
            console.log('da');
            obj=JSON.parse(ajax.responseText);
            per=obj.periodicna;
            van=obj.vanredna;
            Kalendar.ucitajPodatke(per,van);
            Kalendar.obojiZauzeca(divKalendar,oMjesec,oSala,oPocetak,oKraj);
          }
          else{
            alert(ajax.responseText);
            ucitajZauzecaAjaxImpl().then(function(){
              Kalendar.obojiZauzeca(divKalendar,oMjesec,oSala,oPocetak,oKraj);
            });
            //Kalendar.obojiZauzeca(divKalendar,oMjesec,oSala,oPocetak,oKraj);
          }
          console.log("ajax odgovor"+ajax.responseText);
          resolve(ajax.responseText);
        }
        if (ajax.readyState == 4 && ajax.status == 404)
          console.log('greska 404');
      }
      ajax.open("POST", "/zauzeca", true);
      ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      ajax.send("period="+period+"&oDanSedmica="+oDanSedmica+"&oDanMjesec="+oDanMjesec+"&oMjesec="+oMjesec+"&oSemestar="+oSemestar+"&oDatum="+oDatum+"&oPocetak="+oPocetak+"&oKraj="+oKraj+"&oSala="+oSala+"&oPredavac="+oPredavac+"&oPredavacId="+oPredavacId);
    });
  }
  function ucitajSlikeAjaxImpl(divGrid,redniBroj){
    var ajax=new XMLHttpRequest();
    ajax.onreadystatechange=function(){
      if (ajax.readyState == 4 && ajax.status == 200){
        let odg=ajax.responseText;console.log(ajax.responseText);
        odg=odg.split(',');
        slika1.src=odg[1];console.log(odg[1]);
        prethodne.push(odg[1]);
        if(odg[0]=='<') {}
        else if(odg[0]=='0' || odg[0]=='>') {
          slika2.src=odg[2];
          prethodne.push(odg[2]);
          slika3.src=odg[3];
          prethodne.push(odg[3]);
          if(odg[0]=='0') {
            dpSljedeci.disabled=true;
            sveUcitane=true;
          }
        }
        else if(odg[0]=='-1'){
          slika2.src=odg[2];
          prethodne.push(odg[2]);
          slika3.src="";
          slika3.style.display="none";
          dpSljedeci.disabled=true;
          sveUcitane=true;maxRedniBroj=redniBroj;
          prikazanaJedna=false;prikazaneDvije=true;
          divGrid.classList.remove("klasaJedna");
          divGrid.classList.add("klasaDvije");
        }
        else {
          slika2.src="";
          slika2.style.display="none";
          slika3.src="";
          slika3.style.display="none";
          dpSljedeci.disabled=true;
          sveUcitane=true;maxRedniBroj=redniBroj;
          prikazanaJedna=true;prikazaneDvije=false;
          divGrid.classList.add("klasaJedna");
          divGrid.classList.remove("klasaDvije");
        }
      }
      if (ajax.readyState == 4 && ajax.status == 404)
        console.log('greska 404');
    }
    ajax.open("GET", "/slike?br="+redniBroj, true);
    ajax.send();
  }

  function ucitajOsobeAjaxImpl(divOsoblje,stranica=0){
    return new Promise(function(resolve,reject){
      var ajax=new XMLHttpRequest();
      ajax.onreadystatechange=function(){
        if (ajax.readyState == 4 && ajax.status == 200){
          if(stranica===0){
            let obj;
            obj=JSON.parse(ajax.responseText);
            console.log('ajax odg, odoblje: '+ajax.responseText);
            obj.forEach((item, i) => {
              let option=document.createElement("option");
              option.text=item.uloga+' '+item.ime+' '+item.prezime+' id: '+item.id;
              option.idOsobe=item.id;//vise osoba s istim imenom i prezimenom..
              divOsoblje.add(option);
            });
            resolve(ajax.responseText);
          }
          else{
            let obj;
            obj=JSON.parse(ajax.responseText);
            obj.forEach((item, i) => {
              let o=document.createElement('li');
              o.appendChild(document.createTextNode(item.uloga+' '+item.ime+' '+item.prezime+' id: '+item.id));
              divOsoblje.appendChild(o);
            });
            //mozemo s promise al ne moramo
            osobljeDjeca=olOsobljeSala.children;
            for(let i=0;i<osobljeDjeca.length;i++){pocPredavaci.push(osobljeDjeca[i].textContent);}
            ucitajZauzecaAjaxImpl(1);
          }
        }
        if (ajax.readyState == 4 && ajax.status == 404)
          console.log('greska 404');
      }
      ajax.open("GET", "/osoblje", true);
      ajax.send();
    });
  }

  function resetAjaxImpl(){
    return new Promise(function(resolve,reject){
      var ajax=new XMLHttpRequest();
      ajax.onreadystatechange=function(){
        if (ajax.readyState == 4 && ajax.status == 200){
          console.log("ajax odgovor"+ajax.responseText);
          resolve(ajax.responseText);
        }
        if (ajax.readyState == 4 && ajax.status == 404)
          console.log('greska 404');
      }
      ajax.open("POST", "/podaciReset", true);
      ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      ajax.send();
    });
  }

  function ucitajSaleAjaxImpl(sSale){
    return new Promise(function(resolve,reject){
      var ajax=new XMLHttpRequest();
      ajax.onreadystatechange=function(){
        if (ajax.readyState == 4 && ajax.status == 200){
          let obj;
          obj=JSON.parse(ajax.responseText);
          console.log('ajax odg, sale: '+ajax.responseText);
          obj.forEach((item, i) => {
            let option=document.createElement("option");
            option.text=item.naziv;
            sSale.add(option);
          });
          resolve(ajax.responseText);
        }
        if (ajax.readyState == 4 && ajax.status == 404)
          console.log('greska 404');
      }
      ajax.open("GET", "/sale", true);
      ajax.send();
    });
  }
return {
ucitajZauzecaAjax: ucitajZauzecaAjaxImpl,
upisiZauzeceAjax:upisiZauzeceAjaxImpl,
ucitajSlikeAjax:ucitajSlikeAjaxImpl,
ucitajOsobeAjax:ucitajOsobeAjaxImpl,
resetAjax:resetAjaxImpl,
ucitajSaleAjax:ucitajSaleAjaxImpl
}
}());
