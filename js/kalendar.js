let Kalendar = (function(){
  var aPeriodicna=[],aVanredna=[],months,aFirst,aDana,imenaDana;
  //mozemo sve u jedan niz objekata, ali ovako manje pisanja
  months = ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", "August", "Septembar", "Oktobar", "Novembar", "Decembar"];
  imenaDana=["PON","UTO","SRI","ČET","PET","SUB","NED"];
//dakle prosljeduju joj se kalendarRef,prikazaniMjesec ili neki dr,odabrana sala li neka dr,odabrani pocetak i odabrani kraj ili neki dr
//dodat provjere za gore
function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj){
  var bPocetak=pocetak,bKraj=kraj;//spirala 3
  var djecaDani=kalendarRef.getElementsByClassName("days")[0].children;
  var z,zDatum,zDan,zMjesec,zSemestar,zPocetak,zPocetakH,zPocetakM,zKraj,zKrajH,zKrajM,zSala,klZauzeta="zauzeta",nalazi;
  var oPocetakH,oPocetakM,oKrajH,oKrajM;
  pocetak=pocetak.split(":");
  kraj=kraj.split(":");
  oPocetakH=parseInt(pocetak[0]);
  oPocetakM=parseInt(pocetak[1]);
  oKrajH=parseInt(kraj[0]);
  oKrajM=parseInt(kraj[1]);
  //brisemo ako se ne nalazi medu novim + dodajemo nove zauzete;dakle idemo kroz dana i zadano vrijeme
  for(let i=1;i<=37;i++){
    let djete=djecaDani[i-1];//dakle po referenci ide?
    nalazi=false;
    if((' ' + djete.className + ' ').indexOf(' bez ') > -1) continue;
    //oKraj<=oPocetak
    if(!(oPocetakH<oKrajH || oPocetakH==oKrajH && oPocetakM<oKrajM)){
      djete.className=djete.className.replace(/\bzauzeta\b/g, "slobodna");
      djete.onclick=null;
      djete.style.cursor='default';
      continue;
    }
    //periodicna
    for(var j=0;j<aPeriodicna.length;j++){
      z=aPeriodicna[j];
      zDan=z.dan+1;zSemestar=z.semestar;zPocetak=z.pocetak;zKraj=z.kraj;zSala=z.naziv;
      //console.log("boji periodicno, dan:"+zDan);
      //dan
      if(djete.innerHTML==" " || ((zDan==1 || zDan==2) && !(i==zDan || i==zDan+7 || i==zDan+14 || i==zDan+21 || i==zDan+28 || i==zDan+35)) ||
                              (!(zDan==1 || zDan==2)&& !(i==zDan || i==zDan+7 || i==zDan+14 || i==zDan+21 || i==zDan+28 ) )) continue;
      //console.log(zDan+" "+i);
      zPocetak=zPocetak.split(":");
      zKraj=zKraj.split(":");
      zPocetakH=parseInt(zPocetak[0]);
      zPocetakM=parseInt(zPocetak[1]);
      zKrajH=parseInt(zKraj[0]);
      zKrajM=parseInt(zKraj[1]);
      //console.log(zKrajH);
      //sala,poc i kraj,sem
      //gore osigurano da je oPocetak<oKraj, a u ucitajPodatke da je zPocetak<zKraj
      if(sala==zSala && !((zKrajH<oPocetakH || zKrajH==oPocetakH && zKrajM<=oPocetakM)||(oKrajH<zPocetakH || oKrajH==zPocetakH && oKrajM<=zPocetakM))&&
        (zSemestar=="zimski" && (mjesec>=9 && mjesec <=11 || mjesec==0) || zSemestar=="ljetni" && mjesec>=1 && mjesec <=5)){
        nalazi=true;
        break;
      }
    }
    //
    if(nalazi){
      //console.log("nalazi");
      djete.className=djete.className.replace(/\bslobodna\b/g, "zauzeta");
      djete.onclick=null;
      djete.style.cursor='default';
      continue;
    }
    //vanredna
    for(j=0;j<aVanredna.length;j++){
      z=aVanredna[j];
      //console.log(z);
      zDatum=z.datum;zSemestar=z.semestar;zPocetak=z.pocetak;zKraj=z.kraj;zSala=z.naziv;
      zDatum=zDatum.split(".");
      zDan=parseInt(zDatum[0]);//ovdje zDan predstavlja redni broj u mjesecu!
      zMjesec=parseInt(zDatum[1])-1;

      //dan, mjesec
      if(djete.innerHTML!=zDan || zMjesec!=mjesec) continue;
      zPocetak=zPocetak.split(":");
      zKraj=zKraj.split(":");
      zPocetakH=parseInt(zPocetak[0]);
      zPocetakM=parseInt(zPocetak[1]);
      zKrajH=parseInt(zKraj[0]);
      zKrajM=parseInt(zKraj[1]);
      if(sala==zSala && !((zKrajH<oPocetakH || zKrajH==oPocetakH && zKrajM<=oPocetakM)||(oKrajH<zPocetakH || oKrajH==zPocetakH && oKrajM<=zPocetakM))){
        console.log("boji vanredno, datum:"+zDan+"."+zDatum[1]);
        nalazi=true;
        break;
      }
    }
    //
    if(nalazi) {
      djete.className=djete.className.replace(/\bslobodna\b/g, "zauzeta");
      djete.onclick=null;
      djete.style.cursor='default';
    }
    else {
      djete.className=djete.className.replace(/\bzauzeta\b/g, "slobodna");
      djete.onclick=function(){
        if(confirm('Da li zelite rezervisati odabrani termin?')) {
          let xDanMjesec=djete.innerHTML;let xDanSedmica=i%7;
          if(xDanSedmica==0) xDanSedmica=7;
          console.log("zeli rezervisati (danMjesec,i) "+xDanMjesec +" "+i);
          upisiZauzece(xDanMjesec,xDanSedmica,bPocetak,bKraj,sala);
        }
      }
      djete.style.cursor='pointer';
    }
  }
}
function ucitajPodatkeImpl(periodicna, vanredna){
  var z,zDatum,zDan,zMjesec,zSemestar,zPocetak,zPocetakH,zPocetakM,zKraj,zKrajH,zKrajM,zSala,zPredavac,reg;
  //periodicna
  aPeriodicna=[];
  aVanredna=[];
  for(var i=0;i<periodicna.length;i++){
    z=periodicna[i];
    zDan=z.dan;zSemestar=z.semestar;zPocetak=z.pocetak;zKraj=z.kraj;zSala=z.naziv;zPredavac=z.predavac;
    //dan, vidjet trebaju li detaljnije provjere..
    if(typeof(zDan)!="number" || !(zDan==0 || zDan==1 || zDan==2 || zDan==3 || zDan==4 || zDan==5 || zDan==6)) continue;
    //semestar
    if(typeof(zSemestar)!="string" || !(zSemestar=="zimski" || zSemestar=="ljetni")) continue;
    //pocetak,kraj
    reg=/^[0-9][0-9]:[0-9][0-9]$/;//dalja testiranja u kodu..
    if(typeof(zPocetak)!="string" || typeof(zKraj)!="string" || !reg.test(zPocetak) || !reg.test(zKraj)) continue;
    zPocetak=zPocetak.split(":");
    zKraj=zKraj.split(":");
    zPocetakH=parseInt(zPocetak[0]);
    zPocetakM=parseInt(zPocetak[1]);
    zKrajH=parseInt(zKraj[0]);
    zKrajM=parseInt(zKraj[1]);
    if(!(zPocetakH<zKrajH || zPocetakH==zKrajH && zPocetakM<zKrajM)) continue;//zkraj<=zpocetak
    //dalja testiranja nisu potrebna-kao 92:52..osigurana u obojiZauzeca
    //spirala 3 sad ipak treba testirati
    if(zPocetakH<0 || zPocetakH>23 || zPocetakM<0 || zPocetakM>59) continue;
    //sala,predavac
    if(!typeof(zSala)=="string" || !typeof(zPredavac)=="string") continue;

    aPeriodicna.push(z);
  }
  //vanredna
  for(i=0;i<vanredna.length;i++){
    z=vanredna[i];
    zDatum=z.datum;zSemestar=z.semestar;zPocetak=z.pocetak;zKraj=z.kraj;zSala=z.naziv;zPredavac=z.predavac;
    console.log("treba da se ucita u modul: "+zDatum);
    //datum ,dan,mjesec
    reg=/^[0-9][0-9]\.[0-9][0-9]\.2022$/;
    if(typeof(zDatum)!="string" || !reg.test(zDatum)) continue;
    //dalja testiranja nisu potrebna-kao 34.08..osigurana u obojiZauzeca
    //dan i mjesec, testirani u obojiZauzeca
    //pocetak,kraj-isto ko za periodicne, stavit u f-ju
    reg=/^[0-9][0-9]:[0-9][0-9]$/;//dalja testiranja u kodu..
    if(typeof(zPocetak)!="string" || typeof(zKraj)!="string" || !reg.test(zPocetak) || !reg.test(zKraj)) continue;
    zPocetak=zPocetak.split(":");
    zKraj=zKraj.split(":");
    zPocetakH=parseInt(zPocetak[0]);
    zPocetakM=parseInt(zPocetak[1]);
    zKrajH=parseInt(zKraj[0]);
    zKrajM=parseInt(zKraj[1]);
    if(!(zPocetakH<zKrajH || zPocetakH==zKrajH && zPocetakM<zKrajM)) continue;
    //spirala 3 sad ipak treba testirati
    if(zPocetakH<0 || zPocetakH>23 || zPocetakM<0 || zPocetakM>59) continue;
    if(!typeof(zSala)=="string" || !typeof(zPredavac)=="string") continue;

    aVanredna.push(z);
    console.log("ucitana u modul: "+zDatum);
  }
}
//dodat provjere za mjesec
function iscrtajKalendarImpl(kalendarRef, mjesec){
  var first,dana,el,div,x,trenutnaGodina;
  //sve ovo da bi bio neovisan, rad testova;za sad ovako najlaske..
  //dani
  el=kalendarRef.getElementsByClassName("days");
  if(el.length!=0) kalendarRef.removeChild(el[0]);
  el = document.createElement("div");
  el.className += " " + "days";
  kalendarRef.insertBefore(el,kalendarRef.firstElementChild);
  //update 26.02.2022 - zamijeniti 2020 -> trenutnu godinu
  trenutnaGodina=new Date().getFullYear()
  first = new Date(trenutnaGodina,mjesec,1).getDay();
  if(first==0) first=7;
  dana=new Date(trenutnaGodina,mjesec+1,0).getDate();

  for(var i=1;i<=37;i++){
    div = document.createElement("div");
    div.className += " " + "grid-item";
    if(i>=8) div.className += " " + "sakriti";
    x=i-first+1;
    if(x>0 && x<=dana) {
      div.innerHTML=x;
      div.className += " " + "slobodna";
    }
    else{
      div.innerHTML=" ";
      div.className += " " + "bez";
    }
    el.appendChild(div);
  }
  //br-izmedu
  el=kalendarRef.getElementsByClassName("izmedu");
  if(el.length!=0) kalendarRef.removeChild(el[0]);
  el = document.createElement("br");
  el.className += " " + "izmedu";
  kalendarRef.insertBefore(el,kalendarRef.firstElementChild);
  //div-weekdays
  el=kalendarRef.getElementsByClassName("weekdays");
  if(el.length!=0) kalendarRef.removeChild(el[0]);
  el = document.createElement("div");
  el.className += " " + "weekdays";
  kalendarRef.insertBefore(el,kalendarRef.firstElementChild);
  for(var k=0;k<7;k++){
    div = document.createElement("div");
    div.className += " " + "grid-item";
    div.innerHTML=imenaDana[k];
    el.appendChild(div);
  }
  //p-ime mjeseca
  el=kalendarRef.getElementsByClassName("imeMjeseca");
  if(el.length!=0) kalendarRef.removeChild(el[0]);
  el = document.createElement("p");
  el.innerHTML=months[mjesec];
  el.className += " " + "imeMjeseca";
  kalendarRef.insertBefore(el,kalendarRef.firstElementChild);
}
return {
obojiZauzeca: obojiZauzecaImpl,
ucitajPodatke: ucitajPodatkeImpl,
iscrtajKalendar: iscrtajKalendarImpl
}
}());
////////////////////////////////////////////////////
//djete->dijete heh
//u 3.spirali mijenjamo da bude zauzeto ako postoji zauzece unutar raspona, tj ne mora citav biti zauzet
//dakle uslov bio [oPocetak,oKraj]<=[zPocetak,zKraj] a sada da nije tacno da je citav slobodan
//tj !(zKraj<=oPocetak || oKraj<=zPocetak)
////////////////////////////////////////////////////
