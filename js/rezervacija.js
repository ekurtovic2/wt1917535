//mozemo i ovo mozda u modul stavit kasnije
var prikazaniMjesec,divKalendar,dSljedeci,dPrethodni,sSale,iPocetak,iKraj,divOsoblje;
var sSaleTekst,iPocetakTekst,iKrajTekst;
//dugmadi,elementi i eventi
divKalendar=document.getElementById("kalendar");
prikazaniMjesec=new Date().getMonth();
dSljedeci=document.getElementById("sljedeci");
dPrethodni=document.getElementById("prethodni");
sSale=document.getElementById("sale");

iPocetak=document.getElementById("pocetak");
iKraj=document.getElementById("kraj");
Kalendar.iscrtajKalendar(divKalendar,prikazaniMjesec);
if(prikazaniMjesec==11) {dSljedeci.disabled=true;}
if(prikazaniMjesec==0) {dPrethodni.disabled=true;}

divOsoblje=document.getElementById("osoblje");
//sOsobljeTekst=divOsoblje.options[divOsoblje.selectedIndex].text;
//eventi
dSljedeci.addEventListener("click",function(ev){
  prikazaniMjesec++;
  if(typeof(sSale.options[sSale.selectedIndex])!="undefined") sSaleTekst=sSale.options[sSale.selectedIndex].text;
  Kalendar.iscrtajKalendar(divKalendar,prikazaniMjesec);
  if(prikazaniMjesec==11) {dSljedeci.disabled=true;}
  if(prikazaniMjesec!=0) {dPrethodni.disabled=false;}
  console.log(prikazaniMjesec+" "+sSaleTekst+" "+iPocetakTekst+" "+iKrajTekst);
  if(typeof(iPocetakTekst)!="undefined" && typeof(iKrajTekst)!="undefined" && typeof(sSaleTekst)!="undefined")
    Kalendar.obojiZauzeca(divKalendar,prikazaniMjesec,sSaleTekst,iPocetakTekst,iKrajTekst);
},false);
dPrethodni.addEventListener("click",function(ev){
  prikazaniMjesec--;
  if(typeof(sSale.options[sSale.selectedIndex])!="undefined") sSaleTekst=sSale.options[sSale.selectedIndex].text;
  Kalendar.iscrtajKalendar(divKalendar,prikazaniMjesec);
  if(prikazaniMjesec==0) {dPrethodni.disabled=true;}
  if(prikazaniMjesec!=11) {dSljedeci.disabled=false;}
  console.log(prikazaniMjesec+" "+sSaleTekst+" "+iPocetakTekst+" "+iKrajTekst);
  if(typeof(iPocetakTekst)!="undefined" && typeof(iKrajTekst)!="undefined" && typeof(sSaleTekst)!="undefined")
    Kalendar.obojiZauzeca(divKalendar,prikazaniMjesec,sSaleTekst,iPocetakTekst,iKrajTekst);
},false);
sSale.addEventListener("change",function(ev){
  sSaleTekst=sSale.options[sSale.selectedIndex].text;
  console.log(prikazaniMjesec+" "+sSaleTekst+" "+iPocetakTekst+" "+iKrajTekst);
  if(typeof(iPocetakTekst)!="undefined" && typeof(iKrajTekst)!="undefined")
    Kalendar.obojiZauzeca(divKalendar,prikazaniMjesec,sSaleTekst,iPocetakTekst,iKrajTekst);
},false);
iPocetak.addEventListener("change",function(ev){
  if(typeof(sSale.options[sSale.selectedIndex])!="undefined") sSaleTekst=sSale.options[sSale.selectedIndex].text;
  iPocetakTekst=iPocetak.value.split(":");
  iPocetakTekst=iPocetakTekst[0]+":"+iPocetakTekst[1];
  console.log(prikazaniMjesec+" "+sSaleTekst+" "+iPocetakTekst+" "+iKrajTekst);
  if(typeof(iKrajTekst)!="undefined" && typeof(sSaleTekst)!="undefined")
    Kalendar.obojiZauzeca(divKalendar,prikazaniMjesec,sSaleTekst,iPocetakTekst,iKrajTekst);
},false);
iKraj.addEventListener("change",function(ev){
  if(typeof(sSale.options[sSale.selectedIndex])!="undefined") sSaleTekst=sSale.options[sSale.selectedIndex].text;
  iKrajTekst=iKraj.value.split(":");
  iKrajTekst=iKrajTekst[0]+":"+iKrajTekst[1];
  console.log(prikazaniMjesec+" "+sSaleTekst+" "+iPocetakTekst+" "+iKrajTekst);
  if(typeof(iPocetakTekst)!="undefined" && typeof(sSaleTekst)!="undefined")
    Kalendar.obojiZauzeca(divKalendar,prikazaniMjesec,sSaleTekst,iPocetakTekst,iKrajTekst);
},false);
//hardkodirani podaci
var pperiodicna=[{dan:5,semestar:"zimski",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"}];
var pvanredna=[{datum:"21.11.2019",pocetak:"12:35",kraj:"15:00",naziv:"0-01",predavac:"Emir"}];
Kalendar.ucitajPodatke(pperiodicna,pvanredna);
//Kalendar.obojiZauzeca(document.getElementById("kalendar"),10,"0-01","12:35","15:00");
//pregeldat imal gresaka, duple deklaracije...;
//kad budem imao vremena kod poboljsat, duplirani->u fje...

//spirala3...
Pozivi.ucitajZauzecaAjax();//vidjet dal mora u onload, msm da ne treba
Pozivi.ucitajOsobeAjax(divOsoblje);
Pozivi.ucitajSaleAjax(sSale);
//mozemo i ovdje implementirati omogucavanje klika na dan, umjesto u modulu kalendar ako bude da ne spada u funkcionalnost tamo, ali je tamo manje prolaza i koda
//nije precizirano gdje mora, pa cemo tamo za sad

function upisiZauzece(danMjesec,danSedmica,oPocetak,oKraj,oSala){
  //nije ni potreban prije
  let sOsobljeTekst,sOsobljeId;
  if(typeof(divOsoblje.options[divOsoblje.selectedIndex])!="undefined"){
    sOsobljeTekst=divOsoblje.options[divOsoblje.selectedIndex].text;
    sOsobljeId=divOsoblje.options[divOsoblje.selectedIndex].idOsobe;
  }
  else{
    alert('nisu se jos ucitale osobe');
    return;
  }
  let cPeriodicnost=document.getElementById("periodicnost").checked;

  let period=false,oSemestar="nijedan",datum,uprikazaniMjesec=prikazaniMjesec+1,prikazaniDanMjesec=danMjesec;
  if(cPeriodicnost) period=true;
  if(period && prikazaniMjesec>=6 && prikazaniMjesec<=8) {
    alert('Ne moze se izvrsiti periodicno zauzece u mjesesecima koji ne pripadaju niti jednom semestru');
    Kalendar.obojiZauzeca(divKalendar,prikazaniMjesec,oSala,oPocetak,oKraj);
    //necemo sad slati zahtjev za novim, vec samo kad prode do serverske provjere
    return;
  }

  if(prikazaniMjesec>=1 && prikazaniMjesec<=5) oSemestar="ljetni";
  else if(prikazaniMjesec>=9 && prikazaniMjesec <=11 || prikazaniMjesec==0) oSemestar="zimski";

  if(prikazaniDanMjesec<=9) prikazaniDanMjesec="0"+prikazaniDanMjesec;
  if(uprikazaniMjesec<=9) uprikazaniMjesec="0"+uprikazaniMjesec;

  oDatum=prikazaniDanMjesec+"."+uprikazaniMjesec+".2022";

  //console.log('id osobe: '+sOsobljeId);
  Pozivi.upisiZauzeceAjax(divKalendar,period,danSedmica,danMjesec,prikazaniMjesec,oSemestar,oDatum,oPocetak,oKraj,oSala,sOsobljeTekst,sOsobljeId);
  //danSedmica je od 1, prikazaniMjesec od 0
}
