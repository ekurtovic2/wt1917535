let assert = chai.assert;
var kl=document.getElementById("kalendar");
var olOsobljeSala=document.getElementById("osobljeUsali");
var sOsoblje=document.getElementById("osoblje");
var sSale=document.getElementById("sale");
var pocPredavaci=[];
var osobljeDjeca;
Kalendar.iscrtajKalendar(kl,0);
var p=[],v=[],kl;
describe('Spirala 4 testovi', function() {
 describe('testovi prolaze za pocetne podatke, klikom na strelicu pored testa se prikazuje se nj prikaz, a vracanje klikom  na ovaj tekst(ne koristenjem back browsera)', function() {
   it('1.Pozivanje Pozivi.ucitajOsobeAjax(sOsoblje), ruta get/osoblje: očekivano da se dohvate osobe Neko Nekić,Drugi Neko, test test', function() {
     //potrebno testirati rutu, mozemo testirat kroz html element ili kroz sam ajax odg, svejedno
     return Pozivi.ucitajOsobeAjax(sOsoblje).then(function(x){
       let obj=JSON.parse(x);
       let prosaoTest=true;
       assert.equal(obj.length, 3,"br dohvacenih osoba treba biti 3");
       obj.forEach((item, i) => {
         if(item.id!=1 && item.id!=2 && item.id!=3) assert.equal(1, 3,"nisu dohvacene ispravne osobe");
         if(item.id==1 && (item.ime!='Neko' || item.prezime!='Nekić')) assert.equal(1, 3,"nisu dohvacene ispravne osobe");
         if(item.id==2 && (item.ime!='Drugi' || item.prezime!='Neko')) assert.equal(1, 3,"nisu dohvacene ispravne osobe");
         if(item.id==3 && (item.ime!='test' || item.prezime!='test')) assert.equal(1, 3,"nisu dohvacene ispravne osobe");
       });
       return new Promise(function(resolve,reject){resolve(x);});
     });
   });
   it('2.Pozivanje Pozivi.ucitajZauzecaAjax(), ruta get/zauzeca: očekivano da se učita jedno periodično zauzeće', function() {
     return Pozivi.ucitajZauzecaAjax().then(function(x){
       Kalendar.obojiZauzeca(kl,0,"1-11","12:00","14:00");
       let obj,per;
       obj=JSON.parse(x);
       per=obj.periodicna;
       assert.equal(per.length, 1,"br dohvacenih periodicnih zauzeca treba biti 1");
       return new Promise(function(resolve,reject){resolve(x);});
     });
   });
   it('3.Pozivanje Pozivi.ucitajZauzecaAjax(), ruta get/zauzeca: očekivano da se učita jedno vanredno zauzeće', function() {
     return Pozivi.ucitajZauzecaAjax().then(function(x){
       Kalendar.obojiZauzeca(kl,0,"1-11","12:00","14:00");
       let obj,van;
       obj=JSON.parse(x);
       van=obj.vanredna;
       assert.equal(van.length, 1,"br dohvacenih vanrednih zauzeca treba biti 1");
       return new Promise(function(resolve,reject){resolve(x);});
     });
   });
   it('4.Pozivanje Pozivi.ucitajZauzecaAjax(), ruta get/zauzeca: očekivano da se oboji 5 dana', function() {
     return Pozivi.ucitajZauzecaAjax().then(function(x){
       Kalendar.obojiZauzeca(kl,0,"1-11","12:00","14:00");//obrisat pa napravitu catcheve
       let obojeniDani = kl.getElementsByClassName("zauzeta");
       assert.equal(obojeniDani.length, 5,"br obojenih dana u crveno treba biti 5");
       return new Promise(function(resolve,reject){resolve(x);});
     });
   });
   it('5.Pozivanje Pozivi.ucitajZauzecaAjax() i'+
   ' Pozivi.upisiZauzeceAjax(kl,false,5,3,0,"zimski","03.01.2020","12:00","14:00","1-11","profesor Neko Nekić id: 1",1), '+
   'rute get i post zauzeca: ocekivano da se oboji 03.01.2020', function() {
       return Pozivi.ucitajZauzecaAjax().then(function(x){
         Kalendar.obojiZauzeca(kl,0,"1-11","12:00","14:00");
         return Pozivi.upisiZauzeceAjax(kl,false,5,3,0,"zimski","03.01.2020","12:00","14:00","1-11","profesor Neko Nekić id: 1",1).then(function(x){
           //test...
           let obojeniDani = kl.getElementsByClassName("zauzeta");
           let istina=false;
           for(let i=0;i<obojeniDani.length;i++){
             if(obojeniDani[i].innerHTML=="3") {istina=true;break;}
           }
           assert.equal(istina, true,"03.01.2020 treba biti obojen");
           return Pozivi.resetAjax().then(function(a){return new Promise(function(resolve,reject){resolve(a);});});
         });
       });
   });
   it('6.Pozivanje Pozivi.ucitajZauzecaAjax() i'+
   ' Pozivi.upisiZauzeceAjax(kl,true,6,4,0,"zimski","04.01.2020","12:00","14:00","1-11","profesor Neko Nekić id: 1",1), '+
   'rute get i post zauzeca: ocekivano da se oboji svaka subota', function() {
       return Pozivi.ucitajZauzecaAjax().then(function(x){
         Kalendar.obojiZauzeca(kl,0,"1-11","12:00","14:00");
         return Pozivi.upisiZauzeceAjax(kl,true,6,4,0,"zimski","04.01.2020","12:00","14:00","1-11","profesor Neko Nekić id: 1",1).then(function(x){
           //test...
           let obojeniDani = kl.getElementsByClassName("slobodna");
           let istina=true,testDatum,testDanSedmica;
           for(let i=0;i<obojeniDani.length;i++){
             testDatum=new Date(2020,0,obojeniDani[i].innerHTML);
             testDanSedmica=testDatum.getDay();
             if(testDanSedmica==6) {istina=false;break;}
           }
           assert.equal(istina, true,"sve subote trebaju biti obojene");
           return Pozivi.resetAjax().then(function(a){return new Promise(function(resolve,reject){resolve(a);});});
         });
       });
   });
   it('7.Pozivanje Pozivi.ucitajZauzecaAjax() i'+
   ' Pozivi.upisiZauzeceAjax(kl,true,3,8,0,"zimski","08.01.2020","12:00","14:00","1-11","profesor Neko Nekić id: 1",1), '+
   'rute get i post zauzeca, preklapanje periodicnog sa postojecim vanrednim: ocekivano da se ne dozvoli rezervacija', function() {
       return Pozivi.ucitajZauzecaAjax().then(function(){
         Kalendar.obojiZauzeca(kl,0,"1-11","12:00","14:00");
         return Pozivi.upisiZauzeceAjax(kl,true,3,8,0,"zimski","08.01.2020","12:00","14:00","1-11","profesor Neko Nekić id: 1",1).then(function(x){
           if(x.charAt(0)=='{') assert.equal(1, 3,"rezervacija ne treba biti dozvoljena");
           return new Promise(function(resolve,reject){resolve(x);});
         });
       });
   }).timeout(0);
   it('8.Pozivanje Pozivi.ucitajZauzecaAjax() i'+
   ' Pozivi.upisiZauzeceAjax(kl,true,1,6,0,"zimski","06.01.2020","12:00","14:00","1-11","profesor Neko Nekić id: 1",1), '+
   'rute get i post zauzeca, preklapanje periodicnog sa postojecim periodicnim: ocekivano da se ne dozvoli rezervacija', function() {
       return Pozivi.ucitajZauzecaAjax().then(function(){
         Kalendar.obojiZauzeca(kl,0,"1-11","12:00","14:00");
         return Pozivi.upisiZauzeceAjax(kl,true,1,6,0,"zimski","06.01.2020","12:00","14:00","1-11","profesor Neko Nekić id: 1",1).then(function(x){
           if(x.charAt(0)=='{') assert.equal(1, 3,"rezervacija ne treba biti dozvoljena");
           return new Promise(function(resolve,reject){resolve(x);});
         });
       });
   }).timeout(0);
   it('9.Pozivanje Pozivi.ucitajZauzecaAjax() i'+
   ' Pozivi.upisiZauzeceAjax(kl,false,3,1,0,"zimski","01.01.2020","12:00","14:00","1-11","profesor Neko Nekić id: 1",1), '+
   'rute get i post zauzeca, preklapanje vanrednog sa postojecim vanrednim: ocekivano da se ne dozvoli rezervacija', function() {
       return Pozivi.ucitajZauzecaAjax().then(function(){
         Kalendar.obojiZauzeca(kl,0,"1-11","12:00","14:00");
         return Pozivi.upisiZauzeceAjax(kl,false,3,1,0,"zimski","01.01.2020","12:00","14:00","1-11","profesor Neko Nekić id: 1",1).then(function(x){
           if(x.charAt(0)=='{') assert.equal(1, 3,"rezervacija ne treba biti dozvoljena");
           return new Promise(function(resolve,reject){resolve(x);});
         });
       });
   }).timeout(0);
   it('10.Pozivanje Pozivi.ucitajZauzecaAjax() i'+
   ' Pozivi.upisiZauzeceAjax(kl,false,1,13,0,"zimski","13.01.2020","12:00","14:00","1-11","profesor Neko Nekić id: 1",1), '+
   'rute get i post zauzeca, preklapanje vanrednog sa postojecim periodicnim: ocekivano da se ne dozvoli rezervacija', function() {
       return Pozivi.ucitajZauzecaAjax().then(function(){
         Kalendar.obojiZauzeca(kl,0,"1-11","12:00","14:00");
         return Pozivi.upisiZauzeceAjax(kl,false,1,13,0,"zimski","13.01.2020","12:00","14:00","1-11","profesor Neko Nekić id: 1",1).then(function(x){
           if(x.charAt(0)=='{') assert.equal(1, 3,"rezervacija ne treba biti dozvoljena");
           return new Promise(function(resolve,reject){resolve(x);});
         });
       });
   }).timeout(0);
   it('11.Pozivanje Pozivi.ucitajSaleAjax(sSale), ruta get/sale: očekivano da se dohvate sale 1-11 i 1-15', function() {
     return Pozivi.ucitajSaleAjax(sSale).then(function(x){
       let obj=JSON.parse(x);
       let prosaoTest=true;
       assert.equal(obj.length, 2,"br dohvacenih sala treba biti 2");
       obj.forEach((item, i) => {
         if(item.id!=1 && item.id!=2) assert.equal(1, 3,"nisu dohvacene ispravne sale");
         if(item.id==1 && item.naziv!='1-11') assert.equal(1, 3,"nisu dohvacene ispravne sale");
         if(item.id==2 && item.naziv!='1-15') assert.equal(1, 3,"nisu dohvacene ispravne sale");
       });
       return new Promise(function(resolve,reject){resolve(x);});
     });
   });
 });

});
